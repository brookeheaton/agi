AGI THEME - 2019 Refactoring
============

## Developer Notes

We are starting to refactor this theme. The goal is to switch to webpack for bundling and Bootstrap for the framework.

We need to run the current style sheets that are in `assets/build` and `assets/css` directories. We will keep track of which pages are using the new `evo/` directory here.

## EVO Assets.

EVO assets are stored in the `evo/` directory and are being bundled by the `webpack.config.js` in the root. The entry file is `evo/index.js`. Moving forward (March 2019) all new styling should be added to the `evo/` directory and should be compiled with the Make commands (see  `make` file in the root). 

`PLEASE NOTE: We dont have build scripts on the Acquia hosting, so we are storing the compiled assets in git for now.`
