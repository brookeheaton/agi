(function($, Drupal) {
  Drupal.behaviors.evoLayout = {
    attach: function (context, settings) {

      // Sidebar height.
      $(window).scroll(function (event) {
        var sidebarHeight = $("#sidebar-left").height();
        $(".min-height").css({minHeight: sidebarHeight + "px"});
      });

      // Hide coronavirus alert
      $('.close--x').click(function() {
        fadeAlert();
      });

      // Fade coronavirus alert
      function fadeAlert() {
        $(".coronavirus").fadeOut("slow");
        sessionStorage.setItem('covid-19','true');
      }
      //
      // $(window).load(function() {
      //   // Show coronavirus alert
      //   if (sessionStorage.getItem('covid-19') === 'false' || sessionStorage.getItem('covid-19') === null) {
      //     //$(".coronavirus").show();
      //   }
      // });
      $(window).scroll(function (event) {
        if (window.scrollY > 50) {
          // Show coronavirus alert
          if (sessionStorage.getItem('covid-19') === 'false' || sessionStorage.getItem('covid-19') === null) {
            $(".coronavirus").fadeIn();
          }
        }
      });

    }
  };

  Drupal.behaviors.seats = {
    attach: function (context, settings) {
      // Pre-populate student fields.
      if (settings.agi_seats.user !== null) {
        // For user logged.
        let first_name = settings.agi_seats.user.first_name;
        let last_name = settings.agi_seats.user.last_name;
        let mail = settings.agi_seats.user.mail;

        $('.agi-seat-first-name-0').once().val(first_name);
        $("#edit-customer-profile-billing-commerce-customer-address-und-0-first-name" ).once().val(first_name);
        $('.agi-seat-last-name-0').once().val(last_name);
        $("#edit-customer-profile-billing-commerce-customer-address-und-0-last-name" ).once().val(last_name);
        $('.agi-seat-mail-0').once().val(mail);

      } else {
        // For user not logged.
        $("#edit-customer-profile-billing-commerce-customer-address-und-0-first-name" ).once()
          .keyup(function() {
            var value = $( this ).val();
            $( ".agi-seat-first-name-0" ).val( value );
          });
        $("#edit-customer-profile-billing-commerce-customer-address-und-0-last-name" ).once()
          .keyup(function() {
            var value = $( this ).val();
            $( ".agi-seat-last-name-0" ).val( value );
          });
        $("#edit-account-login-mail" ).once()
          .keyup(function() {
            var value = $( this ).val();
            $( ".agi-seat-mail-0" ).val( value );
          });
      }

      $('.form-item-commerce-coupon-coupon-code').once().prepend('<input type="checkbox" id="id-check">');
      $('#id-check').change(function() {
        if ($(this).prop('checked')) {
          $('#edit-commerce-coupon-coupon-code').css('display', 'block');
          $('#edit-commerce-coupon-coupon-add').css('display', 'block');
          $('.form-item-commerce-coupon-coupon-code .description').css('display', 'block');
        }
        else {
          $('#edit-commerce-coupon-coupon-code').hide();
          $('#edit-commerce-coupon-coupon-add').hide();
          $('.form-item-commerce-coupon-coupon-code .description').hide();
        }
      });

    }
  };
})(jQuery, Drupal);
