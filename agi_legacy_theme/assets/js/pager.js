
jQuery(function() {

    var $ = window.jQuery,
        count = numPerPage = 20,

    // Create the 'Show More' button and add a click event listener
        button = $( '<a class="read-more pager-expand" href="">See More Illustrator Class Dates</a>' ).click( function() {

            // Increase the count by one page
            count+= numPerPage;

            hideRows( );

            return false; // Prevent Reload
        });

    function hideRows( ) {
        // Hide all rows in a 'view upcoming events' table placed after the current count
        if( $( '.view-upcoming-events' )
                .find('table.views-table tbody')
                .children(  )
                .show( )
                .filter( 'tr:gt('+(count-1)+')').hide().length == 0 ) {

            // If no rows are hidden, hide the button
            button.hide();
        }
    }

    // Append the 'Show More' button
    $( '.view-upcoming-events' ).after( button );

    hideRows( );

});
