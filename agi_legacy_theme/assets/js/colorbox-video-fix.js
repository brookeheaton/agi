(function ($) {
  Drupal.behaviors.fix_colorbox_video = {
    attach: function (context, settings) {
      $('body')
        .once('fix-colorbox-video', function () {
          $(document).bind('cbox_cleanup', function (e) {
            $('#agi-training-online-courses').trigger('pause');
          });
        });
    }
  };
})(jQuery);
