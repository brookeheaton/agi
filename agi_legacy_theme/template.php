<?php
/**
 * Implements hook_preprocess_page
 */

use Symfony\Component\Validator\Constraints\Language;

/**
 * Implements hook_theme.
 */
function agi_theme() {
  return [
    'cat_loc_adobe_additional_info' => [
      'variables' => [
        'node' => NULL,
        'dynamic_content' => NULL,
        'category' => NULL,
        'location' => NULL
      ],
      'template' => 'cat-loc-adobe-additional-info',
      'path' => drupal_get_path('theme', 'agi') . '/templates/custom'
    ],
    'footer_copy_and_reviews_summary' => [
      'variables' => [],
      'template' => 'footer-copy-and-reviews-summary',
      'path' => drupal_get_path('theme', 'agi') . '/templates/custom'
    ],
    'bbb_logo' => [
      'variables' => [],
      'template' => 'bbb-logo',
      'path' => drupal_get_path('theme', 'agi') . '/templates/custom'
    ],
    'video_testimonials' => [
      'variables' => [],
      'template' => 'video-testimonials',
      'path' => drupal_get_path('theme', 'agi') . '/templates/custom'
    ],
    'secondary_cat_location_additional_info' => [
      'variables' => ['category' => NULL, 'city' => NULL],
      'template' => 'secondary-cat-location-additional-info',
      'path' => drupal_get_path('theme', 'agi') . '/templates/custom'
    ],
    'category_additional_info' => [
      'variables' => [
        'short_category_name' => NULL,
        'city' => NULL
      ],
      'template' => 'category-page-additional-info',
      'path' => drupal_get_path('theme', 'agi') . '/templates/custom'
    ],
    'books_resources' => [
      'base hook' => 'page',
      'template' => 'page--books-resources',
      'path' => drupal_get_path('theme', 'agi') . '/templates/pages',
    ]
  ];
}

function agi_menu_tree__main_menu(&$vars) {
  return '<ul class="nav navbar-nav">' . $vars['tree'] . '</ul>';
}

function agi_menu_tree__main_menu_inner($vars) {
  return '<ul class="dropdown-menu">' . $vars['tree'] . '</ul>';
}

function agi_menu_link__main_menu($vars) {
  $element = $vars['element'];
  $sub_menu = '';

  if (!empty($element['#below'])) {
    foreach ($element['#below'] as $key => $val) {
      if (is_numeric($key)) {
        $element['#below'][$key]['#theme'] = 'menu_link__main_menu_inner'; // 2 level <li>
      }
    }
    $element['#below']['#theme_wrappers'][0] = 'menu_tree__main_menu_inner';  // 2 level <ul>
    $sub_menu = drupal_render($element['#below']);

    $element['#localized_options']['attributes']['class'][] = " dropdown-toggle ";
    $element['#localized_options']['attributes']['role'][] = "menu";
    $element['#localized_options']['attributes']['data-toggle'][] = "dropdown";
    $element['#localized_options']['attributes']['aria-expanded'][] = "true";

    $output = l($element['#title'], $element['#href'], $element['#localized_options']);

  }
  else {
    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  }

  return '<li class="menu-item">' . $output . $sub_menu . '</li>';
}


function agi_menu_link__main_menu_inner($vars) {
  $element = $vars['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);

  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . '</li>';
}

function agi_button(&$variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'submit';
  element_set_attributes($element, array(
    'id',
    'name',
    'value',
  ));
  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'form-button-disabled';
  }

  if ($element['#value'] == 'Search') {
    return '<button' . drupal_attributes($element['#attributes']) . '>
      <span class="sr-only sr-only-focusable">Search for Training Class</span>
      <svg class="feather-icon">
        <use xlink:href="#search">
      </svg>
    </button>';
  }
  else {
    return '<input' . drupal_attributes($element['#attributes']) . ' />';
  }
}

function agi_preprocess_footer_copy_and_reviews_summary(&$variables) {
  $query = db_query('SELECT field_review_rating_value FROM field_data_field_review_rating');
  $variables['reviews_count'] = $query->rowCount();
  $average = array_sum($query->fetchCol(0)) / $variables['reviews_count'];
  $variables['average'] = round($average);
}

/**
 * Implements hook_preprocess_THEME_NAME().
 */
function agi_preprocess_cat_loc_adobe_additional_info(&$variables) {
  $variables['heading_one'] = agi_dynamic_content_extract_data($variables['dynamic_content'], $variables['node'], 'field_add_heading_one');
  $variables['body_one'] = agi_dynamic_content_extract_data($variables['dynamic_content'], $variables['node'], 'field_add_body_one');
  $variables['alt_one'] = agi_dynamic_content_extract_data($variables['dynamic_content'], $variables['node'], 'field_add_alt_one');
  $variables['caption_one'] = agi_dynamic_content_extract_data($variables['dynamic_content'], $variables['node'], 'field_add_caption_one');
  $variables['heading_two'] = agi_dynamic_content_extract_data($variables['dynamic_content'], $variables['node'], 'field_add_heading_two');
  $variables['body_two'] = agi_dynamic_content_extract_data($variables['dynamic_content'], $variables['node'], 'field_add_body_two');
  $variables['alt_two'] = agi_dynamic_content_extract_data($variables['dynamic_content'], $variables['node'], 'field_add_alt_two');
  $variables['caption_two'] = agi_dynamic_content_extract_data($variables['dynamic_content'], $variables['node'], 'field_add_caption_two');
  $variables['city_short'] = $variables['location']->field_city_for_reference_pages['und'][0]['safe_value'];
  $variables['city_long'] = $variables['location']->field_location_full_state_name['und'][0]['safe_value'];
  $variables['short_cat_name'] = $variables['category']->field_short_category_name['und'][0]['safe_value'];

  // Picture settings
  $picture_mapping = picture_mapping_load('body');
  $fallback = '690x0';
  $breakpoints = picture_get_mapping_breakpoints($picture_mapping, $fallback);


  $variables['location_images'] = [];
  if (!empty($variables['location']->field_primary_image_slides)) {
    $count = 0;
    foreach ($variables['location']->field_primary_image_slides[LANGUAGE_NONE] as $image) {
      $variables['location_images'][] = [
        [
          '#theme' => 'picture',
          '#uri' => $image['uri'],
          '#style_name' => PICTURE_EMPTY_IMAGE,
          '#breakpoints' => $breakpoints,
          '#lazyload' => TRUE,
          '#alt' => $count == 0 ? $variables['alt_one'] : $variables['alt_two'],
        ],
        [
          '#type' => 'html_tag',
          '#tag' => 'blockquote',
          '#attributes' => ['class' => 'image-field-caption'],
          '#value' => $count == 0 ? $variables['caption_one'] : $variables['caption_two'],
        ]
      ];
      $count++;
    }
  }
}

/** Implements hook_css_alter */
function agi_css_alter(&$css) {
  // Force panels inline css higher in the order
  // so that theme css in aggregated files overrides.
  if (isset($css[0]) && strpos($css[0]['data'], '.panels') !== FALSE) {
    $css[0]['group'] = 0;
  }

  // Prune.
  $remove = [
    'sites/all/modules/ctools/css/ctools.css',
    'sites/all/modules/panels/css/panels.css',
    // Anything named close to this.
    'modules/system/system',
    'sites/all/modules/date',
    'modules/field',
    'sites/all/modules/logintoboggan',
    'modules/node',
    'sites/all/modules/picture',
    'modules/user',
    'sites/all/modules/views',
    'sites/all/modules/webform',
    'sites/all/modules/print',
    'sites/default/files/css/follow.css',
  ];
  foreach ($css as $key => $data) {
    foreach ($remove as $path) {
      if (strpos($key, $path) !== FALSE) {
        unset($css[$key]);
      }
    }
  }

  // @TODO
  // - Remove Colorbox from pages it is not needed.
  // -
}

/**
 * Implements hook_form_alter().
 */
function agi_form_alter(&$form, &$form_state, $form_id) {
  if (preg_match('/commerce_cart_add_to_cart_form_35746|commerce_cart_add_to_cart_form_29771/', $form_id)) {
    $form['submit']['#attributes']['title'] = $form['submit']['#attributes']['value'] = t('Register');
    $form['submit']['#attributes']['class'][] = 'btn';
    $form['submit']['#attributes']['class'][] = 'btn-primary-submit';
  }
  if ($form_id == 'webform_client_form_76576') {
    $sku = isset($_GET['cp']) ? filter_xss($_GET['cp']) : NULL;
    if ($sku) {
      $product = commerce_product_load_by_sku($sku);
      $form['#certificate_product'] = $product;
      $form['submitted']['title'] = [
        '#markup' => '<h1>' . t('Application for @title', ['@title' => $product->title]) . '</h1>',
        '#weight' => -100,
      ];
      $form['#submit'][] = '_agi_certificate_program_add_to_cart';
    }
    $form['#validate'][] = '_agi_certificate_program_validate_linkedin';
  }

  // Search form.
  if ($form_id == 'search_block_form') {
    $form['search_block_form']['#attributes']['placeholder'] = 'Find class by topic';
  }

  // Class Finder.
  if ($form_id == 'ctools_jump_menu') {
    $form['jump']['#attributes']['class'][] = 'form-control';
  }

  // Cart.
  if ($form_id == 'views_form_commerce_cart_form_default') {
    $form['actions']['submit']['#attributes']['class'][] = 'btn btn-primary mx-1';
    $form['actions']['checkout']['#attributes']['class'][] = 'btn btn-primary-submit mx-1';
  }

  // Search.
  if ($form_id == 'apachesolr_search_custom_page_search_form') {
    $form['basic']['keys']['#attributes']['class'][] = 'form-control';
    $form['basic']['submit']['#attributes']['class'][] = 'btn btn-primary mx-1';
  }

  $children = element_children($form);
  foreach ($children as $child) {
    if (isset($form[$child]['#type']) && $form[$child]['#type'] == 'textfield') {
      $form[$child]['#attributes']['class'][] = 'form-control';
    }
    if (isset($form[$child]['#type']) && $form[$child]['#type'] == 'email') {
      $form[$child]['#attributes']['class'][] = 'form-control';
    }
    elseif (isset($form[$child]['#type']) && $form[$child]['#type'] == 'password') {
      $form[$child]['#attributes']['class'][] = 'form-control';
    }
    elseif (isset($form[$child]['#type']) && $form[$child]['#type'] == 'submit') {
      if (!isset($form[$child]['#attributes']) || !in_array('btn', $form[$child]['#attributes']['class'])) {
        $form[$child]['#attributes']['class'][] = 'btn btn-primary-submit';
      }
    }
    elseif (isset($form[$child]['#type']) && $form[$child]['#type'] == 'actions') {
      $action_children = element_children($form[$child]);
      foreach ($action_children as $action_child) {
        if ($form[$child][$action_child]['#type'] == 'submit' || $form[$child][$action_child]['#type'] == 'button') {
          if (!isset($form[$child][$action_child]['#attributes']) || !in_array('btn', $form[$child][$action_child]['#attributes']['class'])) {
            $form[$child][$action_child]['#attributes']['class'][] = 'btn btn-primary';
          }
        }
      }
    }
  }
}

/**
 * Validate Handler: Validate LinkedIN URL.
 */
function _agi_certificate_program_validate_linkedin($form, &$form_state) {
  if (isset($form_state['values']['submitted']['url_to_linkedin_profile_page']) && !empty($form_state['values']['submitted']['url_to_linkedin_profile_page'])) {
    $profileurl = $form_state['values']['submitted']['url_to_linkedin_profile_page'];

    // First check the URL.
    if (!strstr(parse_url($profileurl,PHP_URL_HOST),'linkedin.com'))  {
      form_set_error('submitted[url_to_linkedin_profile_page]', t('Please enter a valid URL for your LinkedIn Profile.'));
    }
  }
}

/**
 * Submit Handler: Add cert program to cart and redirect.
 */
function _agi_certificate_program_add_to_cart($form, &$form_state) {
  if (isset($form['#certificate_product'])) {
    global $user;
    $product = $form['#certificate_product'];

    // Create new line item:
    $line_item = commerce_product_line_item_new($product, 1);

    // Add to current user's cart: if the user is not logged in ($user->uid: 0) Drupal Commerce manages the $_SESSION
    commerce_cart_product_add($user->uid, $line_item);

    // Redirect to the cart page.
    $form_state['redirect'] = 'Digital Classroom Books from AGI';
  }
}

/**
 * Implements hook_html_head_alter().
 */
function agi_html_head_alter(&$head_elements) {
  foreach ($head_elements as $key => $data) {
    if (isset($data['#name']) && $data['#name'] == 'shortlink') {
      unset($head_elements[$key]);
    }
  }
}

function agi_preprocess_html(&$vars) {
  global $user;

  $vars['node'] = $node = menu_get_object();

  // Template varibables.
  if (isset($node) && isset($node->nid)) {
    $path_alias = drupal_get_path_alias('node/' . $node->nid);
  }
  else {
    $path_alias = current_path();
  }
  if (!in_array($path_alias, $vars['classes_array'])) {
    $vars['classes_array'][] = drupal_html_class('path-' . $path_alias);
  }

  if (arg(0) == 'user' && empty(arg(1)) && !$user->uid) {
    $vars['classes_array'][] = 'path-user-login';
  }

  // Load site-wide assets.
  agi_load_libray();

  // Secondary Location Category Page.
  if (isset($node) && $node->type == 'seconday_location_category_page') {
    $category = $node->field_cat_for_location['und'][0]['entity'];
    $short_category_name = $category->field_short_category_name[LANGUAGE_NONE][0]['value'];
    $vars['head_title'] = $vars['head_title'] . ' | ' . $short_category_name . ' Training';
  }

  if (isset($node) && $node->type == 'course_date') {
    $text = $node->title;
    //Remove html tags and trim to 160 characters
    $insert = substr(strip_tags($text), 0, 160);
    //Remove extra white spaces
    $insert = preg_replace('/[\s]+/',' ',$insert);
    $meta_description = [
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'description',
        'content' => $insert
    ]];
    drupal_add_html_head($meta_description, '%node:meta-description'); //put meta tag in head
  }

  if ($node && $node->type == 'b_news') {
    global $base_url;
    drupal_add_html_head_link([
      'rel' => 'amphtml',
      'href' => $base_url . '/' . drupal_get_path_alias('node/' . $node->nid) . '?amp',
    ]);
  }

  // Books page.
  if (current_path() == 'books') {
    $vars['head_title'] = 'Digital Classroom Books from AGI';
  }

  // Certificate Program Location page.
  if ($node && $node->type == 'certificate_program_location_pag') {
    $text = $node->meta_description['und'][0]['metatags_quick']; //get the value
    $insert = substr(strip_tags($text), 0, 160); //Remove html tags and trim to 160 characters
    $insert = preg_replace('/[\s]+/',' ',$insert); //Remove extra white spaces

    $meta_description = array(
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => array(
        'name' => 'description',
        'content' => $insert //build meta tag
      ));
    drupal_add_html_head($meta_description, '%node:meta-description'); //put meta tag in head
  }
}

function agi_preprocess_page(&$vars) {

  //Setting menu in a tree structure
  $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
  $vars['main_menu'] =  $main_menu_tree;
  // Get the entire main menu tree
  $main_menu_tree = menu_tree_all_data('main-menu');
  // Add the rendered output to the $main_menu_expanded variable
  $vars['main_menu_expanded'] = menu_tree_output($main_menu_tree);

  // Layout grind classes.
  if (empty($vars['page']['left_sidebar']) && empty($vars['right_sidebar'])) {
    $vars['layout_grid']['middle'] = 'col-md-12 p-0';
  }
  elseif (!empty($vars['page']['left_sidebar']) && !empty($vars['page']['right_sidebar'])) {
    $vars['layout_grid']['middle'] = 'col-md-6 push-md-3 px-0';
    $vars['layout_grid']['left_column'] = 'col-md-3 pull-md-9';
    $vars['layout_grid']['right_column'] = 'col-md-3 push-md-3';
  }
  else {
    if (!empty($vars['page']['left_sidebar'])) {
      $vars['layout_grid']['left_column'] = 'col-md-3 pull-md-9';
      $vars['layout_grid']['middle'] = 'col-md-9 push-md-3';
    }
    else {
      $vars['layout_grid']['right_column'] = 'col-md-3 push-md-3';
    }
  }

  // Layout grid modifications.
  $nids = [6, 7, 8, 9, 11, 12, 54, 61, 1700, 3646, 9791, 9901, 9876, 9806];
  if (isset($vars['node'])) {
    if (in_array($vars['node']->nid, $nids)) {
      $vars['layout_grid']['left_column'] = 'col-md-3 pull-md-9 hidden-sm';
      $vars['layout_grid']['middle'] = 'col-md-12 px-0';
    }
  }
  if (preg_match('/books|classes\/all-classes.html|taxonomy\/term\/61|taxonomy\/term\/54/', current_path())) {
    $vars['layout_grid']['left_column'] = 'col-md-3 pull-md-9 hidden-sm';
    $vars['layout_grid']['middle'] = 'col-md-12 px-md-0';
  }

  // BBB LOGO.
  $vars['bbb_logo'] = theme('bbb_logo');

  // Request info.
  //if (!preg_match('/43471/', $vars['node']->nid)) {
    $breakpoints = picture_get_mapping_breakpoints(picture_mapping_load('left_sidebar'));
    $vars['request_info_image'] = theme('picture', [
      'uri' => 'public://training-courses.jpg',
      'breakpoints' => $breakpoints,
      'attributes' => [
        'class' => ['card-img-top']
      ]
    ]);
    $request_info = node_view(node_load('1694'));
    $vars['request_info'] = render($request_info);
  //}

  // Search.
  $vars['searchForm'] = drupal_get_form("search_block_form");

  // Front Page.
  if (drupal_is_front_page()) {
    _agi_front_page_variables($vars);
  }

  // Update page and meta title.
  if (isset($vars['node']) && isset($vars['node']->field_short_category_name) && !empty($vars['node']->field_short_category_name)) {
    $short_category_name = $vars['node']->field_short_category_name[LANGUAGE_NONE][0]['value'];

    if (in_array($vars['node']->nid, [38,39,43,44,46,75])) {
      drupal_set_title($short_category_name . ' Classes and ' . $short_category_name . ' Training');
    }
  }

  // Class Finder title.
  if (current_path() == 'classes/all-classes.html') {
    drupal_set_title("Class Finder Results");
  }


  // 404 pages.
  $status = drupal_get_http_header("status");
  if ($status == "403 Forbidden") {
    $vars['theme_hook_suggestions'] = [];
    $vars['theme_hook_suggestions'][0] = 'page__403';

    // If this is the order page, should special message.
    if (arg(0) == 'user' && arg(2) == 'orders') {
      $vars['access_denied_content'] = '<p>If you previously registered for a class, please sign-in to see your courses using the link provided via email, or contact us.</p><p>If you have not yet placed a registration, your registration appears to be incomplete.</p>';
    }
  }
  if ($status== "404 Not Found") {
    array_unshift($vars['theme_hook_suggestions'], 'page__404');
  }

  // Node Theme Suggestions
  if (isset($vars['node'])) {
    array_unshift($vars['theme_hook_suggestions'], 'page__'.$vars['node']->type);
  }

  // Category.
  if (isset($vars['node']) && $vars['node']->type == 'subject') {
    // Set variables for custom template.
    $category = $vars['category'] = $vars['node'];
    $vars['short_cat_name'] = $category->title;
    $vars['abbrev_cat_name'] = $category->field_short_category_name['und'][0]['safe_value'];

    $vars['reviews'] = views_embed_view('reviews', 'block_1');
    $category_logo = !empty($vars['node']) ? $vars['node']->field_right_header_image[LANGUAGE_NONE][0]['uri'] : '';
    if ($category_logo) {
      $breakpoints = picture_get_mapping_breakpoints(picture_mapping_load('left_sidebar'));
      $vars['category_logo'] = theme('picture', [
        'uri' => $vars['node']->field_right_header_image[LANGUAGE_NONE][0]['uri'],
        'breakpoints' => $breakpoints,
        'attributes' => [
          'class' => ['img-fluid']
        ]
      ]);
    }

    // Update page and meta title.
    if (in_array($vars['node']->nid, [38,39,43,44,46,75])) {
      drupal_set_title($vars['short_cat_name']);
    }

    // Request info.
    $breakpoints = picture_get_mapping_breakpoints(picture_mapping_load('left_sidebar'));
    if (!empty($vars['node']->field_left_header_image)) {
      $vars['request_info_image'] = theme('picture', [
        'uri' => $vars['node']->field_left_header_image[LANGUAGE_NONE][0]['uri'],
        'breakpoints' => $breakpoints,
        'attributes' => [
          'class' => ['card-img-top']
        ]
      ]);
    }

    $related_news_view = views_get_view('related_news_to_a_category');
    $related_news_view->set_display('block');
    $related_news_view->pre_execute();
    $related_news_view->execute();
    if ($related_news_view->result && !empty($related_news_view->result[0]->node_taxonomy_index_nid)) {
      $vars['related_news_to_a_category'] = $related_news_view->render();
    }

    $external_links = field_view_field('node', $vars['node'], 'field_external_links_column', ['label' => 'hidden']);
    $vars['external_links'] = render($external_links);

    $faqs = field_view_field('node', $vars['node'], 'field_faq', ['label' => 'hidden']);
    $vars['faqs'] = render($faqs);

    // Taught by.
    $vars['taught_by'] = NULL;
    if (!empty($vars['node']->field_left_header_image)) {
      $breakpoints = picture_get_mapping_breakpoints(picture_mapping_load('left_sidebar'));
      $vars['taught_by']['image'] = theme('picture', [
        'uri' => $vars['node']->field_right_header_image[LANGUAGE_NONE][0]['uri'],
        'breakpoints' => $breakpoints
      ]);
    }
    if (!empty($vars['node']->field_black_header_subtext)) {
      $vars['taught_by']['text'] = $vars['node']->field_black_header_subtext[LANGUAGE_NONE][0]['value'];
    }

    if (strpos(drupal_get_path_alias(), "adobe") !== FALSE) {
      $vars['related_certification_training'] = views_embed_view('related_certification_training', 'block');
    }
  }

  // CATEGORY LOCATION.
  elseif (isset($vars['node']) && $vars['node']->type =='category_location_page') {

    // Set variables for custom template.
    $category = $vars['category'] = $vars['node']->field_cat_for_location['und'][0]['entity'];
    $location = $vars['location'] = $vars['node']->field_location_for_cat['und'][0]['entity'];
    $vars['short_cat_name'] = $category->title;
    $vars['abbrev_cat_name'] = $category->field_short_category_name['und'][0]['safe_value'];

    if ($location->field_city_for_reference_pages['und'][0]['safe_value']) {
      $vars['city_name'] = $vars['node']->field_location_for_cat['und'][0]['entity']->field_city_for_reference_pages['und'][0]['safe_value'];
      $vars['short_state_name'] = $vars['node']->field_location_for_cat['und'][0]['entity']->field_location_state['und'][0]['safe_value'];
      $vars['full_state_name'] = $vars['node']->field_location_for_cat['und'][0]['entity']->field_location_full_state_name['und'][0]['safe_value'];
    }

    // Set Page title.
    // Specific New York Testing Nodes (Array is nids of NYC primary category location nodes).
    if (in_array($vars['node']->nid, [180, 214, 229, 226, 211, 1560, 197, 64826, 188])) {
      drupal_set_title($vars['abbrev_cat_name'] . ' Classes NYC | ' . $vars['abbrev_cat_name'] . ' Training New York');
    }
    elseif (isset($category->field_adobe_cert_cat_page) && $category->field_adobe_cert_cat_page[LANGUAGE_NONE][0]['value'] == 1) {
      drupal_set_title($vars['node']->title);
    }
    // All other New York Testing.
    elseif ($vars['city_name'] == 'NYC') {
      drupal_set_title($vars['abbrev_cat_name'] . ' Classes ' . $vars['city_name'] . ' | ' .  $vars['abbrev_cat_name'] . ' Training ' . $vars['short_state_name']);
    }
    // Default titles, check for city name and short state name.
    elseif (isset($vars['city_name'], $vars['short_state_name'])) {
      drupal_set_title($vars['abbrev_cat_name'] . ' Classes ' . $vars['city_name'] . ', ' . $vars['short_state_name'] . ' | ' .  $vars['abbrev_cat_name'] . ' Training');
    }
    // Custom location.
    elseif (isset($vars['location']) && $vars['location']->nid == 670) {
      drupal_set_title("Private " . $vars['abbrev_cat_name'] . " Training | Custom " . $vars['abbrev_cat_name'] . " Classes");
    }
    // Default titles, online.
    else {
      drupal_set_title($vars['abbrev_cat_name'] . ' Classes Online | ' .  $vars['abbrev_cat_name'] . ' Training');
    }

    // Request info.
    $breakpoints = picture_get_mapping_breakpoints(picture_mapping_load('left_sidebar'));
    if (!empty($category->field_left_header_image)) {
      $vars['request_info_image'] = theme('picture', [
        'uri' => $category->field_left_header_image[LANGUAGE_NONE][0]['uri'],
        'breakpoints' => $breakpoints,
        'attributes' => [
          'class' => ['card-img-top']
        ]
      ]);
    }
    else {
      $vars['request_info_image'] = theme('picture', [
        'uri' => 'public://training-courses.jpg',
        'breakpoints' => $breakpoints,
        'attributes' => [
          'class' => ['card-img-top']
        ]
      ]);
    }
    $request_info = node_view(node_load('1694'));
    $vars['request_info'] = render($request_info);

    // Right Sidebar.
    $vars['address'] = views_embed_view('category_location_page', 'block_1');

    // Reviews
    $vars['reviews'] = views_embed_view('reviews', 'block_5');

    // Hide the course dates views if private location.
    if (isset($vars['location']) && $vars['location']->nid == 670) {
      if (isset($vars['page']['content']['views_pri-block_2'])) {
        unset($vars['page']['content']['views_pri-block_2']);
      }
    }

    $popular_courses = (isset($location->field_popular_courses['und']) ? $location->field_popular_courses['und'] : NULL);
    if (!empty($popular_courses)) {
      foreach ($popular_courses as $course_nid) {
        $vars['popular_courses'][] = array(
          'url' => drupal_get_path_alias('node/' . $course_nid['target_id']),
          'title' => get_node_title($course_nid),
        );
      }
    }

    $vars['location_map'] = $vars['location']->field_location_map['und'][0]['safe_value'];
    $vars['location_hotels'] = $vars['location']->field_location_subjects['und'][0]['safe_value'];

    $vars['related_news'] = views_embed_view('all_news_articles', 'block_7');

    // Load Dynamic Content Overrides.
    $load_from_field_data = TRUE;
    if (module_exists('agi_dynamic_content')) {
      $criteria = agi_dynamic_content_get_criteria_from_route();
      $dynamic_content = agi_dynamic_content_fetch_dynamic_content($criteria);
      if ($dynamic_content) {
        $load_from_field_data = FALSE;
        $vars['additional_information'] = [
          '#theme' => 'cat_loc_adobe_additional_info',
          '#node' => $vars['node'],
          '#dynamic_content' => $dynamic_content,
          '#category' => $criteria['category'],
          '#location' => $criteria['location'],
          '#weight' => 99,
          '#theme_wrappers' => ['block'],
          '#prefix' => '<div id="additional-information-block">',
          '#suffix' => '</div>'
        ];
        $vars['additional_information'] = render($vars['additional_information']);
      }
      else {
        $field = field_view_field('node', $vars['node'], 'field_additional_information', ['label' => 'hidden']);
        $vars['additional_information'] = render($field);
      }
    }
    if (strtolower($vars['city_name']) == "online") {
      $vars['video'] = '<p><a class="colorbox-inline" href="/?width=640&amp;height=360&amp;inline=true#agi-training-online-courses"><img height="auto" src="/sites/all/themes/agi/assets/images/live-online-classes.png" width="100%" /></a>See how live online training works in this brief video.</p><div style="display:none;"><video controls="" height="360" id="agi-training-online-courses" poster="/sites/all/themes/agi/assets/images/agi-video-cover.png" preload="none" width="100%"><source src="http://d2cqpmwr4b912e.cloudfront.net/agi-training-online-courses.mp4" type="video/mp4"> </source></video></div>';
    }
  }

  // Secondary Category Page.
  elseif (isset($vars['node']) && $vars['node']->type =='seconday_location_category_page') {

    // Set variables for custom template.
    $vars['secondary_location'] = $vars['node']->field_secondary_location[LANGUAGE_NONE][0]['entity'];
    $category = $vars['node']->field_cat_for_location['und'][0]['entity'];
    $vars['short_cat_name'] = $category->title;
    $vars['abbrev_cat_name'] = $category->field_short_category_name['und'][0]['safe_value'];
    if ($vars['node']->field_secondary_location['und'][0]['entity']->field_address_city['und'][0]['safe_value']) {
      $vars['city_name'] = $vars['node']->field_secondary_location['und'][0]['entity']->field_address_city['und'][0]['safe_value'];
      $vars['short_state_name'] = $vars['node']->field_secondary_location['und'][0]['entity']->field_state_abbreviated_['und'][0]['safe_value'];
      $vars['full_state_name'] = $vars['node']->field_secondary_location['und'][0]['entity']->field_state['und'][0]['safe_value'];
    }

    $vars['head_title'] = $vars['short_cat_name'] . ' Classes | ' . $vars['short_cat_name'] . ' Training';

    // Request info.
    $breakpoints = picture_get_mapping_breakpoints(picture_mapping_load('left_sidebar'));
    if (!empty($category->field_left_header_image)) {

      $vars['request_info_image'] = theme('picture', [
        'uri' => $category->field_left_header_image[LANGUAGE_NONE][0]['uri'],
        'breakpoints' => $breakpoints,
        'attributes' => [
          'class' => ['card-img-top']
        ]
      ]);
    }
    else {
      $vars['request_info_image'] = theme('picture', [
        'uri' => 'public://training-courses.jpg',
        'breakpoints' => $breakpoints,
        'attributes' => [
          'class' => ['card-img-top']
        ]
      ]);
    }
    $request_info = node_view(node_load('1694'));
    $vars['request_info'] = render($request_info);

    // Reviews.
    $vars['reviews'] = views_embed_view('reviews', 'block_5');

    // Location Info.
    $vars['location_info'] = '<h2>' . $vars['abbrev_cat_name'] . ' classes in ' . $vars['secondary_location']->title . ', ' . $vars['secondary_location']->field_state_abbreviated_[LANGUAGE_NONE][0]['value'] . ' Location</h2><p>American Graphics Institute utilizes a satellite campus location in ' . $vars['secondary_location']->title . '. This location is available for private and group ' . $vars['secondary_location']->field_nearby_primary_location[LANGUAGE_NONE][0]['entity']->title . ' classes. For regularly scheduled classes for individuals, view the class options listed under the course dates.</p>';

    // Location Images.
    $location_images = field_view_field('node', $vars['secondary_location'], 'field_location_image', 'default');
    $images = element_children($location_images);
    foreach ($images as $delta) {
      $alt = str_replace('[city_name]', $vars['city_name'], $location_images[$delta]['#item']['alt']);
      $alt = str_replace('[abbrev_cat_name]', $vars['abbrev_cat_name'], $alt);
      $location_images[$delta]['#item']['alt'] = $alt;
      $title = !empty($location_images[$delta]['#item']['title']) ? str_replace('[city_name]', $vars['city_name'], $location_images[$delta]['#item']['title']) : $alt;
      $title = str_replace('[abbrev_cat_name]', $vars['abbrev_cat_name'], $title);
      $location_images[$delta]['#item']['title'] = $title;
    }
    $vars['location_images'] = '<div class="cf">' . render($location_images) . '</div>';

    // Location Address.
    $vars['location_address'] = views_embed_view('secondary_city_classes', 'block_2');

    // We had to set the content like this, instead of context because I needed to use Views + custom content together.
    $vars['nearby_primary'] = NULL;
    $nearby_primary_location = NULL;
    if ($vars['secondary_location']->field_nearby_primary_location[LANGUAGE_NONE][0]['target_id']) {
      $nearby_primary_location = node_load($vars['secondary_location']->field_nearby_primary_location[LANGUAGE_NONE][0]['target_id']);
      $nearby_primary_location_node_title = $nearby_primary_location->field_city_for_reference_pages['und'][0]['safe_value'];
      $vars['nearby_primary'] = '<h3>Live in-person ' . $vars['abbrev_cat_name'] . ' training classes near ' . $vars['secondary_location']->title . '</h3><p>The ' . $vars['secondary_location']->title . ' facility for private and corporate ' . $vars['short_cat_name'] . ' training is located within driving distance of our ' . $nearby_primary_location_node_title . ' training center. In ' . $nearby_primary_location_node_title . ' we offer regularly scheduled, live, in-person training courses with an instructor in the same classroom with you.</p>';
      $cat_loc = db_query('
      SELECT loc.entity_id
      FROM {field_data_field_location_for_cat} loc
      INNER JOIN {field_data_field_cat_for_location} cat ON cat.entity_id = loc.entity_id
      WHERE loc.field_location_for_cat_target_id = :loc_nid
      AND cat.field_cat_for_location_target_id = :cat_nid', [
        ':loc_nid' => $nearby_primary_location->nid,
        ':cat_nid' => $vars['node']->field_cat_for_location['und'][0]['entity']->nid
      ])->fetchAssoc();
      $vars['nearby_primary_link'] = '<p>See <a href="/' . drupal_get_path_alias('node/' . $cat_loc['entity_id']) . '" style="display:inline;">' . $vars['abbrev_cat_name'] . ' classes in ' . $nearby_primary_location_node_title . '</a>.';
    }

    $vars['related_news'] = views_embed_view('all_news_articles', 'block_7');
    $vars['video'] = '<p><a class="colorbox-inline" href="/?width=640&amp;height=360&amp;inline=true#agi-training-online-courses"><img height="auto" src="/sites/all/themes/agi/assets/images/live-online-classes.png" width="100%" /></a>See how live online training works in this brief video.</p><div style="display:none;"><video controls="" height="360" id="agi-training-online-courses" poster="/sites/all/themes/agi/assets/images/agi-video-cover.png" preload="none" width="100%"><source src="http://d2cqpmwr4b912e.cloudfront.net/agi-training-online-courses.mp4" type="video/mp4"> </source></video></div>';
  }

  // Location.
  if (isset($vars['node']) && $vars['node']->type == 'location') {

    if (!empty($vars['node']->field_primary_image_slides)) {
      // No more than four.
      foreach ($vars['node']->field_primary_image_slides[LANGUAGE_NONE] as $delta => $value) {
        if ($delta < 4) {

          $breakpoints = picture_get_mapping_breakpoints(picture_mapping_load('article_grid'));
          $vars['location_images'][] = theme('picture', [
            'uri' => $value['uri'],
            'breakpoints' => $breakpoints,
          ]);
        }
      }
    }
  }

  // Secondary Location.
  if (isset($vars['node']) && $vars['node']->type == 'seconday_location_page') {
    $vars['address'] = views_embed_view('secondary_city_classes', 'block_4');
    $vars['sidebar_desc'] = field_view_field('node', $vars['node'], 'field_body', ['label' => 'hidden']);
    $request_info = node_view(node_load('1694'));
    $vars['request_info'] = render($request_info);
    if (!empty($vars['node']->field_location_images)) {
      // No more than four.
      foreach ($vars['node']->field_location_images[LANGUAGE_NONE] as $delta => $value) {
        if ($delta < 4) {

          $breakpoints = picture_get_mapping_breakpoints(picture_mapping_load('article_grid'));
          $vars['location_images'][] = theme('picture', [
            'uri' => $value['uri'],
            'breakpoints' => $breakpoints,
          ]);
        }
      }
    }
  }

  // Course Date.
  if (isset($vars['node']) && $vars['node']->type == 'course_date') {
    $category = $vars['node']->field_cat_for_location['und'][0]['entity'];
    $breakpoints = picture_get_mapping_breakpoints(picture_mapping_load('left_sidebar'));
    if (!empty($category->field_left_header_image)) {
      $vars['request_info_image'] = theme('picture', [
        'uri' => $category->field_left_header_image[LANGUAGE_NONE][0]['uri'],
        'breakpoints' => $breakpoints,
        'attributes' => [
          'class' => ['card-img-top']
        ]
      ]);
    }
    else {
      $vars['request_info_image'] = theme('picture', [
        'uri' => 'public://training-courses.jpg',
        'breakpoints' => $breakpoints,
        'attributes' => [
          'class' => ['card-img-top']
        ]
      ]);
    }
    $request_info = node_view(node_load('1694'));
    $vars['request_info'] = render($request_info);
    $vars['sidebar_display'] = views_embed_view('course_date_detail_panel_content', 'panel_pane_2');
    $vars['video'] = '<p><a class="colorbox-inline" href="/?width=640&amp;height=360&amp;inline=true#agi-training-online-courses"><img height="auto" src="/sites/all/themes/agi/assets/images/live-online-classes.png" width="100%" /></a>During the pandemic all in-person classes are delivered live online. You can retake the course in-person at no cost once our classrooms reopen. See how live online training works in this brief video.</p><div style="display:none;"><video controls="" height="360" id="agi-training-online-courses" poster="/sites/all/themes/agi/assets/images/agi-video-cover.png" preload="none" width="100%"><source src="http://d2cqpmwr4b912e.cloudfront.net/agi-training-online-courses.mp4" type="video/mp4"> </source></video></div>';
  }

  // Course Node.
  if (isset($vars['node']) && $vars['node']->type == 'course') {
    $category = node_load($vars['node']->field_primary_category['und'][0]['target_id']);
    $breakpoints = picture_get_mapping_breakpoints(picture_mapping_load('left_sidebar'));
    if (!empty($vars['node']->field_cat_hero_image)) {
      $vars['request_info_image'] = theme('picture', [
        'uri' => $vars['node']->field_cat_hero_image[LANGUAGE_NONE][0]['uri'],
        'breakpoints' => $breakpoints,
        'attributes' => [
          'class' => ['card-img-top']
        ]
      ]);
    }
    elseif (!empty($category->field_left_header_image)) {
      $vars['request_info_image'] = theme('picture', [
        'uri' => $category->field_left_header_image[LANGUAGE_NONE][0]['uri'],
        'breakpoints' => $breakpoints,
        'attributes' => [
          'class' => ['card-img-top']
        ]
      ]);
    }
    else {
      $vars['request_info_image'] = theme('picture', [
        'uri' => 'public://training-courses.jpg',
        'breakpoints' => $breakpoints,
        'attributes' => [
          'class' => ['card-img-top']
        ]
      ]);
    }
    $request_info = node_view(node_load('1694'));
    $vars['request_info'] = render($request_info);
    $vars['reviews'] = views_embed_view('reviews', 'block_3');
    $variables['related_certification_training'] = views_embed_view('related_certification_training', 'block');
  }

  // Certificate Program Node.
  if (isset($vars['node']) && $vars['node']->type == 'certificate_program') {
    if (!empty($vars['node']->field_cp_related_categories)) {
      $vars['short_cat_name'] = $vars['node']->field_cp_related_categories['und'][0]['entity']->title;
      $vars['abbrev_cat_name'] = $vars['node']->field_cp_related_categories['und'][0]['entity']->field_short_category_name['und'][0]['safe_value'];
    }
    else {
      $vars['abbrev_cat_name'] = str_replace("Certificate", "", $vars['node']->title);
    }
    $request_info = node_view(node_load('9796'));
    $breakpoints = picture_get_mapping_breakpoints(picture_mapping_load('left_sidebar'));
    if (!empty($vars['node']->field_left_header_image)) {
      $vars['request_info_image'] = theme('picture', [
        'uri' => $vars['node']->field_left_header_image[LANGUAGE_NONE][0]['uri'],
        'breakpoints' => $breakpoints,
        'attributes' => [
          'class' => ['card-img-top']
        ]
      ]);
    }
    else {
      $vars['request_info_image'] = theme('picture', [
        'uri' => 'public://training-courses.jpg',
        'breakpoints' => $breakpoints,
        'attributes' => [
          'class' => ['card-img-top']
        ]
      ]);
    }
    $vars['request_info'] = render($request_info);
    $vars['sidebar_video_testimonials'] = theme('video_testimonials');
    $vars['sidebar_buttons'] = views_embed_view('category_page_right_buttons', 'block');
  }

  // Certificate Program Location Page
  if (isset($vars['node']) && $vars['node']->type =='certificate_program_location_pag') {
    $vars['cert_program'] = $vars['node']->field_cp_loc_program[LANGUAGE_NONE][0]['entity']->title;
    if (!empty($vars['node']->field_cp_loc_iocation)) {
      $vars['city_name'] = $vars['node']->field_cp_loc_iocation['und'][0]['entity']->field_city_for_reference_pages['und'][0]['safe_value'];
      $vars['short_state_name'] = $vars['node']->field_cp_loc_iocation['und'][0]['entity']->field_location_state['und'][0]['safe_value'];
    }
    $vars['logo_alt_text'] = $vars['cert_program'] . ' training classes in ' . $vars['city_name'] . ', ' . $vars['short_state_name'] . '.';

    // Request info.
    //$variables['field_cp_loc_program'][0]['entity']->title;
    $breakpoints = picture_get_mapping_breakpoints(picture_mapping_load('left_sidebar'));
    if (!empty($vars['node']->field_cp_loc_program) && !empty($vars['node']->field_cp_loc_program[LANGUAGE_NONE][0]['entity']->field_left_header_image)) {
      $vars['request_info_image'] = theme('picture', [
        'uri' => $vars['node']->field_cp_loc_program[LANGUAGE_NONE][0]['entity']->field_left_header_image[LANGUAGE_NONE][0]['uri'],
        'breakpoints' => $breakpoints,
        'attributes' => [
          'class' => ['card-img-top']
        ]
      ]);
    }
    else {
      $vars['request_info_image'] = theme('picture', [
        'uri' => 'public://training-courses.jpg',
        'breakpoints' => $breakpoints,
        'attributes' => [
          'class' => ['card-img-top']
        ]
      ]);
    }

    $request_info = node_view(node_load('1694'));
    $vars['request_info'] = render($request_info);

    // Random quote, except on UX cert pages.
    if ($vars['node']->field_cp_loc_program[LANGUAGE_NONE][0]['entity']->nid != "9826") {
      $vars['random_quote'] = views_embed_view('random_quote', 'block');
    }

  }

  // Exam Product Display
  if (isset($vars['node']) && $vars['node']->type == 'exam_credit_product_display') {
    $vars['sidebar_display'] = views_embed_view('exam_products', 'block_2');
  }

  // Get the path for conditional use
  $router_item = menu_get_item();

  if ($router_item['path'] == 'profile-main/%' || $router_item['path'] == 'profile-main') {
    global $user;
    $profile = profile2_load_by_user($user->uid);
    if (isset($profile['main'])) {
      $vars['profile_uid'] = $profile['main']->uid;
      $vars['profile_first_name'] = $profile['main']->field_first_name['und'][0]['safe_value'];
      $vars['profile_last_name'] = $profile['main']->field_last_name['und'][0]['safe_value'];
    }
  }

  $cart_count = 0;
  $order = commerce_cart_order_load($vars['user']->uid);
  if ($order) {
    $wrapper = entity_metadata_wrapper('commerce_order', $order);
    $line_items = $wrapper->commerce_line_items;
    $cart_count = commerce_line_items_quantity($line_items, commerce_product_line_item_types());
  }
  // Make the number of products in the shopping cart available to the page template.
  $vars['products_in_shopping_cart'] = $cart_count;
  // Nodes that need request info.
  if ((isset($vars['node'])) && (
    $vars['node']->nid == "7" ||
    $vars['node']->nid == "8" ||
    $vars['node']->nid == "9" ||
    $vars['node']->nid == "11" ||
    $vars['node']->nid == "1702" ||
    $vars['node']->nid == "3646" ||
    $vars['node']->nid == "9791" ||
    arg(0) == 'taxonomy' ||
    $vars['node']->type == "tutorials"
    )) {
    // Request info.
    $image = "public://training-courses.jpg";
    if ($vars['node']->nid == "9791") {
      $image = "public://certificate-programs-agi.jpg";
    }
    if ($vars['node']->nid == "8") {
      $image = "public://custom-private-training.jpg";
    }
    if ($vars['node']->nid == "9") {
      $image = "public://online-training-classes.jpg";
    }

    $breakpoints = picture_get_mapping_breakpoints(picture_mapping_load('left_sidebar'));
    $vars['request_info_image'] = theme('picture', [
      'uri' => $image,
      'breakpoints' => $breakpoints,
      'attributes' => [
        'class' => ['card-img-top']
      ]
    ]);
    if ($vars['node']->nid == "9791") {
      $request_info = node_view(node_load('9796'));
    }
    else {
      $request_info = node_view(node_load('1694'));
    }
    $vars['request_info'] = render($request_info);
  }

  // Locations Page.
  if (isset($vars['node']) && $vars['node']->nid == "3646") {
    $vars['secondary_locations'] = views_embed_view("all_secondary_locations");
  }

  // Book term pages.
  if (arg(0) == "taxonomy" && arg(1) == "term") {
    $term = taxonomy_term_load(arg(2));
    if ($term && $term->vid == 11) {
      $vars['theme_hook_suggestions'] = 'page--taxonomy--' . $term->vocabulary_machine_name;

      $vars['featured_articles'] = views_embed_view('all_news_articles', 'block_3');
      if (!empty($term->field_related_course_category)) {
        $category = node_load($term->field_related_course_category[LANGUAGE_NONE][0]['target_id']);
        $vars['related_category'] = l($category->title, 'node/' . $category->nid);
      }
    }
  }

  // DC Book.
  if (isset($vars['node']) && $vars['node']->type == "dc_book") {
    // Request info.
    $breakpoints = picture_get_mapping_breakpoints(picture_mapping_load('left_sidebar'));
    $vars['request_info_image'] = theme('picture', [
      'uri' => "public://training-courses.jpg",
      'breakpoints' => $breakpoints,
      'attributes' => [
        'class' => ['card-img-top']
      ]
    ]);
    $request_info = node_view(node_load('1694'));
    $vars['request_info'] = render($request_info);
  }

  // Adobe Cert Training Category Page.
  if (isset($vars['node']->field_adobe_cert_cat_page) && $vars['node']->field_adobe_cert_cat_page[LANGUAGE_NONE][0]['value'] == 1) {
    array_unshift($vars['theme_hook_suggestions'], 'page__adobe_cert_training');
  }

  if (isset($vars['node']) && $vars['node']->type == "page") {
    $request_info = node_view(node_load('12'));
    $vars['request_info'] = render($request_info);
  }

  // Explode url to compare books page and add new theme_suggestions.
  $page_path = explode('/', drupal_get_normal_path(current_path()));
  if ($page_path[0] =='books' && $page_path[3] == 'resources') {
    if (in_array("page__books", $vars['theme_hook_suggestions'])) {
      unset($vars['theme_hook_suggestions']['page__books']);
      $vars['theme_hook_suggestions'][] = 'page__books_resources';
    }
  }
}

/**
 * Implements THEME_preprocess_node().
 */
function agi_preprocess_node(&$variables) {
  $reaction = context_get_plugin('reaction', 'block');
  $variables['region']['in_content'] = $reaction->block_get_blocks_by_region('content');
  if (in_array($variables['nid'], [43, 110, 196, 197, 198, 1704, 945, 1023, 1024, 9866, 9724, 9871, 146, 679, 9906])) {
    // TODO Once V3 UX testing is approved we need to dynamically populate $cat_node based on path.
    $cat_id = 43;
    $cat_node = node_load($cat_id);
    $variables['cat_short_name'] = $cat_node->field_short_category_name['und'][0]['safe_value'];
  }
  // If a category location node.
  $variables['loc_node'] = isset($variables['field_location_for_cat']) ? $variables['field_location_for_cat'][0]['entity'] : NULL;
  $variables['loc_state'] = isset($variables['loc_node']) ? $variables['loc_node']->field_location_full_state_name[LANGUAGE_NONE][0]['value'] : NULL;

  if (in_array($variables['nid'], [196, 197, 198, 1704, 945, 1023, 1024, 679])) {
    // Get the city short name.
    $loc_short_name = $variables['loc_node']->field_city_for_reference_pages['und'][0]['safe_value'];
    $variables['location_city_short'] = $loc_short_name;

    // Get the address from the location node.
    $variables['loc_address_field'] = $variables['loc_node']->field_full_address['und'][0]['safe_value'];

    // Modify schema.org rich snippet.
    if ($variables['type'] == 'category_location_page') {
        $orig_string = '<div itemprop="description" style="margin-top:20px; font-style:italic">';
        $new_string = '<div itemprop="description" style="margin-top:20px; font-style:italic">' . $variables['cat_short_name'] . ' Classes and ' . $variables['cat_short_name'] . ' Training in ' . $variables['location_city_short'] . '. ';

        $new_rich_description = str_replace($orig_string, $new_string, $variables['loc_address_field']);

        $variables['loc_address_field'] = $new_rich_description;
    }

    $images = $variables['loc_node']->field_primary_image_slides['und'];
    foreach ($images as $key => $value) {
      $variables['image_slide_urls'][$key] = file_create_url($value['uri']);
    }

    $popular_courses = $variables['loc_node']->field_popular_courses['und'];

    foreach ($popular_courses as $course_nid) {
      $variables['popular_courses'][] = array(
        'url' => drupal_get_path_alias('node/' . $course_nid['target_id']),
        'title' => get_node_title($course_nid),
      );
    }

    $variables['location_map'] = $variables['loc_node']->field_location_map['und'][0]['safe_value'];
    $variables['location_hotels'] = $variables['loc_node']->field_location_subjects['und'][0]['safe_value'];
  }

  // If a certification program location node.
  if (in_array($variables['nid'], [9906])) {
    $loc_node = $variables['field_cp_loc_iocation'][0]['entity'];

    // Get the city short name.
    $loc_short_name = $loc_node->field_city_for_reference_pages['und'][0]['safe_value'];
    $variables['location_city_short'] = $loc_short_name;

    // Get the address from the location node.
    $variables['loc_address_field'] = $loc_node->field_full_address['und'][0]['safe_value'];

    $images = $loc_node->field_primary_image_slides['und'];
    foreach ($images as $key => $value) {
      $variables['image_slide_urls'][$key] = file_create_url($value['uri']);
    }

    $popular_courses = $loc_node->field_popular_courses['und'];

    foreach ($popular_courses as $course_nid) {
      $variables['popular_courses'][] = array(
        'url' => drupal_get_path_alias('node/' . $course_nid['target_id']),
        'title' => get_node_title($course_nid),
      );
    }

    $variables['location_map'] = $loc_node->field_location_map['und'][0]['safe_value'];
    $variables['location_hotels'] = $loc_node->field_location_subjects['und'][0]['safe_value'];
  }

  // Exam Credit Product Display.
  if ($variables['node']->type == 'exam_credit_product_display') {
    if (!empty($variables['node']->field_exam_product_title_icon)) {
      $icon = theme('image', ['path' => $variables['node']->field_exam_product_title_icon[LANGUAGE_NONE][0]['uri']]);
      $variables['title_icon'] = $icon;
    }
  }

  // Category (subject) page.
  // @see hook_preprocess_page for left rail items.
  if (isset($variables['node']) && $variables['node']->type =='subject') {
    $variables['news_url'] = (isset($cat_id) ? get_news_url($cat_id) : NULL);
    $variables['classes_in_category'] = views_embed_view('classes_in_category', 'block_1');
    $variables['courses_in_category'] = views_embed_view('course_in_category_snippet', 'panel_pane_1');
    $variables['location_list'] = views_embed_view('location_list_for_category_sidebar', 'block', [$variables['node']->nid]);
    $variables['short_category_name'] = (isset($variables['node']->field_short_category_name[LANGUAGE_NONE][0]['value']) ?
      $variables['node']->field_short_category_name[LANGUAGE_NONE][0]['value'] : NULL);
    $variables['classes_in_category'] = views_embed_view('classes_in_category', 'block_1');
    $variables['secondary_navigation'] = views_embed_view('category_page_right_buttons_grey_header', 'block');
    if (!empty($variables['node']->field_dcb_category)) {
      $variables['category_panes'] = views_embed_view('category_panes', 'panel_pane_1');
    }

    if ($variables['node']->nid == 165) {
      $variables['upcoming_classes'] = views_embed_view('upcoming_apple_classes', 'block');
      $variables['all_locations_link'] = views_embed_view('all_locations_link', 'block_2');
    }
    elseif ($variables['node']->nid == 6) {
      // Different template for Adobe landing page.
      $variables['theme_hook_suggestions'][0] = 'node__subject__adobe_landing';
      $variables['upcoming_classes'] = views_embed_view('upcoming_adobe_classes', 'block');
      $variables['all_locations_link'] = views_embed_view('all_locations_link', 'block_2');
    }
    elseif (isset($variables['node']->field_adobe_cert_cat_page[LANGUAGE_NONE][0]['value']) &&
      $variables['node']->field_adobe_cert_cat_page[LANGUAGE_NONE][0]['value'] == 1) {
      $variables['theme_hook_suggestions'][0] = 'node__subject';
      $variables['upcoming_classes'] = views_embed_view('upcoming_events', 'block_4');
    }
    else {
      $variables['upcoming_classes'] = views_embed_view('upcoming_events', 'block_2');
      $variables['all_locations_link'] = "";
      $variables['all_locations_link'] .= views_embed_view('all_locations_link', 'block_2');
      $variables['all_locations_link'] .= views_embed_view('all_locations_link', 'block_3');
      $variables['cat_additional_info'] = theme('category_additional_info', ['short_category_name' => $variables['short_category_name'] ]);
    }

    $variables['related_certificate_programs'] = NULL;
    if (!empty($variables['node']->field_certificate_programs)) {
      $programs = [];
      foreach ($variables['node']->field_certificate_programs[LANGUAGE_NONE] as $item) {
        if (!isset($item['entity']) && isset($item['target_id'])) {
          $item['entity'] = node_load($item['target_id']);
        }
        $programs[] = l($item['entity']->title, drupal_get_path_alias("node/" . $item['entity']->nid));
      }
      $variables['related_certificate_programs'] = [
        [
          '#markup' => '<em>In addition to ' . $variables['short_category_name'] . ' classes, AGI offers ' . format_plural(count($programs), 'this', 'these').  ' multi-week certificate ' . format_plural(count($programs), 'program', 'programs') . '.</em>'
        ],
        [
          '#markup' => theme('item_list', [
            'items' => $programs,
            'attributes' => ['class' => ['related-certificate-programs']]
          ])
        ]
      ];
    }
  }

  // Category Location page.
  if ($variables['node']->type =='category_location_page') {
    // Set variables for custom template.
    $variables['category'] = $variables['node']->field_cat_for_location['und'][0]['entity'] ?? NULL;
    $variables['cat_short_name'] = $variables['field_cat_for_location'][0]['entity']->field_short_category_name['und'][0]['safe_value'];
    $variables['loc_city_short'] = $variables['field_location_for_cat'][0]['entity']->field_city_for_reference_pages['und'][0]['safe_value'];
    if (empty($variables['loc_city_short']) && $variables['field_location_for_cat'][0]['entity']->nid == 670) {
      $variables['loc_city_short'] = 'private';
    }
    $variables['secondary_location'] = $variables['node']->field_secondary_location[LANGUAGE_NONE][0]['entity'];
    $variables['short_cat_name'] = $variables['node']->field_cat_for_location['und'][0]['entity']->title;
    $variables['abbrev_cat_name'] = $variables['node']->field_cat_for_location['und'][0]['entity']->field_short_category_name['und'][0]['safe_value'];
    if (isset($variables['node']->field_secondary_location['und'][0]['entity']->field_address_city['und'][0]['safe_value'])) {
      $variables['city_name'] = $variables['node']->field_secondary_location['und'][0]['entity']->field_address_city['und'][0]['safe_value'];
      $variables['short_state_name'] = $variables['node']->field_secondary_location['und'][0]['entity']->field_state_abbreviated_['und'][0]['safe_value'];
      $variables['full_state_name'] = $variables['node']->field_secondary_location['und'][0]['entity']->field_state['und'][0]['safe_value'];
    }
    $variables['secondary_navigation'] = views_embed_view('category_page_right_buttons_grey_header', 'block_1');

    // Main content.
    $intro = field_view_field('node', $variables['node'], 'body', ['label' => 'hidden']);
    $variables['intro'] = render($intro);
    $variables['classes_in_category'] = views_embed_view('category_location_view', 'block_1');
    $variables['courses_in_category'] = views_embed_view('category_location_view', 'block_3');
    $variables['upcoming_classes'] = views_embed_view('pri', 'block_2');
    $about_cat = field_view_field('node', $variables['node'], 'field_about_cat_and_loc', ['label' => 'hidden']);
    $variables['about_cat'] = render($about_cat);
  }

  // Secondary Location Category page.
  if ($variables['node']->type == 'seconday_location_category_page') {
    $variables['secondary_location'] = $variables['node']->field_secondary_location[LANGUAGE_NONE][0]['entity'];
    $variables['city_name'] = $variables['secondary_location']->title;
    $variables['full_state_name'] = !empty($variables['field_secondary_location']) ? $variables['field_secondary_location'][0]['entity']->field_address_state[LANGUAGE_NONE][0]['value'] : "";
    $variables['cat_name'] = $variables['field_cat_for_location'][0]['entity']->title;
    $variables['cat_short_name'] = $variables['field_cat_for_location'][0]['entity']->title;
    $variables['abbrev_cat_name'] = $variables['node']->field_cat_for_location['und'][0]['entity']->field_short_category_name['und'][0]['safe_value'];

    // We had to set the content like this, instead of context because I needed to use Views + custom content together.
    $variables['main_content'] = [];
    $variables['main_content']['custom_one']['#markup'] = '<h2>' . $variables['cat_short_name'] . ' in ' . $variables['secondary_location']->title . ', ' . $variables['secondary_location']->field_state[LANGUAGE_NONE][0]['value'] . '</h2><p>AGI offers ' . $variables['abbrev_cat_name'] . ' classes in ' . $variables['secondary_location']->title . ', ' . $variables['secondary_location']->field_state[LANGUAGE_NONE][0]['value'] . ' with a live instructor. This includes private ' . $variables['abbrev_cat_name'] . ' training for groups and individuals with a live, in-person instructor that comes to your location in ' . $variables['secondary_location']->title . '. Or benefit from regularly scheduled ' . $variables['abbrev_cat_name'] . ' courses with a live instructor that you can participate in online from your home or office.</p><p>Click a ' . $variables['abbrev_cat_name'] . ' course title below to view class details, dates, and locations.</p><h3>Popular ' . $variables['abbrev_cat_name'] . ' classes in ' . $variables['secondary_location']->title . ':</h3>';
    $variables['main_content']['custom_one']['#class'] = 'mb-3';

    $variables['main_content']['classes']['#markup'] = views_embed_view('secondary_city_classes', 'block');
    $variables['main_content']['classes']['#class'] = 'mb-5';

    $variables['main_content']['custom_two']['#markup'] = '<h2>' . $variables['abbrev_cat_name'] . ' Classes in ' . $variables['secondary_location']->title . ' for Individuals</h2><p>Our live ' . $variables['abbrev_cat_name'] . ' classes are a cost effective way for individuals in ' . $variables['secondary_location']->title . ', ' .  $variables['secondary_location']->field_state[LANGUAGE_NONE][0]['value'] . ' to learn ' . $variables['abbrev_cat_name'] . ' while receiving individualized attention. In these classes you see the instructors screen, hear their voice, and are able to ask questions. Course materials and a headset with microphone are provided with your enrollment. If you prefer to be in the same classroom as an instructor, see our <a href="' . url(drupal_get_path_alias('node/' . $variables['field_cat_for_location'][0]['entity']->nid)) . '">' . $variables['abbrev_cat_name'] . ' classes</a> listed above for dates and locations of our in-person courses.</p>';
    $variables['main_content']['custom_two']['#class'] = 'mb-5';

    $variables['main_content']['custom_three']['#markup'] = '<h2>' . $variables['abbrev_cat_name'] . ' Classes in ' . $variables['secondary_location']->title . ' for Groups</h2><p>If you have three or more people that need ' . $variables['abbrev_cat_name']. ' training in ' . $variables['secondary_location']->title. ', ' . $variables['secondary_location']->field_state[LANGUAGE_NONE][0]['value'] . ', AGI offers live, on-site training at your location for your group. We also have dedicated classroom space at our offices in several U.S. cities if you prefer to hold the course in one of our training centers.</p><p><a href="/custom-training">Submit a request for private training&nbsp;information</a> for your group in ' . $variables['secondary_location']->title . ', ' . $variables['secondary_location']->field_state[LANGUAGE_NONE][0]['value'] . ' or call <span style="white-space: nowrap;">800-851-9237</span> for more information.</p>';
    $variables['main_content']['custom_three']['#class'] = 'mb-5';

    $variables['main_content']['upcoming_events']['#markup'] = views_embed_view('secondary_location_upcoming_events', 'block');
    $variables['main_content']['upcoming_events']['#class'] = 'upcoming-classes mb-5';

    $variables['main_content']['custom_four']['#markup'] = '<h2>' . $variables['abbrev_cat_name'] . ' Classes in ' . $variables['secondary_location']->title . ' Course Details and Outlines</h2>';
    $variables['main_content']['custom_four']['#markup'] .= views_embed_view('category_location_view', 'block_4');
    $variables['main_content']['custom_four']['#class'] = 'mb-5 course-details';

    $free_text = field_view_field('node', $variables['node'], 'field_sec_loc_cat_free_text', ['label' => 'hidden']);
    $variables['main_content']['free_text']['#markup'] = render($free_text);
    $variables['main_content']['free_text']['class'] = '';

    // Popular Courses.
    $sql = "
    SELECT n.nid, n.title
    FROM {node} n INNER JOIN {field_data_field_secondary_location} loc ON n.nid = loc.entity_id
    WHERE n.type = :type AND n.status = 1 AND loc.field_secondary_location_target_id = :loc_nid";

    $results = db_query($sql, [
      ':type' => 'seconday_location_category_page',
      ':loc_nid' => $variables['node']->field_secondary_location[LANGUAGE_NONE][0]['target_id']
    ])->fetchAll();

    if ($results) {
      foreach ($results as $result) {
        $vars['popular_courses'][] = l($result->title, 'node/' . $result->nid);
      }
    }

    // Special text for certain subjects:
    // Illustrator, InDesign, Photoshop, Premiere Pro, After Effects, XD
    $categories = ['Illustrator', 'InDesign', 'Photoshop', 'Premiere Pro', 'After', 'XD'];
    if (in_array($variables['abbrev_cat_name'], $categories)) {
      $variables['additional_info'] = theme("secondary_cat_location_additional_info", ['category' => $variables['abbrev_cat_name'], 'city' => $variables['city_name']]);
    }
  }

  // Location.
  if ($variables['node']->type == 'location') {
    $variables['location_city_short'] = $variables['node']->field_city_for_reference_pages[LANGUAGE_NONE][0]['value'];
    $variables['upcoming_classes'] = views_embed_view('upcoming_events', 'block_7');
  }

  // Secondary Location.
  if ($variables['node']->type == 'seconday_location_page') {
    $variables['secondary_city_link_to_primary_city'] = views_embed_view('secondary_city_link_to_primary_city', 'block_1');
    $variables['upcoming_classes'] = views_embed_view('all_classes_for_a_secondary_location', 'block');

    $variables['main_content'] = [];
    $variables['main_content']['upcoming_events']['#markup'] = views_embed_view('secondary_location_upcoming_events', 'block_1');
    $variables['main_content']['upcoming_events']['#class'] = 'upcoming-classes mb-5';
  }

  // Course.
  if (isset($variables['node']) && $variables['node']->type == 'course') {
    $cat_short_name = NULL;
    if (!empty($variables['node']->field_primary_category[LANGUAGE_NONE][0]['target_id'])) {
      $cat_node = node_load($variables['node']->field_primary_category[LANGUAGE_NONE][0]['target_id']);
      $cat_short_name = $cat_node->field_short_category_name[LANGUAGE_NONE][0]['value'];
    }
    if (isset($variables['content']['field_in_our_classroom_text'])) {
      $markup = $variables['content']['field_in_our_classroom_text'][0]['#markup'];
      $variables['content']['field_in_our_classroom_text'][0]['#markup'] = '<h3>' . $cat_short_name . ' Classes in Our Classroom</h3>' . $markup;
    }
    if (isset($variables['content']['field_training_online_text'])) {
      $markup = $variables['content']['field_training_online_text'][0]['#markup'];
      $variables['content']['field_training_online_text'][0]['#markup'] = '<h3>' . $cat_short_name . ' Classes Online</h3>' . $markup;
    }

    $links[] = ['data' => l("See Course Topics", current_path(), ["fragment" => "course-topics", "attributes" => ["class" => ["btn btn-sm btn-primary mx-1"]]]), 'class' => ['list-inline-item']];
    $links[] = ['data' => l("See Course Dates", current_path(), ["fragment" => "see-course-dates", "attributes" => ["class" => ["btn btn-sm btn-primary mx-1"]]]), 'class' => ['list-inline-item']];
    $links[] = ['data' => l("Contact AGI about this course", "/contact", ["attributes" => ["class" => ["btn btn-sm btn-primary mx-1"]]]), 'class' => ['list-inline-item']];

    if (!empty($variables['field_course_pdf'])) {
      $links[] = [
        'data' => l("Download Course Details (PDF)", file_create_url($variables['field_course_pdf'][LANGUAGE_NONE][0]['uri']), ["attributes" => ["class" => ["btn btn-sm btn-primary mx-1"], "target" => "_blank"]]),
        'class' => ['list-inline-item']
      ];
    }

    $variables['content']['in_page_nav'] = [
      '#weight' => (isset($variables['content']['field_desc']['#weight']) ? $variables['content']['field_desc']['#weight'] : NULL)
    ];
    $variables['content']['in_page_nav'][0] = [
      '#markup' => theme('item_list', [
        'items' => $links,
        'attributes' => ['class' => ['list-inline secondary-navigation']]
      ])
    ];

    // Related Certificate Programs.
    if (!empty($variables['node']->field_certificate_programs)) {
      $variables['content']['related_certificate_programs'] = [
        '#weight' => $variables['content']['field_desc']['#weight'] + 1
      ];
      $programs = [];
      foreach ($variables['node']->field_certificate_programs[LANGUAGE_NONE] as $item) {
        $programs[] = l($item['entity']->title, drupal_get_path_alias("node/" . $item['entity']->nid));
      }
      $variables['content']['related_certificate_programs'][0] = [
        [
          '#markup' => '<em>This course is available individually or as part of these certificate programs:</em>'
        ],
        [
          '#markup' => theme('item_list', [
            'items' => $programs,
            'attributes' => ['class' => ['related-certificate-programs']]
          ])
        ]
      ];
    }
  }

  // Course Date.
  if ($variables['node']->type == 'course_date') {
    $variables['course_date_detail'] = views_embed_view('course_date_detail_panel_content', 'panel_pane_1');
  }

  // Certificate Program.
  if ($variables['node']->type == 'certificate_program') {
    $description = field_view_field('node', $variables['node'], 'field_cp_description', ['label' => 'hidden']);
    $variables['description'] = render($description);
    $dates_cost = field_view_field('node', $variables['node'], 'field_cp_dates_cost', ['label' => 'hidden']);
    $variables['dates_cost'] = render($dates_cost);
    $courses = field_view_field('node', $variables['node'], 'field_cp_courses', [
      'label' => 'hidden',
      'type' => 'entityreference_label',
      'settings' => [
        'link' => 1
      ]
    ]);
    $variables['courses'] = render($courses);

    // Course details.
    $courses = [];
    if (!empty($variables['node']->field_cp_courses)) {
      foreach ($variables['node']->field_cp_courses[LANGUAGE_NONE] as $delta => $item) {
        $courses[] = [
          'nid' => $item['entity']->nid,
          'title' => $item['entity']->title,
          'description' => $item['entity']->field_course_abstract[LANGUAGE_NONE][0]['value']
        ];
      }
    }
    $variables['courses_details'] = $courses;
    $variables['locations'] = views_embed_view('cp_locations_list', 'block');
  }

  // Certificate Program Location.
  if ($variables['node']->type == 'certificate_program_location_pag') {
    $variables['location'] = $variables['field_cp_loc_iocation'][0]['entity']->field_city_for_reference_pages[LANGUAGE_NONE][0]['value'];
    $variables['program'] = $variables['field_cp_loc_program'][0]['entity']->title;

    if (!empty($variables['node']->field_cp_loc_program) && !empty($variables['node']->field_cp_loc_program[LANGUAGE_NONE][0]['entity']->field_cp_courses)) {
      $program_node = $variables['node']->field_cp_loc_program[LANGUAGE_NONE][0]['entity'];
      // Course details.
      $courses = [];
      foreach ($program_node->field_cp_courses[LANGUAGE_NONE] as $delta => $item) {
        $course = node_load($item['target_id']);
        $courses[] = [
          'nid' => $course->nid,
          'title' => $course->title,
          'description' => $course->field_course_abstract[LANGUAGE_NONE][0]['value']
        ];
      }

      $variables['course_details'] = $courses;
    }

    $variables['locations'] = views_embed_view('cp_locations_list', 'block');
  }

  // Custom Training.
  if ($variables['nid'] == '8') {
    $block = module_invoke('webform', 'block_view', 'client-block-1752');
    $variables['form'] = render($block['content']);
  }

  // DC Books.
  if ($variables['node']->type == 'dc_book') {
    $variables['book_detail'] = views_embed_view('dc_book_detail', 'panel_pane_2');
    $variables['book_detail_2'] = views_embed_view('dc_book_detail', 'panel_pane_1');
    $variables['dcb'] = views_embed_view('dcb_errata_pages', 'panel_pane_1');
    $variables['buy'] = views_embed_view('dc_book_detail', 'panel_pane_3');
  }
}

function agi_preprocess_block(&$variables) {
  $block = $variables['block'];
  if ($block->delta == 'pri-block_2') {
    $variables['title_attributes_array']['class'][] = 'h2';
  }
}

function agi_preprocess_views_view(&$vars) {
  if ($vars['view']->name == 'reviews' && $vars['view']->current_display == 'block_5') {
    // Get the category location node.
    $cat_loc_node = node_load(arg(1));

    // Category label.
    $cat_label = $cat_loc_node->field_cat_for_location['und'][0]['entity']->title;

    // Location label.
    $location_value = '';
    if ($cat_loc_node->type == 'seconday_location_category_page') {
      $location_value = $cat_loc_node->field_secondary_location[LANGUAGE_NONE][0]['entity']->field_address_city[LANGUAGE_NONE][0]['safe_value'];
    }
    elseif (!empty($cat_loc_node->field_location_for_cat)) {
      $location_nid = $cat_loc_node->field_location_for_cat[LANGUAGE_NONE][0]['target_id'];
      // Private.
      if ($location_nid == "670") {
        $location_value = "Private";
      }
      // Online.
      elseif ($location_value == "1702") {
        $location_value = "Online";
      }
      elseif (!empty($cat_loc_node->field_location_for_cat['und'][0]['entity']->field_city_for_reference_pages)) {
        $location_value = $cat_loc_node->field_location_for_cat['und'][0]['entity']->field_city_for_reference_pages['und'][0]['safe_value'];
      }
    }
    else {
      $location_value = $cat_loc_node->field_location_for_cat['und'][0]['entity']->field_city_for_reference_pages['und'][0]['safe_value'];
    }

    if (empty($location_value)) {
      $vars['header'] = t('<h3>Reviews of ' . $cat_label . ' Online</h3>');
    }
    elseif ($location_value == "Online") {
      $vars['header'] = t('<h3>Reviews of ' . $cat_label . ' Online</h3>');
    }
    elseif ($location_value == "Private") {
      $vars['header'] = t('<h3>Reviews of Private ' . $cat_label . '</h3>');
    }
    else {
      $vars['header'] = t('<h3>Reviews of ' . $cat_label . ' in ' . $location_value . '</h3>');
    }
  }
}

function agi_preprocess_panels_pane(&$vars) {
    // Update the description rich snippet on primary category location pages.
    if ($vars['pane']->panel == 'grey_bg_header_right_col' && $vars['pane']->subtype == 'node:field_full_address') {
        $short_category_name = $vars['display']->context['relationship_entity_from_field:field_cat_for_location-node-node_1']->data->field_short_category_name['und'][0]['safe_value'];
        $short_location_name = $vars['display']->context['relationship_entity_from_field:field_location_for_cat-node-node_1']->data->field_city_for_reference_pages['und'][0]['safe_value'];

        $orig_string = '<div itemprop="description" style="margin-top:20px; font-style:italic">';
        $new_string = '<div itemprop="description" style="margin-top:20px; font-style:italic">' . $short_category_name . ' Classes and ' . $short_category_name . ' Training in ' . $short_location_name . '. ';

        $new_rich_description = str_replace($orig_string, $new_string, $vars['content'][0]['#markup']);

        $vars['content'][0]['#markup'] = $new_rich_description;
    }
}


function agi_preprocess_webform_form(&$vars) {
  // Customize the certificate program application webform.
  if ($vars['form']['#form_id'] == 'webform_client_form_76576') {
    drupal_add_css(drupal_get_path('theme', 'agi').'/assets/bootstrap/css/bootstrap.css');
    $vars['form']['actions']['submit']['#attributes']['class'][] = 'btn btn-primary-submit';
    $vars['form']['actions']['#attributes']['class'][] = 'col-xs-12';
  }

  // Add Bootstrap classes to webform components.
  if (isset($vars['form']['submitted'])) {
    $children = element_children($vars['form']['submitted']);
    foreach ($children as $child) {
      if ($vars['form']['submitted'][$child]['#type'] == 'textfield') {
        $vars['form']['submitted'][$child]['#attributes']['class'][] = 'form-control';
      }
      if ($vars['form']['submitted'][$child]['#type'] == 'webform_email') {
        $vars['form']['submitted'][$child]['#attributes']['class'][] = 'form-control';
      }
    }
  }
}

/**
 * Get the url of the news page for a given category.
 *
 * @param $cat_nid
 * @return string
 */
function get_news_url($cat_nid) {
  $query = db_query('
        SELECT rc.entity_id
        FROM field_data_field_related_course_category rc
        WHERE rc.field_related_course_category_target_id = :cat_id', array(':cat_id' => $cat_nid)
  );
  $term_id = $query->fetchField();
  $term_url = drupal_get_path_alias('taxonomy/term/' . $term_id);
  if (isset($term_url)) {
    return $term_url;
  }
  else {
    return 'design-news';
  }
}

/**
 * Override theme_quiz_progress().
 * @see quiz.pages.inc
 */
function agi_quiz_progress($variables) {
  $question_number = $variables['question_number'];
  $num_of_question = $variables['num_questions'];
  // TODO Number of parameters in this theme funcion does not match number of parameters found in hook_theme.
  // Determine the percentage finished (not used, but left here for other implementations).
  //$progress = ($question_number*100)/$num_of_question;

  // Get the current question # by adding one.
  $current_question = $question_number + 1;

  if ($variables['allow_jumping']) {
    $current_question = theme('quiz_jumper', array('current' => $current_question, 'num_questions' => $num_of_question));
  }

  $output  = '';
  $output .= '<div id="quiz_progress">';
  $output .= t('<span class="quiz-progress-text d-flex"><span class="quiz-label">Question</span><span id="quiz-question-number" class="px-1">!x</span> of <span id="quiz-num-questions" class="px-1">@y</span></span>', array('!x' => $current_question, '@y' => $num_of_question));
  $output .= '</div>' . "\n";
  // Add div to be used by jQuery countdown
  if ($variables['time_limit']) {
    $output .= '<div class="countdown"></div>';
  }
  return $output;
}

/**
 * Override theme_multichoice_response
 *
 * @see multichoice.theme.inc
 */
function agi_multichoice_response($variables) {
  $items = [];
  foreach ($variables['data'] as &$alternative) {
    if ($alternative['is_chosen']) {
      $items[] = t('<span class="!correct">!choice</span>', array(
        '!correct' => $alternative['is_correct'] ? 'correct' : 'incorrect',
        '!choice' => strip_tags($alternative['answer']),
      ));
    }
  }

  return theme('item_list', array('items' => $items));
}

/**
 * Override theme_quiz_admin_summary
 *
 * @see quiz.admin.inc
 */
function agi_quiz_admin_summary($variables) {
  global $user;
  $quiz = $variables['quiz'];
  $quiz_type = $quiz->field_quiz_type[LANGUAGE_NONE][0]['value'];
  $questions = $variables['questions'];
  $score = $variables['score'];

  // To adjust the title uncomment and edit the line below:
  // drupal_set_title(check_plain($quiz->title));
  if (!$score['is_evaluated']) {
    drupal_set_message(t('This quiz has not been scored yet.'), 'warning');
  }
  // Display overall result.
  $output = '';

  // Display Scores differently for quiz type.
  $score_summary = '';
  switch ($quiz_type) {
    default:
      $pass = $score['percentage_score'] >= $quiz->pass_rate ? TRUE : FALSE;
      $score_summary .= '<div class="quiz_score_percent' . ($pass ? ' pass' : ' fail') . '">' . t('@score% @pass', array('@pass' => $pass ? 'Pass' : 'Fail', '@score' => $score['percentage_score'])) . '</div>' . "\n";
      break;
  }

  // Get the feedback for all questions.
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'quiz') . '/quiz.pages.inc';
  $allow_scoring = quiz_allow_score_access($quiz);
  $report_form = drupal_get_form('quiz_report_form', $questions, TRUE, TRUE, $allow_scoring);
  $report_form['pass'] = $pass;
  $report_form['score_summary'] = $score_summary;
  $output .= drupal_render($report_form);
  return $output;
}

/**
 * Override theme_quiz_user_summary
 *
 * @see quiz.pages.inc
 */
function agi_quiz_user_summary($variables) {
  global $user;
  $quiz = $variables['quiz'];
  $quiz_type = $quiz->field_quiz_type[LANGUAGE_NONE][0]['value'];
  $questions = $variables['questions'];
  $score = $variables['score'];
  $rid = $variables['rid'];
  // Set the title here so themers can adjust.
  drupal_set_title($quiz->title);

  if (!$score['is_evaluated']) {
    if (user_access('score taken quiz answer')) {
      $msg = t('Parts of this @quiz have not been evaluated yet. The score below is not final. <a class="self-score" href="!result_url">Click here</a> to give scores on your own.', array('@quiz' => QUIZ_NAME, '!result_url' => url('node/' . $quiz->nid . '/results/' . $rid)));
    }
    else {
      $msg = t('Parts of this @quiz have not been evaluated yet. The score below is not final.', array('@quiz' => QUIZ_NAME));
    }
  }

  // Display overall result.
  $output = '';

  // Display Scores differently for quiz type.
  $score_summary = '';
  switch ($quiz_type) {
    default :
      $pass = $score['percentage_score'] >= $quiz->pass_rate ? TRUE : FALSE;
      $score_summary .= '<div class="quiz_score_percent' . ($pass ? ' pass' : ' fail') . '">' . t('@score% @pass', array('@pass' => $pass ? 'Pass' : 'Fail', '@score' => $score['percentage_score'])) . '</div>' . "\n";
      break;
  }

  // Get the feedback for all questions.
  $allow_scoring = quiz_allow_score_access($quiz);
  $form = drupal_get_form('quiz_report_form', $questions, FALSE, TRUE, $allow_scoring);
  $form['pass'] = $pass;
  $form['score_summary'] = $score_summary;
  $output .= drupal_render($form);
  return $output;
}

/**
 * Override theme_quiz_take_summary
 *
 * @see quiz.pages.inc
 */
function agi_quiz_take_summary($variables) {
  $quiz = $variables['quiz'];
  $quiz_type = $quiz->field_quiz_type[LANGUAGE_NONE][0]['value'];
  $questions = $variables['questions'];
  $score = $variables['score'];
  $summary = $variables['summary'];
  $rid =  $variables['rid'];
  // Set the title here so themers can adjust.
  drupal_set_title($quiz->title);

  // Display overall result.
  $output = '';

  // Display Scores differently for quiz type.
  $score_summary = '';
  if (!empty($score['possible_score'])) {
    if (!$score['is_evaluated']) {
      if (user_access('score taken quiz answer')) {
        $msg = t('Parts of this @quiz have not been evaluated yet. The score below is not final. <a class="self-score" href="!result_url">Click here</a> to give scores on your own.', array('@quiz' => QUIZ_NAME, '!result_url' => url('node/' . $quiz->nid . '/results/' . $rid)));
      }
      else {
        $msg = t('Parts of this @quiz have not been evaluated yet. The score below is not final.', array('@quiz' => QUIZ_NAME));
      }
      drupal_set_message($msg, 'warning');
    }

    switch ($quiz_type) {
      default :
        $pass = $score['percentage_score'] >= $quiz->pass_rate ? TRUE : FALSE;
        $score_summary .= '<div class="quiz_score_percent' . ($pass ? ' pass' : ' fail') . '">' . t('@score% @pass', array('@pass' => $pass ? 'Pass' : 'Fail', '@score' => $score['percentage_score'])) . '</div>' . "\n";
        break;
    }
  }
  if (isset($summary['passfail'])) {
    $score_summary .= '<div id="quiz_summary">' . $summary['passfail'] . '</div>' . "\n";
  }
  if (isset($summary['result'])) {
    $score_summary .= '<div id="quiz_summary">' . $summary['result'] . '</div>' . "\n";
  }
  // Get the feedback for all questions. These are included here to provide maximum flexibility for themers
  if ($quiz->display_feedback) {
    $form = drupal_get_form('quiz_report_form', $questions);
    $form['pass'] = $pass;
    $form['score_summary'] = $score_summary;
    $output .= drupal_render($form);
  }
  return $output;
}

/**
 * Implements hook_preprocess_picture().
 * @param $variables
 */
function agi_preprocess_picture(&$variables) {
  if (empty($variables['alt'])) {
    // Check if this is a node page.
    $node = menu_get_object();
    if ($node && is_object($node) && isset($node->nid)) {

      // If the alt tag is blank add the node title.
      if (isset($variables['alt']) || empty($variables['alt'])) {
        $title = $node->title == "Home" ? "American Graphics Institute" : $node->title;
        $variables['alt'] = $title;
        $variables['title'] = $title;
      }
    }
  }

  $variables['lazyload'] = TRUE;
}

/**
 * Implements hook_preprocess_THEME_NAME().
 */
function agi_preprocess_image_srcset(&$variables) {
  $variables['lazyload'] = TRUE;
}


// Helper function.
function get_node_title($nid) {
  return db_query('SELECT title FROM {node} WHERE nid = :nid', array(':nid' => $nid))->fetchField();
}

/**
 * Helper: Load the appropriate asset files.
 * @param $path
 */
function agi_load_libray() {
  $evo_css = drupal_get_path('theme', 'agi') . '/evo/build/style-evo.css';
  $evo_js = drupal_get_path('theme', 'agi') . '/evo/build/bundle.js';
  $css_options = ['group' => CSS_THEME, 'preprocess' => FALSE];
  $js_options = ['scope' => 'footer'];

  drupal_add_css($evo_css, $css_options);
  drupal_add_js($evo_js, $js_options);
}

/**
 * Helper function for setting the homepage variables.
 * @param $variables
 */
function _agi_front_page_variables(&$variables) {

  // Jump Class Finder.
  $variables['class_finder'] = jump_menu('menu-class-finder', 0, FALSE, 0, $choose = 'Find a Class');

  // Request Info form.
  $request_info = node_view(node_load('1694'));
  $variables['request_info'] = render($request_info);

  // Related News.
  $variables['recent_news'] =  views_embed_view('all_news_articles', 'block_5');

  // Copyright and Reviews.
  $variables['footer_copy_and_reviews'] = theme('footer_copy_and_reviews_summary');
}

/**
 * Override theme_menu_local_tasks().
 */
function agi_menu_local_tasks(&$variables) {
  $output = '';
  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="nav nav-tabs text-sm mb-3">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="nav nav-tabs text-sm mb-3">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }
  return $output;
}

/**
 * Override theme_menu_local_task().
 */
function agi_menu_local_task($variables) {
  $link = $variables['element']['#link'];
  $link_text = $link['title'];
  $link['localized_options']['attributes']['class'][] = 'nav-link';
  if (!empty($variables['element']['#active'])) {

    // Add text to indicate active tab for non-visual users.
    $active = '<span class="element-invisible">' . t('(active tab)') . '</span>';

    // If the link does not contain HTML already, check_plain() it now.
    // After we set 'html'=TRUE the link will not be sanitized by l().
    if (empty($link['localized_options']['html'])) {
      $link['title'] = check_plain($link['title']);
    }
    $link['localized_options']['html'] = TRUE;
    $link_text = t('!local-task-title!active', array(
      '!local-task-title' => $link['title'],
      '!active' => $active,
    ));
  }
  return '<li class="nav-item"' . (!empty($variables['element']['#active']) ? ' active' : '')  . '">' . l($link_text, $link['href'], $link['localized_options']) . "</li>\n";
}

/**
 * Override theme_image().
 * @param $variables
 * @return string
 */
function agi_image($variables) {
  $attributes = $variables['attributes'];
  $attributes['src'] = file_create_url($variables['path']);

  // Check if this is a node page.
  $node = menu_get_object();
  if ($node && is_object($node) && isset($node->nid)) {

    // If the alt tag is blank add the node title.
    if (isset($attributes['alt']) || empty($attributes['alt'])) {
      $title = $node->title == "Home" ? "American Graphics Institute" : $node->title;
      $attributes['alt'] = $title;
    }
  }

  foreach ([
             'width',
             'height',
             'alt',
             'title',
           ] as $key) {
    if (isset($variables[$key])) {
      $attributes[$key] = $variables[$key];
    }
  }
  return '<img' . drupal_attributes($attributes) . ' />';
}
