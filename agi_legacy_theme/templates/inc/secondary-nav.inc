<ul class="list-inline">
  <li class="list-inline-item"><a href="/about">About</a></li>
  <li class="list-inline-item"><a href="/contact">Contact</a></li>
  <li class="list-inline-item"><a href="/classes/locations">Locations</a></li>
  <li class="list-inline-item"><a href="/books">Books</a></li>
  <li class="list-inline-item"><a href="/classes/all-classes.html">All classes</a></li>
  <li class="list-inline-item"><a href="/ux/classes"><span class="mobile-upper">UX</span> classes</a></li>

  <?php
  if ($products_in_shopping_cart > 0) {
    echo '<li class="cart-nav list-inline-item d-flex justify-content-between align-items-center"><a href="/cart">Cart </a><span class="cart-number badge badge-primary">' . $products_in_shopping_cart . '</span></li>';
  }
  else {
    echo '<li class="list-inline-item"><a href="/cart">Cart</a></li>';
  }
  if ($logged_in != "TRUE") {
    echo "<li class='list-inline-item'><a href='/user' rel='nofollow'>Log in</a></li>";
  }
  else {
    echo "<li class='list-inline-item'><a href='/profile' rel='nofollow'>My account</a></li>";
    echo "<li class='list-inline-item'><a href='/user/logout' rel='nofollow'>Log out</a></li>";
  }
  ?>
  <li class="list-inline-item last-item search-item">
    <div class="search-wrapper">
      <div id="siteSearchBar">
        <?php print render($searchForm); ?>
      </div>
    </div>
  </li>
</ul>
