<div class="field-items"<?php print $item_attributes;
$current_url = url(current_path(), array('absolute' => TRUE));
if ($node = menu_get_object()) {
  $updated = $node->changed;
}
$datetimeFormat = 'Y-m-d H:i:s';
$date_formated = date($datetimeFormat, $updated);
?>>
  <?php foreach ($element['#items'] as $delta => $item): ?>
    <div class="field-item <?php print $delta % 2 ? 'odd' : 'even'; ?>"<?php print $item_attributes[$delta]; ?>>
      <script type="application/ld+json">
        {
          "@context": "https://schema.org",
          "@type": "QAPage",
          "mainEntity": {
            "@type": "Question",
            "name": "<?php print render($item['question']); ?>",
            "answerCount": 1,
            "dateCreated": "<?php print render($date_formated); ?>",
            "author": {
              "@type": "Organization",
               "name": "American Graphics Institute"
            },
            "acceptedAnswer": {
              "@type": "Answer",
              "text": "<?php print render($item['answer']); ?>",
              "dateCreated": "2016-11-02T21:11Z",
              "url": "<?php print $current_url ?>",
              "author": {
                "@type": "Organization",
                "name": "American Graphics Institute"
              }
            }
          }
        }
      </script>

      <p class="font-weight-bold mb-2"><?php print render($item['question']); ?></p>
      <p id="answer-<?php print $delta; ?>"><?php print render($item['answer']); ?></p>
    </div>
  <?php endforeach; ?>
</div>
