<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <div class="row">
    <div class="col-lg-8 offset-lg-4 min-height">
      <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_body']);
      ?>

      <div>
        <div class="intro">
          <p>Learn from an expert instructor without leaving your home or office with live online courses.&nbsp;</p>
          <div class="row flex-column-reverse flex-md-row">
            <div class="col-md-8">
              <ul>
                <li>Live instructors leading small-group online courses.</li>
                <li>Same expert instructors that teach our in-person classes.</li>
                <li>See the instructor&rsquo;s screen and ask questions in real-time.</li>
                <li>Hear and talk with the instructor using a provided headset or use your computer&rsquo;s audio.</li>
                <li>Instructors are the authors of <em>Dummies </em>and <em>Digital Classroom</em> books.</li>
                <li>Small class sizes: average class size is four with a limit of six participants in public courses.</li>
              </ul>
            </div>
            <div class="col-md-4">
              <div class="videos mb-3 mb-md-0">
                <a class="colorbox-inline" href="/?width=640&amp;height=360&amp;inline=true#agi-training-online-courses">
                    <img height="auto" src="/sites/all/themes/agi/assets/images/live-online-classes.png" width="100%"/>
                  </a>
                  <div class="text-sm">See how live online training works in this brief video.</div>
                <div style="display:none;">
                  <video controls="" height="360" id="agi-training-online-courses"
                         poster="/sites/all/themes/agi/assets/images/agi-video-cover.png" preload="none" width="100%">
                    <source src="http://d2cqpmwr4b912e.cloudfront.net/agi-training-online-courses.mp4"
                            type="video/mp4"></source>
                  </video>
                </div>
              </div>
            </div>
          </div>

          <div class="in-page-nav">
            <ul class="list-inline">
              <li class="list-inline-item first"><a href="#see-courses" class="btn btn-sm btn-primary mx-1">Available classes</a></li>
              <li class="list-inline-item"><a href="#how-it-works" class="btn btn-sm btn-primary mx-1">How it works</a></li>
              <li class="list-inline-item"><a href="#what-you-need" class="btn btn-sm btn-primary mx-1">What you need</a></li>
              <li class="list-inline-item last"><a href="#class-times" class="btn btn-sm btn-primary mx-1">Class times</a></li>
            </ul>
          </div>
        </div>

        <a id="see-courses"></a>
        <div class="categories">
          <h3><a href="/ux/classes/online"><img alt="user experience training classes"
                                         src="/sites/default/files/ux-training.png"
                                         style="width: 36px; height: 36px; float: left;"/> User
              Experience Classes Online</a></h3>
          <p>Discover the process and methods for creating stand-out websites and apps with our online user experience
            classes. Our live online UX training is delivered by the same instructors that teach UX workshops in our
            classrooms. Learn effective UX processes that help keep your projects on-track, and deliver a better
            experience for your users. Learn how UX can drive results, reduce costs, and improve revenues in these UX
            courses.</p>
          <h3><a href="/adobe/photoshop/classes/online"><img alt="photoshop training classes"
                                                      src="/sites/default/files/adobe-photoshop-training-36x36.png"
                                                      style="width: 36px; height: 36px; float: left;"/>Photoshop Classes Online</a>
          </h3>
          <p>Participate in any of these regularly scheduled Photoshop classes online, led by a live instructor. Learn
            from the authors of the books<em>&nbsp;Adobe Photoshop Digital Classroom&nbsp;</em>and&nbsp;<em>Adobe&nbsp;Creative
              Cloud for Dummies</em>. These live classes are small-group, and include hands-on exercieses and projects.
          </p>
          <h3><a href="/adobe/indesign/classes/indesign-training-classes-online"><img alt="indesign training classes"
                                                              src="/sites/default/files/adobe-indesign-training-36x36.png"
                                                              style="width: 36px; height: 36px; float: left;"/>InDesign
              Classes Online</a></h3>
          <p>Our InDesign courses online are led by a live instructor and help you quickly gain skills necessary for
            creating print and digital documents. Learn InDesign from authors that have&nbsp;written more than 10
            InDesign books and are exceptional at teaching. Learn from the same team of instructors Adobe selected to
            introduce InDesign to their clients around the world.</p>
          <h3><a href="/adobe/illustrator/classes/online"><img alt="illustrator training classes"
                                                        src="/sites/default/files/adobe-illustrator-training-36x36.png"
                                                        style="width: 36px; height: 36px; float: left;"/>Illustrator
              Classes Online</a></h3>
          <p>Our live online Illustrator courses are for all types of designers. Discover how to use Illustrator for
            creating products, packaging, models, branding, advertising and marketing artwork. Our instructors have
            written seven best-selling books covering Illustrator, including official books for Adobe Systems. These
            courses provide you with exercises, projects, and an expert instructor in a small-group online Illustrator
            course.</p>
          <h3><a href="/adobe/premiere-pro/classes/online"><img alt="premiere pro training classes"
                                                         src="/sites/default/files/adobe-premiere-pro-training-36x36.png"
                                                         style="width: 36px; height: 36px; float: left;"/>Premiere Pro
              Classes Online</a></h3>
          <p>Discover how to efficiently use the widely-used digital video editing app included with the Creative Cloud.
            Our live online Premiere Pro courses teach you skills for importing, editing, and creating complete
            projects. Our classes are led by the authors of the&nbsp;<em>Adobe Premiere Pro Digital Classroom</em>&nbsp;book.
          </p>
          <h3><a href="/adobe/after-effects/classes/online"><img alt="after effects training classes"
                                                          src="/sites/default/files/adobe-after-effects-training-36x36.png"
                                                          style="width: 36px; height: 36px; float: left;"/>After Effects
              Classes Online</a></h3>
          <p>Learn After Effects from experts in special effects and animation. Our team wrote the&nbsp;<em>After
              Effects Digital Classroom</em>&nbsp;book. You&#39;ll learn to create complex motion graphics and effects
            quickly with live online After Effects training led by expert instructors in small-group sessions.</p>
          <h3><a href="/adobe/dreamweaver/classes/online"><img alt="adobe dreamweaver training-classes"
                                                        src="/sites/default/files/adobe-dreamweaver-training-36x36.png"
                                                        style="width: 36px; height: 36px; float: left;"/>Dreamweaver
              Courses Online</a></h3>
          <p>Learn web design with introductory through advanced Dreamweaver training classes led by a live instructor.
            Our team wrote the<em>&nbsp;Adobe Dreamweaver Digital Classroom&nbsp;</em>book, so you&#39;ll be learning
            from experts. Benefit from their expertise with Dreamweaver and web design.</p>
          <h3><a href="/adobe/animate/classes/online"><img alt="adobe animate training classes"
                                                    src="/sites/default/files/animate-training-classes.png"
                                                    style="width: 36px; height: 36px; float: left;"/>Adobe Animate
              Courses Online</a></h3>
          <p>Discover easy ways to create interactive and animated content using Adobe Animate. Classes are led by our
            staff who have written the best-selling&nbsp;<em>Digital Classroom</em>&nbsp;book, and you&#39;ll work on
            modern HTML5 and CSS3 animations in this visual design tool.</p>
          <h3><a href="/adobe/acrobat/training/online"><img alt="adobe acrobat training classes"
                                                    src="/sites/default/files/Acrobat-XI-training-36x36.png"
                                                    style="width: 36px; height: 36px; float: left;"/>Acrobat &amp; PDF
              Courses Online</a></h3>
          <p>Learn all that you can do with Acrobat and PDF in these live online Acrobat courses. Discover how to
            secure, edit, and add comments to Adobe PDF files. Learn from &nbsp;instructors with 10 years of Acrobat
            experience who have written several best-selling books about Acrobat and the PDF format.</p>
          <h3><a href="/adobe/captivate/classes/online"><img alt="adobe captivate training classes"
                                                      src="/sites/default/files/captivate-36px.png"
                                                      style="width: 36px; height: 36px; float: left;"/>Captivate
              Training Courses Online</a></h3>
          <p>Our live online Captivate courses help you to quickly and efficiently create e-learning and recorded
            training content. In these Captivate workshops you&#39;ll gain hands-on skills you need to get
            up-and-running with practical skills.</p>
          <h3><a href="/adobe/creative-cloud-training/creative-cloud-classes-online"><img alt="creative cloud training classes"
                                                            src="/sites/default/files/adobe-creative-cloud-training-36.png"
                                                            style="width: 36px; height: 36px; float: left;"/>Creative
              Cloud Courses Online</a></h3>
          <p>Our Adobe Creative Cloud courses and Creative Suite classes help you master these design, interactive, and
            publishing applications.</p>
          <h3><a href="/adobe/captivate/classes/online"><img alt="apple keynote training classes"
                                                      src="/sites/default/files/keynote-36px.png"
                                                      style="width: 36px; height: 36px; float: left;"/>Apple Keynote Classes Online</a></h3>
          <p>Learn to quickly create compelling presentations that you can deliver using your Mac or iPad with our live
            online Keynote courses and online Keynote training programs.</p>
          <h3><a href="/apple/final-cut/classes/online"><img alt="final cut pro training classes"
                                                      src="/sites/default/files/finalcutpro-36px.png"
                                                      style="width: 36px; height: 29px; float: left;"/>Final Cut Pro
              Classes Online</a></h3>
          <p>You&#39;ll be quickly importing, editing, and rendering high-quality digital video projects with the skills
            you learn in our Final Cut Pro courses.</p>
          <h3><a href="/google/analytics/training/online"><img alt="Google Analytics Training Classes"
                                                        src="/sites/default/files/Google-Analtyics-Training-Classes.png"
                                                        style="width: 37px; height: 45px; float: left;"/>Google Analytics Training Courses Online</a></h3>
          <p style="clear: both;">Learn Google Analytics and discover who is visiting your site, how they arrived, and what they viewed. Our
            Google Analtytics training courses help you to quickly understand and measure what is happening on your
            website. Our live online Google Analytics courses are led by a live instructor, and you&#39;ll be able to
            work on your own Google Analytics account.</p>
          <h3><a href="/html/classes/online"><img alt="HTML training classes" src="/sites/default/files/web-design-courses.png"
                                           style="width: 36px; height: 36px; float: left;"/>HTML Classes Online</a></h3>
          <p>Learn HTML online and gain skills that are essential for modern design and development. These online HTML
            courses provide the foundation you need if you plan to create websites, marketing emails, or work with a
            design tool such as Dreamweaver, or a CMS such as Drupal, Joomla, or Wordpress.</p>
          <h3><a href="/html5/training/online"><img alt="HTML5 training classes" src="/sites/default/files/HTML5-training.png"
                                            style="width: 36px; height: 36px; float: left;"/>HTML5 Classes Online</a></h3>
          <p>Our online HTML5 courses give you the skills to create modern, interactive websites. You&#39;ll discover
            how to create modern forms, use web fonts, and understand ways to make the most of the modern web with HTML5
            and CSS3.</p>
          <h3><a href="/html-email/training/online"><img alt="HTML email training classes"
                                                 src="/sites/default/files/html-email-training-courses.png"
                                                 style="width: 36px; height: 36px; float: left;"/>HTML Email Courses Online</a>
          </h3>
          <p>Gain HTML email skills for modern marketing in these online HTML email classes. Start with foundational
            skills covering the essentials of HTML email marketing and design, then move to advanced HTML email training
            covering responsive email design.</p>
          <h3><a href="/css/training/online"><img alt="CSS training classes"
                                           src="/sites/default/files/css-training-classes.png"
                                           style="width: 36px; height: 36px; float: left;"/>CSS Training Classes Online</a>
          </h3>
          <p>Learn CSS for styling and formatting text and layouts. These online CSS courses include introductory
            through advanced CSS training to help you build your web design and development skills.</p>
          <h3><a href="/javascript/classes/online"><img alt="javascript training classes"
                                         src="/sites/default/files/javascript-training-classes.png"
                                         style="width: 36px; height: 36px; float: left;"/>JavaScript Training
              Courses Online</a></h3>
          <p>Learn JavaScript and jQueary in these online JavaScript workshops that teach essential skills. These
            JavaScript courses help you learn JavaScript and jQuery with an emphasis on practical web design and
            development.</p>
          <h3><a href="/sketchup-training-classes/online-sketchup-classes"><img alt="sketchup training classes"
                                                        src="/sites/default/files/sketchup-training-classes.png"
                                                        style="width: 36px; height: 36px; float: left;"/>Sketchup Training Courses Online</a></h3>
          <p>Find out how to use Sketchup efficiently to easily create designs for architecture, interior design, and
            product design. These live online Sketchup courses are offered monthly and provide a small-group way to
            effectively learn SketchUp.</p>
          <h3><a href="/web-design-classes/online"><img alt="web-design-training-classes"
                                                 src="/sites/default/files/wd-icon.png"
                                                 style="width: 36px; height: 36px; float: left;"/>Web Design Courses Online</a></h3>
          <p>Learn to create web designs that work well across desktop, laptop, tablet, and mobile devices in these live
            online courses. Our web design workshops teach skills needed for designing and building better websites.</p>
          <h3><a href="/web-accessibility-training-classes/web-accessibility-training-online"><img alt="web accessibility training classes"
                                                                 src="/sites/default/files/web-accessibility-training-classes.png"
                                                                 style="width: 36px; height: 36px; float: left;"/>Web Accessibility Training Courses Online</a></h3>
          <p>Learn to design with web accessibility in mind with these live online web accessibility courses delivered
            by expert instructors.</p>
          <h3><a href="/wordpress/classes/online"><img alt="wordpress training classes"
                                                 src="/sites/default/files/wordpress-logo-32-blue.png"
                                                 style="width: 32px; height: 32px; float: left;"/>WordPress Courses Online</a></h3>
          <p>Our live online WordPress classes teach you to create and manage sites using this popular content
            management system. Our introductory WordPress courses provide a fast and easy way to learn hands-on, and
            advanced training is available for site administrators.</p>

          <h3><a href="/digital-marketing-classes/online-digital-marketing-training"><img alt="Digital Marketing Classes Online"
                                                                                          src="/sites/all/themes/agi/assets/images/digital-marketing.svg"
                                                                                          style="width: 32px; height: 32px; float: left;"/> Digital Marketing Classes Online</a></h3>
          <p>Our Digital Marketing online classes teach modern day marketing techniques and skills.</p>

          <h3><a href="/graphic-design-classes/online-graphic-design-classes"><img alt="Digital Marketing Classes Online"
                                                                                   src="/sites/all/themes/agi/assets/images/web-design.svg"
                                                                                   style="width: 32px; height: 32px; float: left;"/> Graphic Design Classes online</a></h3>
          <p>Learn graphic design with live online classes teaching design techniques and digital design tools.</p>

          <h3><a href="/web-development/classes/online"><img alt="Digital Marketing Classes Online"
                                                             src="/sites/all/themes/agi/assets/images/web-development.svg"
                                                             style="width: 32px; height: 32px; float: left;"/> Web Development Classes Online</a></h3>
          <p>Learn web development through these online classes delivered by a live instructor. Gain modern day web coding skills through hands-on training delivered online.</p>

          <h3><a href="/search/site"><img alt="search for training classes"
                                                            src="/sites/default/files/search-for-classes.png"
                                                            style="width: 36px; height: 36px; float: left;"/>Search For Courses Online</a></h3>
          <p>American Graphics Institute offers more than 100 regularly scheduled public courses and many more as
            private or customized classes. Search our courses or call 781-376-6044 to speak with a representative.</p>
        </div>

        <a id="how-it-works"></a>
        <div class="row justify-content-start">
          <div class="col-lg-8">
            <div class="training-info">
              <div class="hidden-md hidden-lg">
                <p>
                  <video controls="" height="360" id="agi-training-online-courses"
                         poster="/sites/all/themes/agi/assets/images/agi-video-cover.png" preload="none" width="100%">
                    <source src="http://d2cqpmwr4b912e.cloudfront.net/agi-training-online-courses.mp4"
                            type="video/mp4"></source>
                  </video>
                  See how live online training works in this brief video
                </p>
              </div>
              <a id="class-times"></a>
              <h3>How online training works</h3>
              <h3>Class times</h3>
              <p>Most regularly scheduled online training classes start at 10:00 a.m. and end at 5:00 p.m. Eastern time.
                There is a one hour lunch break taken approximately at noon each day. With your class registration you
                will receive a web address and code to log-in and participate in your class. You may always call a
                training consultant at 781-376-6044 or 800-851-9237 if you have questions.</p>
              <h3>Custom classes &amp; private training</h3>
              <p>Custom classes and private training are available online and can be arranged to accommodate your
                schedule. Call a training consultant at 781-376-6044 to discuss details for private online training
                classes or complete our&nbsp;<a href="http://www.agitraining.com/classes/contact.html">contact form</a>.
              </p>
              <span id="what-you-need"></span>
              <h3>What you need for online training</h3>
              <p>The requirements for online training are relatively simple. If you are online viewing this page, you
                likely have everything you need to participate in an online training session.</p>
              <p>Computer: A Mac OS or Windows PC with a &nbsp;high-speed Internet connection.</p>
              <p>Applications: We recommend you install the relevant course applications prior to receiving training.
                For example, if you are taking a class on Photoshop, we recommend having Photoshop installed before the
                start of the training session. If you are uncertain as to what applications or tools you should have
                installed, ask us when registering or contact a training consultant at 781-376-6044.</p>
              <p>Speakers and Microphone: If your computer includes a speaker and microphone, you may use these to
                communicate with the instructor, or request a headset be included with your curriculum. We also provide
                a dial-in number if you prefer to use your phone.</p>
              <p>Books and curriculum:&nbsp;American Graphics Institute provides curriculum with all classes. Your
                curriculum will arrive a few days before your class is scheduled to begin. Many of our classes use&nbsp;<a
                  href="http://www.digitalclassroombooks.com/">Digital Classroom Books</a>&nbsp;written by the
                instructors at American Graphics Institute.</p>
            </div>
          </div>
        </div>
      </div>


    </div>
  </div>

</div>
