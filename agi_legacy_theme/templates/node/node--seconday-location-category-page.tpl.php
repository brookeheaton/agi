<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <div class="row">
    <div class="col-lg-8 offset-lg-4 min-height">

      <div class="pt-3">
        <?php if (isset($main_content) && count($main_content)) : ?>
          <?php foreach ($main_content as $block_id => $block) : ?>
            <div id="main-content-custom-block-<?php print $block_id; ?>" class="<?php print $block['#class']; ?>">
              <?php print render($block['#markup']); ?>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <h3>Learning <?php print $abbrev_cat_name; ?> in <?php print $city_name; ?></h3>
          <p>Learning <?php print $abbrev_cat_name; ?> is easier with the right
            instructor, resources, and support. American Graphics Institute
            offers <?php print $abbrev_cat_name; ?> training
            in <?php print $city_name; ?>. These professional development courses
            make it possible for individuals in <?php print $city_name; ?> to
            learn <?php print $abbrev_cat_name; ?>. You can benefit by
            participating in live <?php print $abbrev_cat_name; ?> classes led by
            the same instructors who teach <?php print $abbrev_cat_name; ?> at AGI
            locations, and can do this without needing to travel to a classroom.
            These regularly scheduled professional
            development <?php print $abbrev_cat_name; ?> courses occur in
            real-time and provide small-group sessions,
            making <?php print $abbrev_cat_name; ?> classes accessible to those in
            and around <?php print $city_name; ?>, <?php print $full_state_name; ?>. Enrollment for these
            public <?php print $abbrev_cat_name; ?> classes is available under the
            class dates section.</p>
          <h3>Private <?php print $abbrev_cat_name; ?> training
            in <?php print $city_name; ?></h3>
          <p>Groups, such as companies, organizations, and departments that need
            live, in-person <?php print $abbrev_cat_name; ?> training
            in <?php print $city_name; ?> can have a live, in-person instructor
            come on-site to their company. AGI can deliver
            a <?php print $abbrev_cat_name; ?> workshop
            in <?php print $city_name; ?> that is customized to your specific
            needs, or the training can be delivered using standard curriculum.
            Scheduling for private <?php print $abbrev_cat_name; ?> training
            in <?php print $city_name; ?> can occur on specific dates that align
            to your schedule, and the <?php print $abbrev_cat_name; ?> class time
            can also be adjusted to accommodate your organizations needs. Contact
            AGI to discuss options if you are interested in arranging
            private <?php print $abbrev_cat_name; ?> training.</p>

          <!-- Illustrator, InDesign, Photoshop, Premiere Pro, After Effects, XD -->
          <?php if (isset($additional_info)) : ?>
            <?php print $additional_info; ?>
          <?php endif; ?>

          <?php
          $body = field_get_items('node', $node, 'body');
          if (strlen($body[0]['value']) > 0) : ?>
            <div class="description">
              <?php print $body[0]['value'];
              ?>
            </div>
          <?php endif; ?>

        </div>
      </div>

    </div>
  </div>

  <div class="row">
    <div class="col">
      <div class="upcoming-classes mb-5">
        <?php print $upcoming_classes; ?>
      </div>
      <div class="additional-info">
        <?php print $additional_information; ?>
      </div>
    </div>
  </div>
</div>
