<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>


    <div class="col-lg-8 offset-lg-4 min-height">
      <?php if ($loc_city_short == "private") : ?>
        <h1>Private <?php print $abbrev_cat_name; ?> Classes</h1>
      <?php elseif (isset($category) && isset($category->field_adobe_cert_cat_page) && $category->field_adobe_cert_cat_page[LANGUAGE_NONE][0]['value'] == 1) : ?>
        <h1><?php print $node->title; ?></h1>
      <?php else : ?>
        <h1><?php print $abbrev_cat_name; ?> Classes <?php print $loc_city_short ?></h1>
      <?php endif; ?>

      <?php if ($loc_city_short == 'custom') : ?>
        <h2><?php print $abbrev_cat_name; ?> classes & training Online</h2>
      <?php elseif ($loc_city_short == 'Online') : ?>
        <h2><?php print $abbrev_cat_name; ?> classes & training Online</h2>
      <?php elseif (!empty($loc_city_short) && $loc_city_short != 'private') : ?>
        <h2><?php print $abbrev_cat_name; ?> classes & training in <?php print $loc_city_short; ?></h2>
      <?php endif; ?>

      <div class="intro"><?php print $intro; ?></div>

      <?php if (isset($secondary_navigation) && $loc_city_short != "private") : ?>
        <div class="secondary-navigation">
          <?php print $secondary_navigation; ?>
        </div>
      <?php endif; ?>

      <div class="training-offer mb-5">
        <?php if ($loc_city_short == "Online") : ?>
          <h2><?php print $abbrev_cat_name; ?> Classes <?php print $loc_city_short; ?></h2>
        <?php elseif ($loc_city_short == "private") : ?>
          <h2>Private <?php print $abbrev_cat_name; ?> Classes</h2>
        <?php else : ?>
          <h2><?php print $abbrev_cat_name; ?> Classes in <?php print $loc_city_short; ?> Offered</h2>
        <?php endif; ?>
        <p>Attend introductory to advanced <?php print $abbrev_cat_name; ?> Classes <?php print ($loc_city_short != "private" && $loc_city_short != "Online" ? "in " . $loc_city_short : ""); ?> in our training centers, online, or at your office. We have been teaching <?php print $abbrev_cat_name; ?> classes <?php print ($loc_city_short != "private" && $loc_city_short != "Online" ? "in " . $loc_city_short : ""); ?> for more than a decade and can help you to achieve professional results faster and more efficiently.</p>
        <div class="classes-offered mb-5">
          <?php print $classes_in_category; ?>
        </div>

        <a id="courses"></a>
        <div class="course-details mb-5">
          <?php if ($loc_city_short == "Online") : ?>
          <h2>Online <?php print $abbrev_cat_name; ?> Classes Course Details and Outlines</h2>
          <?php elseif ($loc_city_short == "private") : ?>
            <h2>Private <?php print $abbrev_cat_name; ?> Classes Course Details and Outlines</h2>
          <?php else : ?>
            <h2><?php print $abbrev_cat_name; ?> Classes in <?php print $loc_city_short; ?> Course Details and Outlines</h2>
          <?php endif; ?>
          <?php print $courses_in_category; ?>
        </div>

        <?php if (isset($about_cat) && !empty($about_cat)) : ?>
          <div class="about-category mb-5 flex">
            <?php if ($loc_city_short == "private") : ?>
              <h2>About Private UX Classes</h2>
            <?php elseif (strtolower($loc_city_short) == "online") : ?>
              <h2>About <?php print $abbrev_cat_name; ?> Classes <?php print $loc_city_short; ?></h2>
            <?php else : ?>
              <h2>About <?php print $abbrev_cat_name; ?> Classes in <?php print $loc_city_short; ?></h2>
            <?php endif; ?>
            <?php print $about_cat; ?>
          </div>
        <?php endif; ?>
      </div>
      <?php if ($loc_city_short != "private") : ?>
        <a id="see-course-dates"></a>
        <div class="upcoming-classes mb-5">
          <?php if ($loc_city_short == "Online") : ?>
            <h3>Upcoming Dates for Online <?php print $abbrev_cat_name; ?> Classes</h3>
          <?php elseif ($loc_city_short == "Private") : ?>
            <h3>Upcoming Dates for <?php print $abbrev_cat_name; ?> Classes in <?php print $loc_city_short; ?></h3>
          <?php else : ?>
            <h3>Upcoming Dates for <?php print $abbrev_cat_name; ?> Classes in <?php print $loc_city_short; ?></h3>
          <?php endif; ?>

          <?php print $upcoming_classes; ?>
        </div>
      <?php endif; ?>
    </div>
</div>
