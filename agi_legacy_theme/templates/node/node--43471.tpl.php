<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <div class="row">
    <div class="col-lg-8 offset-lg-4 min-height">
      <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_body']);
      ?>

      <h1 class="mb-3">Adobe Certification Training and Adobe Certification Classes</h1>

      <p><strong>Adobe certification training</strong> and <strong>Adobe certification classes</strong> at&nbsp;American
        Graphics Institute provide you with the skills needed for the Adobe certification exams. If you don&#39;t need
        Adobe certification you can select from any of the regularly scheduled&nbsp;<a href="/adobe">Adobe classes</a>
        offered at AGI or review the Adobe certification prep training programs below to prepare for specific Adobe
        certification exams.&nbsp;</p>
      <h3 dir="ltr">Adobe Certifications Available</h3>
      <p dir="ltr">There are two different Adobe Certifications offered for most applications. The foundation level
        Adobe certification is the Adobe Certified Associate exam. The Adobe ACA exam is suitable for most users of
        their applications and you can prepare for it with the introductory and intermediate Adobe courses at American
        Graphics Institute. The more advanced Adobe certification is the Adobe Certified Expert exam. This exam requires
        and in-depth, expert level knowledge of the application for which you are taking the Adobe certification test.
        This is useful for consultants and instructors.&nbsp;</p>
      <h3 dir="ltr">Do employers require Adobe Certification?</h3>
      <p dir="ltr">Most employers do not require Adobe certification, and many companies find a certificate of
        completion from an intensive course at American Graphics Institute to be a suitable proof of skills attained
        with the Adobe programs.</p>
      <h3 dir="ltr">About Certification Prep classes at American Graphics Institute</h3>
      <p dir="ltr">American Graphics Institute instructors have been hired by Adobe to write Adobe Certification exams
        and also have developed official Adobe Certification prep curriculum for both the Adobe ACA and ACE exams. Our
        instructors are uniquely qualified to help you prepare for Adobe Certification with our training classes and
        curriculum.</p>
      <h3 dir="ltr">Adobe Certification Classes Locations</h3>
      <p dir="ltr">American Graphics Institute offers Adobe certification training with a live instructor in the same
        classroom with you in Boston, New York City, and Philadelphia along with live Adobe Certification training prep
        programs online.</p>

      <h3><a href="/adobe/photoshop/classes/photoshop-certification-training-classes"><img
            alt="adobe photoshop training classes" src="/sites/default/files/adobe-photoshop-training-36x36.png"
            style="width: 36px; height: 36px; float: left;"/></a></h3>
      <h3><a href="/adobe/photoshop/classes/photoshop-certification-training-classes">Adobe Photoshop Certification
          Training</a></h3>
      <p>Live instructor-led Adobe certification classes for Photoshop. This includes public Adobe Photoshop
        certification training courses and private certification prep courses delivered by expert instructors. Learn
        from the team of instructors that have created official Adobe certification prep curriculum. There are many
        other <a href="/adobe/photoshop/classes">Photoshop classes</a> available in addition to those that prepare for
        the Adobe certification exams.</p>
      <h3><a href="/adobe/indesign/classes/indesign-certification-training-courses"><img
            alt="adobe indesign training classes" src="/sites/default/files/adobe-indesign-training-36x36.png"
            style="width: 36px; height: 36px; float: left;"/>Adobe InDesign Certification Training</a></h3>
      <p>Become highly skilled with creating print and digital publications with Adobe certification training for
        InDesign and InDesign certification prep courses. Learn InDesign from instructors that have written more than 10
        books for learning InDesign, have created official InDesign certification prep curriculum, and contributed to
        the development of the InDesign Certified Expert exam. Beyond the Adobe certification prep courses AGI offers
        many other <a href="/adobe/indesign/classes">InDesign training classes</a> as well.</p>
      <h3><a href="/adobe/illustrator/classes/illustrator-certification-training-courses"><img
            alt="adobe illustrator training classes" src="/sites/default/files/adobe-illustrator-training-36x36.png"
            style="width: 36px; height: 36px; float: left;"/>Adobe Illustrator Certification Training</a></h3>
      <p>Learn Adobe Illustrator and prepare for the Adobe Illustrator certification from professional designers who
        have written more than 10 books on Illustrator. Prepare for the Illustrator certification test with regularly
        scheduled public classes at our classrooms with a live instructor or online. AGI also offers many other <a
          href="/adobe/illustrator/classes">Illustrator training classes</a> beyond the certification prep training.</p>
      <h3><a href="/adobe/premiere-pro/classes/premiere-pro-certification-training-courses"><img
            alt="adobe premiere pro training classes" src="/sites/default/files/adobe-premiere-pro-training-36x36.png"
            style="width: 36px; height: 36px; float: left;"/>Adobe Premiere Pro Certification Training</a></h3>
      <p>Discover how to create high quality digital video with our Adobe certification workshops for Premiere Pro. In
        our Adobe Premiere Pro certification prep courses you will learn from expert digital video instructors that have
        been teaching Premiere Pro for a decade, and are able to help you prepare for this Adobe certification exam.
        Learn from the authors of the&nbsp;<em>Adobe Premiere Pro Digital Classroom</em>&nbsp;book. If you don&#39;t
        need Adobe certification for Premiere, AGI offers regularly scheduled public <a
          href="/adobe/premiere-pro/classes">Premiere Pro training courses.</a></p>
      <h3><a href="/adobe/after-effects/classes/after-effects-certification-training-courses"><img
            alt="adobe after effects training classes" src="/sites/default/files/adobe-after-effects-training-36x36.png"
            style="width: 36px; height: 36px; float: left;"/>Adobe After Effects Certification Training</a></h3>
      <p>The Adobe certification prep courses for After Effects provide an opportunity to learn from exceptional motion
        graphics and effects artists at AGI. These courses prepare you for the Adobe certification for After Effects
        while learning from the authors of the&nbsp;<em>Adobe After Effects Digital Classroom</em>&nbsp;book. You can
        also take <a href="/adobe/after-effects/classes">After Effects courses</a> at AGI without preparing for the
        Adobe certification.</p>
      <h3><a href="/adobe/dreamweaver/classes/dreamweaver-certification-training-courses"><img
            alt="adobe dreamweaver training classes" src="/sites/default/files/adobe-dreamweaver-training-36x36.png"
            style="width: 36px; height: 36px; float: left;"/>Adobe Dreamweaver Certification Training</a></h3>
      <p>Learn the skills needed to prepare for the Adobe certification for Dreamweaver. Classes led by the authors of
        the<em>&nbsp;Adobe Dreamweaver Digital Classroom&nbsp;</em>book who have a decade of experience teaching
        Dreamweaver help you get ready for this Adobe certification.</p>
      <h3><a href="/adobe/flash/classes/flash-certification-training-courses"><img alt="adobe flash training classes"
                                                                                   src="/sites/default/files/adobe-flash-training-36x36.png"
                                                                                   style="width: 36px; height: 36px; float: left;"/>Adobe
          Flash (Animate) Certification Training</a></h3>
      <p>From animations to video, Flash (Adobe Animate) remains an important multimedia delivery format. While Adobe is
        renaming this tool Animate, the Adobe certification for Flash remains and you can prepare for it with classes at
        AGI. You learn from instructors who have written the best-selling&nbsp;<em>Adobe Flash Digital Classroom</em>&nbsp;book.
      </p>
      <h3><a href="/adobe/acrobat/classes/acrobat-certification-training-courses"><img
            alt="adobe acrobat training classes" src="/sites/default/files/Acrobat-XI-training-36x36.png"
            style="width: 36px; height: 36px; float: left;"/>Adobe Acrobat Certification Training</a></h3>
      <p>The skills of creating, securing, annotating, editing, and revising Adobe PDF files are part of the Adobe
        certification training for Acrobat. The instructors at AGI have been using and teaching Adobe Acrobat for more
        than 10 years, have written about Adobe Acrobat in multiple books, and created the Adobe Acrobat and PDF
        Conference. When you prepare for the Adobe certification for Acrobat with AGI you are working with highly
        skilled professionals.</p>
      <h3><a href="/adobe/creative-cloud-training/creative-cloud-certification-training-courses"><img
            alt="adobe creative cloud training classes" src="/sites/default/files/adobe-creative-cloud-training-36.png"
            style="width: 36px; height: 36px; float: left;"/>Adobe Creative Cloud Certification Training</a></h3>
      <p>Our Adobe certification classes for the Creative Cloud cover all design, video, and web applications. AGI
        provides regularly scheduled Adobe Certification classes for Creative Cloud. These Adobe certification training
        classes are available in our classrooms, as live on-line courses, as well as private and customized programs,
        and on-demand learning options.</p>
      <div><h3><a href="/adobe/digital-publishing-suite/adobe-dps-certification-prep-training"><img
              alt="adobe dps certification training classes" src="/sites/default/files/adobe-dps-training-36.png"
              style="width: 36px; height: 36px; float: left;"/>Adobe Digital Publishing Suite (DPS) Certification
            Training</a></h3>
        <p>The Adobe DPS certification training prepare you to create digital publications using Adobe Digital
          Publishing Suite while you prep for the certification test.</p></div>
      <div><h3><a href="/adobe/muse/classes/adobe-muse-certification-training"><img
              alt="adobe muse certification training classes"
              src="/sites/default/files/adobe-muse-training-courses_1.png"
              style="width: 36px; height: 36px; float: left;"/>Adobe Muse Certification Training</a></h3>
        <p>The Adobe Must certification prep classes at American Graphics Institute provide an opportunity for you to
          learn to create responsive websites and prepare for the certification exam.</p></div>
      <div><h3><a href="/adobe/captivate/classes/adobe-captivate-certification-prep-training"><img
              alt="adobe captivate certification training" src="/sites/default/files/captivate-training-classes.png"
              style="width: 36px; height: 36px; float: left;"/>Adobe Captivate Certification Training</a></h3>
        <p>Our Adobe Captivate certification classes provide e-learning professionals with the opportunity to obtain the
          skills needed for this certification exam.</p>
      </div>
    </div>
  </div>
</div>
