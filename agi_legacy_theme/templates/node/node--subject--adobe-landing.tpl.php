<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <div class="row">
    <div class="col-lg-8 offset-lg-4 min-height">
      <h2><?php print $node->title; ?> from the experts</h2>
      <div class="field-body">
        <?php
        $body = field_view_field('node', $node, 'body', ['label' => 'hidden']);
        print render($body);
        ?>
      </div>

      <?php if (isset($secondary_navigation)) : ?>
        <div class="secondary-navigation">
          <?php print $secondary_navigation; ?>
        </div>
      <?php endif; ?>

      <div class="classes-offered mb-3">
        <h3><?php print $node->title; ?> Offered</h3>
        <p>American Graphics Institute specializes in Adobe training, offering public Adobe courses monthly in our
          classrooms led by a live instructor in the same room. Private and online Adobe classes are also available.</p>
        <h3>Most Popular Adobe Classes</h3>
        <div class="views-field-field-courses-in-category">
          <ul>
            <li>
              <div><strong>Adobe Class</strong></div>
              <div><strong>Course Dates</strong></div>
            </li>
            <li>
              <div><a href="/adobe/indesign/classes">InDesign</a></div>
              <div><a href="/adobe/indesign/classes#see-course-dates">Schedule</a></div>
            </li>
            <li>
              <div><a href="/adobe/photoshop/classes">Photoshop</a></div>
              <div><a href="/adobe/photoshop/classes#see-course-dates">Schedule</a></div>
            </li>
            <li>
              <div><a href="/adobe/illustrator/classes">Illustrator</a></div>
              <div><a href="/adobe/illustrator/classes#see-course-dates">Schedule</a></div>
            </li>
            <li>
              <div><a href="/adobe/creative-cloud-training">Creative Cloud</a></div>
              <div><a href="/adobe/creative-cloud-training#see-course-dates">Schedule</a></div>
            </li>
            <li>
              <div><a href="/adobe/captivate/classes">Captivate</a></div>
              <div><a href="/adobe/captivate/classes#see-course-dates">Schedule</a></div>
            </li>
            <li>
              <div><a href="/adobe/premiere-pro/classes">Premiere Pro</a></div>
              <div><a href="/adobe/premiere-pro/classes#see-course-dates">Schedule </a></div>
            </li>
            <li>
              <div><a href="/adobe/after-effects/classes">After Effects</a></div>
              <div><a href="/adobe/after-effects/classes#see-course-dates">Schedule</a></div>
            </li>
          </ul>
        </div>
        <p><em>See all Adobe courses below.</em></p>
      </div>

      <a id="courses"></a>
      <div class="course-details mb-5">
        <div class="views-row">
          <h3><a href="/adobe/photoshop/classes"><img alt="photoshop training classes" src="/sites/default/files/adobe-photoshop-training-36x36.png" style="width: 36px; height: 36px; margin-right: 15px; float: left;">Adobe Photoshop Training Courses</a></h3>
          <p>Regularly scheduled Adobe classes for Photoshop including public courses and private training delivered by
            expert instructors. Live, in-person <a href="/adobe/photoshop/classes/nyc">Photoshop classes in NYC</a> and
            other cities including <a href="/adobe/photoshop/classes/boston">Boston </a>and <a
              href="/adobe/photoshop/classes/philadelphia">Philadelphia </a>are available. Live <a
              href="/adobe/photoshop/classes/online">Photoshop classes online</a> are also led by a live instructor. Learn
            directly from the authors of the<em>&nbsp;Adobe Photoshop Digital Classroom&nbsp;</em>and <em>Adobe&nbsp;Creative
              Cloud for Dummies</em>.</p>
        </div>
        <div class="views-row views-row-odd">
          <h3><a href="/training/adobe/indesign-classes"><img alt="adobe indesign training classes" src="/sites/default/files/adobe-indesign-training-36x36.png" style="width: 36px; height: 36px; margin-right:15px; float: left;">Adobe InDesign Training Courses</a></h3>
          <p>Learn to create print and digital publications with our Adobe courses for InDesign. Our instructors have
            written more than 10 books covering InDesign, and we were selected by Adobe as the exclusive training partner
            to launch InDesign to their clients around the world. Attend live, in-person <a
              href="/adobe/indesign/classes/indesign-training-classes-nyc">InDesign classes in NYC</a>, or at training
            centers including <a href="/adobe/indesign/classes/indesign-classes-boston-ma">Boston</a> and <a
              href="/adobe/indesign/classes/indesign-training-classes-philadelphia-pa">Philadelphia</a>, or <a
              href="/adobe/indesign/classes/indesign-training-classes-online">InDesign classes online</a> are also led by
            live instructors.</p>
        </div>
        <div class="views-row">
          <h3><a href="/adobe/illustrator/classes"><img alt="adobe illustrator training classes" src="/sites/default/files/adobe-illustrator-training-36x36.png" style="width: 36px; height: 36px; float: left; margin-right:15px;">Adobe Illustrator Training Courses</a></h3>
          <p>Learn from professional designers and illustrators in our Adobe classes for Illustrator. We&#39;ve delivered
            Illustrator training to thousands of fashion designers, shoe designers, product designers, and artists. We
            offer live in-person classroom, online, and private Adobe Illustrator courses.</p>
        </div>
        <div class="views-row views-row-odd">
          <h3><a href="/adobe/premiere-pro/classes"><img alt="adobe premiere pro training classes" src="/sites/default/files/adobe-premiere-pro-training-36x36.png" style="width: 36px; height: 36px; float: left; margin-right:15px;">Adobe Premiere Pro Training Courses</a></h3>
          <p>Quickly and efficiently import, edit, and render high quality digital video with our Adobe workshops covering
            Premiere Pro. In our Adobe Premiere Pro courses you learn from instructors that have been teaching Premiere
            Pro for a decade, and are the authors of the&nbsp;<em>Adobe Premiere Pro Digital Classroom</em>&nbsp;book.</p>
        </div>
        <div class="views-row">
          <h3><a href="/adobe/after-effects/classes"><img alt="adobe after effects training classes" src="/sites/default/files/adobe-after-effects-training-36x36.png" style="width: 36px; height: 36px; float: left; margin-right:15px;">Adobe After Effects Training Courses</a></h3>
          <p>Create fantastic motion graphics and effects with ease using the skills you&#39;ll learn in our Adobe
            training for After Effects. You&#39;ll learn After Effects from digital video and motion graphics pros who are
            the authors of the&nbsp;<em>Adobe After Effects Digital Classroom</em>&nbsp;book. <a
              href="/after-effects-classes-nyc">After Effects Classes NYC</a>, <a
              href="/adobe/after-effects/classes/boston">After Effects courses in Boston</a>, and <a
              href="/adobe/after-effects/classes/philadelphia">After Effects Classes in Philadelphia</a> are all led by a
            live instructor in the same classroom with you. We also offer live online, and private Adobe training for
            After Effects on-site at your location.</p>
        </div>
        <div class="views-row views-row-odd">
          <h3><a href="/adobe/dreamweaver/classes"><img alt="adobe dreamweaver training classes" src="/sites/default/files/adobe-dreamweaver-training-36x36.png" style="width: 36px; height: 36px; float: left; margin-right:15px;">Adobe Dreamweaver Training Courses</a></h3>
          <p>Learn tools for web design our Adobe classes for Dreamweaver. Learn Adobe Dreamweaver in our classrooms,
            online, or on-site at your location. Our instructors wrote the<em>&nbsp;Adobe Dreamweaver Digital Classroom&nbsp;</em>book,
            and have been teaching Dreamweaver for more than a decade.</p>
        </div>
        <div class="views-row">
          <h3><a href="/adobe/muse/classes"><img alt="adobe muse training classes" src="/sites/default/files/adobe-muse-training-courses.png" style="width: 36px; height: 36px; float: left;margin-right:15px;">Adobe Muse Training Courses</a></h3>
          <p>The Adobe courses for Muse at AGI teach you how to build responsive websites using standards-based, modern
            HTML5 and CSS3 without coding. Discover how to use this web design tool in our Adobe workshops for Muse.</p>
        </div>
        <div class="views-row views-row-odd">
          <h3><a href="/adobe/flash/classes"><img alt="adobe animate training classes" src="/sites/default/files/adobe-animate-training-classes-1.png" style="width: 36px; height: 36px; float: left;margin-right:15px;">Adobe Animate Training Courses</a></h3>
          <p>From animations to video, Animate is an important multimedia tool that allows you to deliver in HTML5 / CSS3
            or Flash formats. Our Adobe classes for Flash help you create interactive and animated content for delivery to
            a variety of devices. Learn from instructors who have written the best-selling <em>Adobe Flash Digital
              Classroom</em> book.</p>
        </div>
        <div class="views-row">
          <h3><a href="/adobe/xd/classes"><img alt="adobe xd training classes" src="/sites/default/files/adobe-xd-training-classes-36.png" style="width: 36px; height: 36px; float: left; margin-right:15px;">Adobe XD Training Courses</a></h3>
          <p>Discover tools for efficiently prototyping apps and sites in this Adobe XD course. Learn Adobe Experience
            Design from instructors with real-world experience in the UX field.</p>
        </div>
        <div class="views-row views-row-odd">
          <h3><a href="/adobe/acrobat/classes"><img alt="adobe acrobat training classes" src="/sites/default/files/Acrobat-XI-training-36x36.png" style="width: 36px; height: 36px; float: left; margin-right:15px;">Adobe Acrobat Training Classes</a></h3>
          <p>Quickly create, secure, annotate, edit, and revise Adobe PDF files with the skills you&#39;ll gain in our
            Adobe training for Acrobat. Our instructors have been teaching Adobe Acrobat for more than 10 years, written
            about it in multiple books, and created the Adobe Acrobat and PDF Conference.</p>
        </div>
        <div class="views-row">
          <h3><a href="/adobe/creative-cloud/training"><img alt="adobe creative cloud training classes" src="/sites/default/files/adobe-creative-cloud-training-36.png" style="width: 36px; height: 36px; float: left;margin-right:15px;">Adobe Creative Cloud Training Classes</a></h3>
          <p>Our Adobe classes for the Creative Cloud tools cover all design, video, and web applications. We offer
            regularly scheduled Creative Cloud training workshops in our classrooms, live on-line courses, private and
            customized programs, and on-demand learning.</p>
        </div>
        <div class="views-row views-row-odd">
          <h3><a href="/adobe/captivate/classes"><img alt="adobe captivate training classes" src="/sites/default/files/captivate-36px.png" style="width: 36px; height: 36px; float: left; margin-right:15px;">Adobe Captivate Training Courses</a></h3>
          <p>Learn Adobe's tool for quickly and efficiently create e-learning and recorded training content. In our
            Adobe training for Captivate you'll gain skills you need to get up-and-running rapidly with practical
            skills.</p>
        </div>
        <div class="views-row">
          <h3><a href="/adobe/livecycle/training"><img alt="adobe livecycle training classes" src="/sites/default/files/livecycle-36pxx.png" style="width: 36px; height: 35px; float: left; margin-right:15px;">Adobe LiveCycle Designer Training Courses</a></h3>
          <p>Discover how to build dynamic forms and form workflows with our Adobe classes for LiveCycle Designer. In our
            Adobe LiveCycle Designer training which is available in our classrooms, at your location, or online we provide
            courses that are customized for your specific needs.</p>
        </div>
        <div class="views-row views-row-odd">
          <h3><a href="/adobe/acrobat/classes/connect"><img alt="adobe connect training classes" src="/sites/default/files/creative-cloud-training-36.png" style="width: 36px; height: 36px; float: left; margin-right:15px;">Adobe Connect Training Courses</a></h3>
          <p>If you're implementing an online meeting or online training site for your organization, our Adobe classes
            for Connect provide you and your team with the foundation skills they need to set-up and manage your Adobe
            Connect account. We tailor these courses to your exact needs so that you can get your account up and running
            quickly.</p>
        </div>
        <div class="views-row">
          <h3><a href="/adobe/incopy/training"><img alt="adobe incopy training classes" src="/sites/default/files/incopy-36px.png" style="width: 36px; height: 35px; float: left; margin-right:15px;">Adobe InCopy Training Courses</a></h3>
          <p>Our Adobe training for InCopy is the course for magazines, newspapers, newsletter publishers, catalog
            producers, and book publishers. Let us help create an efficient and productive editorial workflow as we put
            our extensive InCopy experience to work for you with our InCopy training.</p>
        </div>
        <div class="views-row views-row-odd">
          <h3><a href="/adobe/coldfusion/training"><img alt="adobe coldfusion training classes" src="/sites/default/files/coldfusion-36px_0.png" style="width: 36px; height: 36px; float: left; margin-right: 15px;">Adobe Coldfusion Training Courses</a></h3>
          <p>Learn to create dynamic websites and web content with our Adobe training courses for Coldfusion. We customize
            these courses to your specific needs, whether you are upgrading to a new version, or just starting your first
            Coldfusion site.</p>
        </div>
        <div class="views-row">
          <h3><a href="/adobe/adobe-certification-training"><img alt="adobe creative cloud certification training classes" src="/sites/default/files/creative-cloud-training-36.png" style="width: 36px; height: 36px; float: left; margin-right: 15px;">Adobe Certification Training Courses</a></h3>
          <p>The Adobe training programs prepare your for various Adobe Certification exams. Whether you are looking for
            an individual Adobe certification prep course or private Adobe certification training, there are a variety of
            Adobe classes and training offerings from AGI to help you prepare.</p>
        </div>
      </div>
      <div class="course-options mb-5">
        <h2>Adobe classes and Adobe training options</h2>
        <p>We have <?php print $node->title; ?> Classes for every schedule and budget.</p>

        <div class="row mb-3">
          <div class="col-md-6 d-flex justify-content-lg-start align-items-center">
            <svg class="feather-icon">
              <use xlink:href="#users">
            </svg>
            <a href="#">Public Adobe classes</a>
          </div>
          <div class="col-md-6 d-flex justify-content-lg-start align-items-center">
            <svg class="feather-icon">
              <use xlink:href="#lock">
            </svg>
            <a href="#">Private Adobe class</a>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6 d-flex justify-content-lg-start align-items-center">
            <svg class="feather-icon">
              <use xlink:href="#wifi">
            </svg>
            <a href="#">Online Adobe class</a>
          </div>
          <div class="col-md-6 d-flex justify-content-lg-start align-items-center">
            <svg class="feather-icon">
              <use xlink:href="#book">
            </svg>
            <a href="#">Adobe training books</a>
          </div>
        </div>
      </div>

      <a id="see-course-dates"></a>
      <div class="row">
        <div class="col">
          <div class="upcoming-classes mb-5">
            <h2>Upcoming Adobe classes </h2>
            <p>All classes in Boston, New York City, and Philadelphia start at 9:30 a.m. and end at 4:30 p.m. and are led by a live instructor in the same classroom with you. All online classes are led by a live instructor and run from 10:00 a.m. to 5:00 p.m. Eastern Time.</p>
            <?php print $upcoming_classes; ?>
            <?php print $related_certification_training ?? NULL; ?>
          </div>
          <div class="related-news mb-3">
            <h5>Recent <?php print $short_category_name; ?> Training News</h5>
            <?php print $related_news_to_a_category ?? NULL; ?>
          </div>
        </div>
      </div>

      <a id="see-locations-header"></a>
      <div class="classroom-locations mb-5">
        <h4>Adobe classes - Locations for Adobe training </h4>
        <p>We offer regularly scheduled, public <?php print $short_cat_name ?? NULL; ?> Classes at the following locations.</p>
        <div class="row">
          <div class="col-md-6"></div>
          <div class="col-md-6"></div>
        </div>
        <div class="row">
          <div class="col-md-6"><a href="/<?php print drupal_get_path_alias('node/179'); ?>">Boston: Adobe Classes Boston</a></div>
          <div class="col-md-6"><a href="/<?php print drupal_get_path_alias('node/918'); ?>">Chicago: Chicago Adobe classes</a></div>
        </div>
        <div class="row">
          <div class="col-md-6"><a href="/<?php print drupal_get_path_alias('node/258'); ?>">Philadelphia: Adobe Classes Philadelphia</a></div>
          <div class="col-md-6"><a href="/<?php print drupal_get_path_alias('node/970'); ?>">Orlando: Orlando Adobe classes</a></div>
        </div>
        <div class="row">
          <div class="col-md-6"><a href="/<?php print drupal_get_path_alias('node/257'); ?>">New York City: Adobe Classes NYC</a></div>
          <div class="col-md-6"><a href="/<?php print drupal_get_path_alias('node/969'); ?>">London: London Adobe training</a></div>
        </div>
        <div class="row">
          <div class="col-md-6"><a href="/<?php print drupal_get_path_alias('node/1722'); ?>">Online: Online Adobe Training</a></div>
          <div class="col-md-6"><a href="/adobe/adobe/classes/all-locations">All locations for Adobe training classes</a></div>
        </div>
        <div class="row">
          <div class="col-md-12"><a href="/all-tutorials">AGI also provides many free Adobe tutorials online.</a></div>
        </div>
      </div>
    </div>
  </div>
</div>
