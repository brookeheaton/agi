<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <div class="row">
    <div class="col-lg-8 offset-lg-4 min-height">
      <h2><?php print $node->title; ?> from the experts</h2>
      <div class="field-body">
        <?php
        $body = field_view_field('node', $node, 'body', ['label' => 'hidden']);
        print render($body);
        ?>
      </div>

      <?php if (isset($secondary_navigation)) : ?>
      <div class="secondary-navigation">
        <?php print $secondary_navigation; ?>
      </div>
      <?php endif; ?>

      <div class="training-offer mb-3">
        <h3><?php print $node->title; ?> Offered</h3>
        <p>Attend introductory to advanced <?php print $node->title; ?> in our training centers, online, or at your office.</p>
        <div class="classes-offered">
          <?php print $classes_in_category; ?>
        </div>
      </div>

      <?php if (!empty($related_certificate_programs)) : ?>
      <div class="related-certificate-programs mb-5">
        <?php print render($related_certificate_programs); ?>
      </div>
      <?php endif; ?>

      <?php
      // Adobe Cert Training Category Page.
      if (isset($node->field_adobe_cert_cat_page[LANGUAGE_NONE][0]['value']) && $node->field_adobe_cert_cat_page[LANGUAGE_NONE][0]['value'] == 1) : ?>
        <div class="custom-text">
          <h2>Adobe <?php print $node->field_short_category_name[LANGUAGE_NONE][0]['value']; ?> Certification Available</h2>
          <p>American Graphics Institute offers Adobe <?php print $node->field_short_category_name[LANGUAGE_NONE][0]['value']; ?> Certification Training in Boston, NYC, Philadelphia, and through live online classes. As the authors of more than 50 books on the Adobe tools and having helped Adobe create many certification exams, AGI is in a unique position to help you prepare for the Adobe Certification exams with training classes led by experts. Participants who take the introductory through advanced <?php print $node->field_short_category_name[LANGUAGE_NONE][0]['value']; ?> classes to prepare for a certification test may retake any of the courses at no cost for up to one full year until they have passed the certification.</p>
          <h3>Adobe <?php print $node->field_short_category_name[LANGUAGE_NONE][0]['value']; ?> Certified Expert ACE Training.</h3>
          <p>The introductory through advanced <?php print $node->field_short_category_name[LANGUAGE_NONE][0]['value']; ?> courses at American Graphics Institute prepare you for the <?php print $node->field_short_category_name[LANGUAGE_NONE][0]['value']; ?> ACE certification exam. This is the highest-level certification exam offered by Adobe. </p>
          <h3>Adobe <?php print $node->field_short_category_name[LANGUAGE_NONE][0]['value']; ?> Certified Associate Exam Training.</h3>
          <p>For the <?php print $node->field_short_category_name[LANGUAGE_NONE][0]['value']; ?> ACA certification exam the introductory <?php print $node->field_short_category_name[LANGUAGE_NONE][0]['value']; ?> course provides sufficient preparation for taking this certification exam. This is the foundational level exam for certification, and is useful for proving that you have an understanding of essential <?php print $node->field_short_category_name[LANGUAGE_NONE][0]['value']; ?> skills.</p>
          <h3>Adobe <?php print $node->field_short_category_name[LANGUAGE_NONE][0]['value']; ?> Certification Prep Training</h3>
          <p>Adobe <?php print $node->field_short_category_name[LANGUAGE_NONE][0]['value']; ?> Certification Prep training helps prepare you for the certification tests. Many employers also know that American Graphics Institute courses provide exceptional preparation and once you receive a certificate for your courses from AGI an extra certification test may not be needed due to the intensive preparation provided by the courses at American Graphics Institute.</p>
        </div>
      <?php else : ?>
        <div class="custom-text">
          <h2><?php print (isset($node->field_short_category_name[LANGUAGE_NONE][0]['value']) ? $node->field_short_category_name[LANGUAGE_NONE][0]['value'] : NULL); ?> Course Delivery Options</h2>
          <p>We have <?php print $node->title; ?>
            for every schedule and budget, led by expert instructors who help you to quickly and easily achieve professional results with your work.</p>
          <h3>Regularly Scheduled <?php print $node->title; ?></h3>
          <p>Our regularly scheduled, public <?php print $node->title; ?>
            include introductory through advanced training. Most courses are offered monthly. See the list of classes below.
            We offer live classes led by an instructor in the same classroom with you.</p>
          <h3>On-site <?php print $node->title; ?></h3>
          <p>All <?php print (isset($node->field_short_category_name[LANGUAGE_NONE][0]['value']) ? $node->field_short_category_name[LANGUAGE_NONE][0]['value'] : NULL); ?>
            courses can be delivered on-site at your location. Call or complete the form on the above link to receive information about classes at your location.
            We deliver on-site classes across the U.S. and globally.
          </p>
          <h3>Online <?php print $node->title; ?>
          </h3>
          <p>Live online <?php print (isset($node->field_short_category_name[LANGUAGE_NONE][0]['value']) ? $node->field_short_category_name[LANGUAGE_NONE][0]['value'] : NULL); ?>
            courses provide you with a small group course that you can take from anywhere with a high-speed Internet connection.
            You can ask questions, hear and talk with the instructor, even share your screen as part of these interactive classes.
          </p>
        </div>
      <?php endif; ?>

      <a id="courses"></a>
      <div class="course-details mb-5">
        <h2>Course Details and Outlines for <?php print $node->title; ?></h2>
        <p>Choose any of our <?php print $node->title; ?> below to see outlines, pricing and dates:</p>
        <?php print $courses_in_category; ?>
      </div>

      <a id="see-course-dates"></a>
      <div class="row">
        <div class="col">
          <div class="upcoming-classes mb-5">
            <?php print $upcoming_classes; ?>
          </div>
        </div>
      </div>

      <a id="see-locations-header"></a>
      <div class="classroom-locations mb-5">
        <h4>Locations for <?php print $node->title; ?></h4>
        <p><?php print $node->title; ?> are offered at the following locations. Most occur monthly with a live instructor in the same classroom:</p>
        <?php print $location_list; ?>
        <?php
        $text = field_view_field('node', $node, 'field_cat_left_sidebar_text', ['label' => 'hidden']);
        print render($text);
        ?>
        <?php print $all_locations_link; ?>
      </div>

      <?php if (isset($cat_additional_info)) : ?>
      <div class="cat-additional-info">
        <?php print $cat_additional_info; ?>
      </div>
      <?php endif; ?>

      <?php if (!isset($node->field_adobe_cert_cat_page[LANGUAGE_NONE][0]['value']) || $node->field_adobe_cert_cat_page[LANGUAGE_NONE][0]['value'] != 1) : ?>
        <?php if (strpos(drupal_get_path_alias(), "adobe") !== FALSE) : ?>

        </div>
        <?php endif; ?>
      <?php endif; ?>
    </div>
  </div>
</div>
