<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <div class="row">
    <div class="col-lg-8 offset-lg-4 min-height">
      <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_body']);
      ?>

      <div class="intro">
        <div class="intro">
          <p>Regularly scheduled courses for individuals and private classes for groups at American Graphics Institute&rsquo;s
            training centers. We&rsquo;ve been delivering training classes since 1995:</p>
          <ul>
            <li>Expert instructor in the same classroom with you</li>
            <li>Small class sizes: average class size is six, with a maximum of 10</li>
            <li>Hands-on learning: we supply your choice of a Mac or Windows PC</li>
            <li>Instructors are the authors of <em>Dummies</em> and <em>Digital Classroom</em> books</li>
          </ul>
        </div>

        <div class="in-page-nav">
          <ul class="list-inline">
            <li class="list-inline-item first"><a href="#locations" class="btn btn-sm btn-primary mx-1">Locations</a></li>
            <li class="list-inline-item"><a href="#class-times" class="btn btn-sm btn-primary mx-1">Class Times</a></li>
            <li class="list-inline-item"><a href="#see-courses" class="btn btn-sm btn-primary mx-1">Available Classes</a></li>
            <li class="list-inline-item last"><a href="#class-times" class="btn btn-sm btn-primary mx-1">Class Information</a></li>
          </ul>
        </div>


        <a id="see-courses"></a>
        <div class="categories">
          <h3><a href="/ux/classes"><img alt="user experience training classes"
                                         src="/sites/default/files/ux-training.png"
                                         style="width: 36px; height: 36px;"/>User Experience Classes</a></h3>
          <p>Discover the process and methods for creating stand-out websites and apps with our user experience classes,
            training, and workshops. Learn effective UX processes that help keep your projects on-track, and deliver a
            better experience for your users, which can drive results, reduce costs, and improve revenues with our UX
            courses and training workshops.</p>
          <h3><a href="/adobe/photoshop/classes"><img alt="photoshop training classes"
                                                      src="/sites/default/files/adobe-photoshop-training-36x36.png"
                                                      style="width: 36px; height: 36px;"/>Photoshop Classes</a>
          </h3>
          <p>We offer regularly scheduled Photoshop classes led by the authors of the<em>&nbsp;Adobe Photoshop Digital
              Classroom&nbsp;</em>and&nbsp;<em>Adobe&nbsp;Creative Cloud for Dummies</em>. Classes are led by
            professionals that are among the best digital imaging instructors in the world.</p>
          <h3><a href="/adobe/indesign/classes"><img alt="indesign training classes"
                                                     src="/sites/default/files/adobe-indesign-training-36x36.png"
                                                     style="width: 36px; height: 36px;"/>InDesign
              Classes</a></h3>
          <p>Our InDesign courses help you quickly gain the skills you need to create print and digital documents. Learn
            from authors that have&nbsp;written more than 10 InDesign books and you&#39;ll benefit from the same team of
            instructors Adobe selected to introduce InDesign to their clients around the world. Attend <a
              href="/adobe/indesign/classes/indesign-training-classes-nyc">InDesign classes in NYC</a>, at our training
            center in <a href="/adobe/indesign/classes/indesign-classes-boston-ma">Boston</a>, and <a
              href="/adobe/indesign/classes/indesign-training-classes-philadelphia-pa">Philadelphia</a>, or <a
              href="/adobe/indesign/classes/indesign-training-classes-online">InDesign classes online</a> led by a live
            instructor.</p>
          <h3><a href="/adobe/illustrator/classes"><img alt="adobe illustrator training classes"
                                                        src="/sites/default/files/adobe-illustrator-training-36x36.png"
                                                        style="width: 36px; height: 36px;"/>Illustrator
              Classes</a></h3>
          <p>Our Illustrator courses are for all types of designers - whether creating products, packaging, models,
            branding, or advertising. Our instructors have written seven best-selling books covering Illustrator,
            including official books for Adobe Systems.</p>
          <h3><a href="/adobe/premiere-pro/classes"><img alt="premiere pro training classes"
                                                         src="/sites/default/files/adobe-premiere-pro-training-36x36.png"
                                                         style="width: 36px; height: 36px;"/>Premiere Pro
              Classes</a></h3>
          <p>Discover how to use this digital video tool for importing, editing, and completing complete projects. Our
            trainers are the authors of the&nbsp;<em>Adobe Premiere Pro Digital Classroom</em>&nbsp;book.</p>
          <h3><a href="/adobe/after-effects/classes"><img alt="after effects training classes"
                                                          src="/sites/default/files/adobe-after-effects-training-36x36.png"
                                                          style="width: 36px; height: 36px;"/>After Effects
              Classes</a></h3>
          <p>Learn After Effects from the team who wrote the&nbsp;<em>After Effects Digital Classroom</em>&nbsp;book.
            You&#39;ll be creating complex motion graphics and effects quickly with training from our expert
            instructors.</p>
          <h3><a href="/adobe/dreamweaver/classes"><img alt="dreamweaver training classes"
                                                        src="/sites/default/files/adobe-dreamweaver-training-36x36.png"
                                                        style="width: 36px; height: 36px;"/>Dreamweaver
              Courses</a></h3>
          <p>Learn web design with introductory through advanced Dreamweaver training classes from the instructors who
            wrote the<em>&nbsp;Adobe Dreamweaver Digital Classroom&nbsp;</em>book. Benefit from their expertise with
            Dreamweaver, design, and development.</p>
          <h3><a href="/adobe/animate/classes"><img alt="adobe animate training classes"
                                                    src="/sites/default/files/animate-training-classes.png"
                                                    style="width: 36px; height: 36px;"/>Adobe Animate
              Courses</a></h3>
          <p>Discover easy ways to create interactive and animated content using Adobe Animate. Classes are led by our
            staff who have written the best-selling&nbsp;<em>Adobe Flash Digital Classroom</em>&nbsp;book.</p>
          <h3><a href="/adobe/acrobat/classes"><img alt="adobe acrobat training classes"
                                                    src="/sites/default/files/Acrobat-XI-training-36x36.png"
                                                    style="width: 36px; height: 36px;"/>Acrobat &amp; PDF
              Courses</a></h3>
          <p>Learn all that you can do with Acrobat and PDF in these courses. Discover how to secure, edit, and add
            comments to Adobe PDF files. Learn from &nbsp;instructors with 10 years of Acrobat experience who have
            written several best-selling books about Acrobat and the PDF format.</p>
          <h3><a href="/adobe/captivate/classes"><img alt="adobe captivate training classes"
                                                      src="/sites/default/files/captivate-36px.png"
                                                      style="width: 36px; height: 36px;"/>Captivate
              Training Courses</a></h3>
          <p>Our Captivate courses help you to quickly and efficiently create e-learning and recorded training content.
            In our Captivate workshops you&#39;ll gain hands-on skills you need to get up-and-running with practical
            skills.</p>
          <h3><a href="/adobe/creative-cloud/training"><img alt="creative cloud training classes"
                                                            src="/sites/default/files/adobe-creative-cloud-training-36.png"
                                                            style="width: 36px; height: 36px;"/>Creative
              Cloud Courses</a></h3>
          <p>Our Adobe Creative Cloud courses and Creative Suite classes help you master these design, interactive, and
            publishing applications.</p>
          <h3><a href="/apple/keynote/classes"><img alt="apple keynote training classes"
                                                    src="/sites/default/files/keynote-36px.png"
                                                    style="width: 36px; height: 36px;"/>Ap</a><a
              href="/apple/keynote/classes">ple Keynote Classes</a></h3>
          <p>Learn to quickly create compelling presentations that you can deliver using your Mac or iPad with our
            Keynote courses and Keynote training programs.</p>
          <h3><a href="/apple/final-cut/classes"><img alt="final cut pro training classes"
                                                      src="/sites/default/files/finalcutpro-36px.png"
                                                      style="width: 36px; height: 29px;"/>Final Cut Pro
              Classes</a></h3>
          <p>You&#39;ll be quickly importing, editing, and rendering high-quality digital video projects with the skills
            you learn in our Final Cut Pro courses.</p>
          <h3><a href="/google/analytics/training"><img alt="Google Analytics Training Classes"
                                                        src="/sites/default/files/Google-Analtyics-Training-Classes.png"
                                                        style="width: 37px; height: 45px;"/></a><a
              href="/google/analytics/training">Google Analytics Training Courses</a></h3>
          <p style="clear: both;">Learn Google Analytics and discover who is visiting your site, how they arrived, and what they viewed. Our
            Google Analtytics training courses help you to quickly understand and measure what is happening on your
            website.</p>
          <h3><a href="/html/classes"><img alt="HTML training classes" src="/sites/default/files/web-design-courses.png"
                                           style="width: 36px; height: 36px;"/>HTML Classes</a></h3>
          <p>Learn HTML skills that are essential for modern design and development. These courses provide the
            foundation you need if you plan to create websites, marketing emails, or work with a design tool such as
            Dreamweaver, or a CMS such as Drupal, Joomla, or Wordpress.</p>
          <h3><a href="/html5/classes"><img alt="HTML5 training classes" src="/sites/default/files/HTML5-training.png"
                                            style="width: 36px; height: 36px;"/>HTML5 Classes</a></h3>
          <p>Our HTML5 courses give you the skills to create modern, interactive websites. You&#39;ll discover how to
            create modern forms, use web fonts, and understand ways to make the most of the modern web with HTML5 and
            CSS3.</p>
          <h3><a href="/html-email/classes"><img alt="HTML email training classes"
                                                 src="/sites/default/files/html-email-training-courses.png"
                                                 style="width: 36px; height: 36px;"/>HTML Email Courses</a>
          </h3>
          <p>Gain HTML email skills for modern marketing in these HTML email classes. Start with foundational skills
            covering the essentials of HTML email marketing and design, then move to advanced HTML email training
            covering responsive email design.</p>
          <h3><a href="/css/training"><img alt="CSS training classes"
                                           src="/sites/default/files/css-training-classes.png"
                                           style="width: 36px; height: 36px;"/>CSS Training Classes</a>
          </h3>
          <p>Learn CSS for styling and formatting text and layouts. These CSS courses include introductory through
            advanced CSS training to help you build your web design and development skills.</p>
          <h3><a href="/javascript"><img alt="javascript training classes"
                                         src="/sites/default/files/javascript-training-classes.png"
                                         style="width: 36px; height: 36px;"/>JavaScript Training
              Courses</a></h3>
          <p>Learn JavaScript and jQueary in these workshops that provide essential skills. These JavaScript courses
            teach JavaScript and jQuery with an emphasis on practical web design and development.</p>
          <h3><a href="/sketchup-training-classes"><img alt="sketchup training classes"
                                                        src="/sites/default/files/sketchup-training-classes.png"
                                                        style="width: 36px; height: 36px;"/></a></h3>
          <h3><a href="/sketchup-training-classes">Sketchup Training Courses</a></h3>
          <p>Find out how to use Sketchup to create designs for architecture, interior design, and product design in
            these live Sketchup courses.</p>
          <h3><a href="/web-design-classes"><img alt="web design training classes"
                                                 src="/sites/default/files/wd-icon.png"
                                                 style="width: 36px; height: 36px;"/></a><a
              href="/web-design-classes">Web Design Courses</a></h3>
          <p>Learn to create web designs that work well across desktop, laptop, tablet, and mobile devices. Our web
            design courses provide the skills you need for designing and building better websites.</p>
          <h3><a href="/adobe/acrobat/classes/connect"><img alt="web accessibility training classes"
                                                            src="/sites/default/files/web-accessibility-training-classes.png"
                                                            style="width: 36px; height: 36px;"/></a><a
              href="/web-accessibility-training-classes">Web Accessibility Training Courses</a></h3>
          <p>Learn web accessibility in these live accessbility classes delivered by expert instructors.</p>
          <h3><a href="/web-design-classes"><img alt="wordpress training classes"
                                                 src="/sites/default/files/wordpress-logo-32-blue.png"
                                                 style="width: 32px; height: 32px;"/></a><a
              href="/wordpress/classes">WordPress Courses</a></h3>
          <p>Learn WordPress and how to work with and manage sites in this widely used content management system. Our
            introductory WordPress courses provide a fast and easy way to learn hands-on, and advanced training is
            available for site administrators.</p>
          <h3><a href="/adobe/acrobat/classes/connect"><img alt="search for training classes"
                                                            src="/sites/default/files/search-for-classes.png"
                                                            style="width: 36px; height: 36px;"/></a><a
              href="/search/site">Search For Courses</a></h3>
          <p>American Graphics Institute offers more than 100 regularly scheduled public courses and many more as
            private or customized classes. Search our courses or call 781-376-6044 to speak with a representative.</p>
        </div>


        <a id="class-times"></a>
        <div class="training-info mb-3">
          <h4>Training Class Information</h4>
          <p>Most regularly scheduled classroom training sessions start at 10:00 a.m. and end at 5:00 p.m. with one hour
            provided for lunch at approximately noon each day. Your class confirmation will list exact times, or call a
            training consultant at 781-376-6044 if you&nbsp;have questions about the class schedule.</p><h4>Custom
            Classes &amp; Private Training</h4>
          <p>AGI offers custom classes and private training to accommodate your schedule, address specific training
            topics, or train a group from your business or organization. Training can&nbsp;occur at your location or at
            our classroom training centers. For private training and custom classes, the location and times are
            customized to meet your specific needs. AGI can also provide a portable classroom of laptop computers for
            your group, if requested.</p><h4>What to Bring</h4>
          <p>If you are taking a class at one of AGI&#39;s classroom training centers:</p><h5>Computers</h5>
          <p>AGI&#39;s classroom training centers are equipped with computers running Mac OS and Windows operating
            systems, ready for your use and loaded with the applications used in the class. Let us know your preference
            at the time your register, and we&#39;ll have a system ready for you to use when you arrive for your
            training class. If you wish to use your own computer, please arrive 30 minutes before class to load lesson
            files, and let us know this at the time you register.</p><h5>Dress</h5>
          <p>Our classrooms are business casual. Dress comfortably. If you tend to be cold, you may wish to bring a
            sweater,&nbsp;sweatshirt, or light jacket.</p><h5>Books and Curriculum</h5>
          <p>AGI provides curriculum with all of our classes. Many of our classes use the&nbsp;<a
              href="http://www.digitalclassroombooks.com/">Digital Classroom Books</a>&nbsp;written by our instructors.
          </p>
        </div>

        <a id="locations"></a>
        <h3>Classroom Training Locations</h3>
        <div class="locations">
          <h5><a href="/locations/boston">AGI Training Boston</a></h5>
          <p>150 Presidential Way<br/>Woburn, MA 01801-1072<br/>(800) 851-9237</p><h5><a
              href="/locations/chicago">AGI Training Chicago</a></h5>
          <p>22 W. Washington Street, Suite 1500<br/>Chicago, IL 60602<br/>(800) 851-9237&nbsp;</p><h5><a
              href="/locations/nyc">AGI Training New York City</a></h5>
          <p>(Between 5th and 6th Ave)<br/>21 West 46th Street, 16th floor<br/>New York, NY 10036<br/>(212) 922-1206</p>
          <h5><a href="/locations/orlando">AGI Training Orlando</a></h5>
          <p>1800 Pembrook Drive, Suite 300<br/>Orlando, FL 32820<br/>(800) 851-9237</p><h5><a
              href="/locations/philadelphia">AGI Training Philadelphia</a></h5>
          <p>101 West Elm Street, Suite 300<br/>Conshohocken, PA 19428<br/>(800) 851-9237</p><h5><a
              href="/locations/london">AGI Training London</a></h5>
          <p>Corinthian House<br/>279 Tottenham Court Road<br/>London, W1T 7RJ</p>
        </div>

        <div class="discounted">
          <p><strong>Discounted Training for Groups or Multiple Courses</strong></p>
          <p>If you or your organization is considering multiple classes, consider our <a
              href="/training-vouchers-and-training-passes.html">training passes or training vouchers</a>.</p>
        </div>

      </div>

      <?php if (isset($form)) : ?>
        <div class="request-form">
          <?php print $form; ?>
        </div>
      <?php endif; ?>

      <div class="testimonials">
        <p><strong>Testimonials</strong></p>
        <p>Thousands of clients have provided testimonials about our training. We&#39;re happy to supply references for
          specific classes and industries.</p>
        <p>&ldquo;This was one of the best trainings that I have participated in. The whole group loved the
          training and benefited from it. The examples, the support, the multi-version exercises (with completed
          code), the accompanying textbook, the additional research, the hands-on aspect all contributed to an overall,
          extremely helpful session. On behalf of the Center for Online Learning, I thank you for such an
          outstanding experience.&rdquo;<br/><em>Director, Center for Online Learning</em><br/><em>Metropolitan State
            University</em></p>
        <p></p>
        <p>&ldquo;An excellent experience -- the customization of the training was truly impressive. Our designers
          are excited and ready to go.&rdquo;<br/><em>Business Manager, Design Ops</em><br/><em>Reebok
            International</em></p>
        <p></p>
        <p>&ldquo;AGI took the time to listen and develop a curriculum around our specific needs. The preparation
          and overall client service paid off with an exceptional training session. I will be recommending AGI. Thanks
          again!&rdquo;<br/><em>Publishing Systems Manager<br/>Staples</em></p>
        <p></p>
        <p>&ldquo;The staff are very personable, professional and accommodating. The facility is easily
          accessible, very modern, clean, comfortable, and the class size is perfect. The instructors are
          extremely sharp, they are great at explaining the program(s) in their simplest form and getting to the core of
          what each programs strengths and weaknesses are. They make the material very palatable and easily digestible.
          They include a comprehensive booklet with tutorial CD for each class. On top of everything else they make
          themselves accessible after the training class when we are back in our own world via email in case we get
          stuck or have any issues figuring something out.Kudos to AGI!&rdquo;<br/><em>Hrair A.</em></p>
      </div>

    </div>
  </div>

</div>
