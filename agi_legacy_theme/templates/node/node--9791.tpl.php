<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <div class="row">
    <div class="col-lg-8 offset-lg-4 min-height">
      <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_body']);
      ?>

      <div class="intro">
        <div class="intro">
          <p>A <strong>Certificate Program</strong> from American Graphics Institute provides you with the skills
            necessary for the modern workforce. These multi-week programs range from 60 to 100 hours of class time, and
            can be taken either part-time or full-time. If you are looking for individual classes, which also include
            certificates of completion, see our <a href="/classroom-training">public classes and workshops</a>.</p>
          <p>About our certificate programs:</p>
          <ul>
            <li>Study UX Design, Photoshop, Digital Marketing, Graphic Design, Video Editing, Web Design or Web Development.</li>
            <li>AGI has provided design and digital marketing courses to more than 50,000 individuals.</li>
            <li>American Graphics Institute is both licensed and bonded, with two decades of experience.</li>
            <li>Placement assistance available from dedicated career staff.</li>
            <li>Approved for GI Bill benefits. Eligible participants can use Veterans Affairs (VA) funds.</li>
          </ul>
          <p>Choose from any of the following certificate programs:</p>
        </div>

        <div class="categories">
          <h3><a href="/ux/classes/ux-certificate">
              <img alt="user-experience-training-classes"
                   src="/sites/default/files/ux-training.png"
                   style="width: 36px; height: 36px; float: left;"/></a><a
              href="/ux/classes/ux-certificate"> UX Certificate</a></h3>
          <div><p>The <a href="/ux/classes/ux-certificate">UX certification program</a> from American Graphics
              Institute prepares you for the role of a user experience designer or to become a more well-rounded product
              manager, business analyst, developer, or user interface designer. The UX program includes classes covering
              the processes for creating websites and apps across multiple form factors and platforms. Discover ways
              to improve efficiency while delivering a better user experience for end-users, increasing adoption of your
              apps or website, and reducing development and support costs. </p></div>
          <h3><img alt="HTML-training-classes" src="/sites/default/files/web-design-courses.png"
                   style="width: 36px; height: 36px; float: left;"/><a
              href="/certificates/digital-marketing-certificate">Digital
              Marketing Certificate</a></h3>
          <p>The <a href="/certificates/digital-marketing-certificate">Digital Marketing Certificate</a> program at
            American Graphics Institute provides skills needed for marketing roles that include digital design and
            analysis. In this program you&#39;ll learn necessary skills for implementing successful digital marketing
            programs. The digital marketing certificate courses help you learn skills ranging from creating HTML Email,
            to
            using Adobe design tools and Google Analytics to measure programs success.</p>
          <h3><a href="/certificates/web-design-certificate"><img alt="web-design-training-classes" src="/sites/default/files/wd-icon.png"
                                                                  style="width: 36px; height: 36px; float: left;"/>Web Design Certificate</a></h3>
          <p>The <a href="/certificates/web-design-certificate">Web Design Certificate</a> program prepares you for
            roles
            creating web designs for desktop, laptop, tablet, and mobile devices. Learn the skills necessary for modern
            web design, working with HTML, CSS, and related web design tools.</p>

          <h3><a href="/certificates/web-development-certificate"><img alt="web-development-training-classes" src="/sites/default/files/HTML5_training_0.png" style="width: 36px; height: 36px; float: left;"/>Web Development Certificate</a></h3>
          <p>The Web Development Certificate program teach modern web coding skills including introductory through advanced HTML, CSS, and JavaScript. Participants also learn to manage WordPress sites, the most commonly used website content management system (CMS).</p>

          <h3><a href="/certificates/graphic-design-certificate"><img alt="graphic-design-training-classes" src="/sites/default/files/pictures/pd-icon.png" style="width: 36px; height: 36px; float: left;"/>Graphic Design Certificate</a></h3>
          <p>Learn graphic design principles and modern design tools in this intensive program. You’ll learn to illustrate, edit images, create layouts and apply best design practices through coursework and projects.</p>

          <h3><a href="/certificates/video-editing-certificate"><img alt="web-development-training-classes" src="/sites/all/themes/agi/assets/images/video-editing.svg" style="width: 36px; height: 36px; float: left;"/>Video Editing Certificate</a></h3>
          <p>Discover how to edit and organize video to tell compelling stories. Find out how to improve audio. Learn to apply effects and motion graphics using modern tools. Program participants leave with solid skills in the industry standard digital video tools.</p>

          <h3><a href="/certificates/photoshop-certification"><img alt="adobe-photoshop-training-classes" src="/sites/default/files/adobe-photoshop-training-36x36.png"
                                                                   style="width: 36px; height: 36px; float: left;"/>Photoshop Certificate</a></h3>
          <p>The Adobe <a href="/certificates/photoshop-certification">Photoshop certification</a> program prepares
            you for a career in digital imaging. This program is designed for aspiring digital artists who need to gain
            expert-level skills in manipulating and retouching images using Photoshop. Classes are led by the authors of
            the best-selling books <em>Adobe Photoshop Digital Classroom </em>and <em>Adobe Creative
              Cloud for Dummies</em>.</p>
          <p>You can also create your own learning path with an annual or semi-annual <a
              href="/training-vouchers-and-training-passes">training pass or training vouchers</a> from American
            Graphics
            Institute.</p>
        </div>

        <div class="locations">
          <h4>Certificate Program Locations</h4>
          <p>American Graphics Institute offers certificate programs at its headquarters in suburban Boston and online.
            You can learn more about these program locations:</p>
          <ul>
            <li><a href="/ux/classes/ux-certificate/boston-user-experience-certificate">UX Certification in Boston</a>
            </li>
            <li><a href="/certificates/photoshop-certification/boston-photoshop-certification">Photoshop Certification
                in
                Boston</a></li>
            <li><a href="/certificates/digital-marketing-certificate/boston-digital-marketing-certificate">Digital
                Marketing Certificate in Boston</a></li>
            <li><a href="/certificates/web-design-certificate/boston-web-design-certificate">Web Design Cerification in
                Boston</a></li>
          </ul>
          <p>Scholarships are available. See the <a href="/scholarships">scholarships page </a>for more information.</p>
          <h4>Using Veterans Affairs (VA) GI Bill benefits for Certificate Programs</h4>
          <p>American Graphics Institute and these certificate programs have been approved for GI Bill benefits.
            Eligible
            participants can use their Veterans Affairs (VA) funds for the certificate program tuition.</p>
        </div>
      </div>
    </div>
  </div>
</div>
