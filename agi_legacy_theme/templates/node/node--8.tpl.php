<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <div class="row">
    <div class="col-lg-8 offset-lg-4 min-height">
      <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_body']);
      ?>

      <div class="intro">
        <p>We deliver private and custom courses, workshops, seminars and training programs for organizations and
          groups.</p>
        <ul>
          <li>Private training for 1 person or 1,000 employees.</li>
          <li>Single classes or long-term programs.</li>
          <li>Classes can be tailored to your specific needs.</li>
          <li>Available on-site at your location, in our training centers including <a
              href="/locations/boston">Boston</a>,
            <a href="/locations/nyc">NYC</a>, and <a href="/locations/philadelphia">Philadelphia</a>, or remotely
            on-line.
          </li>
          <li>Available courses include <a href="/adobe/illustrator/classes">Illustrator</a>, <a
              href="/adobe/indesign/classes">InDesign</a>, <a href="/adobe/photoshop/classes">Photoshop</a>,<a
              href="/adobe/after-effects/classes"> After Effects</a>, <a href="/adobe/creative-cloud-training">Creative
              Cloud</a>, <a href="/adobe/premiere-pro/classes">Premiere Pro</a>, <a href="/google/analytics/training">Google
              Analytics</a>, <a href="/html/classes">HTML</a>, <a href="/css/training">CSS</a>, <a
              href="/ux/classes">UX</a>, <a href="/wordpress/classes">Word Press</a>, and many more.
          </li>
          <li>More than 20 years of providing training to companies and organizations. See a representative <a
              href="/about/clients">sample of AGI clients</a>.
          </li>
        </ul>
        <p>For information about private and custom training complete the form below, or for immediate assistance call a
          training consultant at 781 376-6044 or 800 851-9237.</p>
      </div>

      <?php if (isset($form)) : ?>
        <div class="request-form">
          <?php print $form; ?>
        </div>
      <?php endif; ?>

      <div class="testimonials">
        <p><strong>Testimonials</strong></p>
        <p>Thousands of clients have provided testimonials about our training. We&#39;re happy to supply references for
          specific classes and industries.</p>
        <p>&ldquo;This was one of the best trainings that I have participated in. The whole group loved the
          training and benefited from it. The examples, the support, the multi-version exercises (with completed
          code), the accompanying textbook, the additional research, the hands-on aspect all contributed to an overall,
          extremely helpful session. On behalf of the Center for Online Learning, I thank you for such an
          outstanding experience.&rdquo;<br/><em>Director, Center for Online Learning</em><br/><em>Metropolitan State
            University</em></p>
        <p></p>
        <p>&ldquo;An excellent experience -- the customization of the training was truly impressive. Our designers
          are excited and ready to go.&rdquo;<br/><em>Business Manager, Design Ops</em><br/><em>Reebok
            International</em></p>
        <p></p>
        <p>&ldquo;AGI took the time to listen and develop a curriculum around our specific needs. The preparation
          and overall client service paid off with an exceptional training session. I will be recommending AGI. Thanks
          again!&rdquo;<br/><em>Publishing Systems Manager<br/>Staples</em></p>
        <p></p>
        <p>&ldquo;The staff are very personable, professional and accommodating. The facility is easily
          accessible, very modern, clean, comfortable, and the class size is perfect. The instructors are
          extremely sharp, they are great at explaining the program(s) in their simplest form and getting to the core of
          what each programs strengths and weaknesses are. They make the material very palatable and easily digestible.
          They include a comprehensive booklet with tutorial CD for each class. On top of everything else they make
          themselves accessible after the training class when we are back in our own world via email in case we get
          stuck or have any issues figuring something out.Kudos to AGI!&rdquo;<br/><em>Hrair A.</em></p>
      </div>

    </div>
  </div>

</div>
