<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <div class="row">
    <div class="col-lg-8 offset-lg-4 min-height">
      <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_body']);
      ?>

      <div class="intro">

        <h4>AGI Training locations: training classes with a live instructor in the same classroom with you.</h4>
        <div class="row">
          <div class="col-lg-6">
            <h4><a href="/locations/boston">Boston</a></h4>
            <p>150 Presidential Way<br/>Woburn, MA 01801<br/>(781) 376-6044<br/>Regularly-scheduled live public
              training with an instructor in the same classroom with you in our Boston training center. Private
              and custom training is also available in Boston. Available courses include <a
                href="/adobe/creative-cloud-classes-boston">Creative Cloud</a>, <a
                href="/adobe/photoshop/classes/boston">Photoshop</a>, <a
                href="/adobe/indesign/classes/indesign-classes-boston-ma">InDesign</a>, <a
                href="/adobe/illustrator/classes/boston">Illustrator</a>, <a
                href="/adobe/premiere-pro/classes/boston">Premiere Pro</a>, <a
                href="/adobe/after-effects/classes/boston">After Effects</a>, <a
                href="/google/analytics/training/boston-google-analytics-training-classes">Analytics</a>, and <a
                href="/ux/classes/boston">UX</a> training in Boston.</p><h4><a href="/locations/nyc">New York City</a>
            </h4>
            <p>185 Madison Av.<br/>New York, NY 10016<br/>(212) 922-1206<br/>Regularly scheduled live
              public courses with an instructor in the same classroom with you at our midtown NYC location. Private
              and custom courses are also available in New York.</p><h4><a
                href="/classes/locations/agi-training-philadelphia.html">Philadelphia</a></h4>
            <p>101 West Elm Street<br/>Conshohocken, PA 19428<br/>(610) 228-0951<br/>Regularly scheduled live public classes
              with an instructor in the same classroom with you in our suburban Philadelphia training center. We also
              offer private and custom training.</p><h4><a href="/online-training">Online Training</a></h4>
            <p>The same instructors that teach in our classrooms also deliver our live online classes. These classes
              occur separately from our classroom training, and allow you to benefit from live courses in
              any location where you have a broadband Internet connection. Our small-group online classes are
              delivered by a live instructor. You can see the instructor&#39;s screen, hear them over a supplied
              headset or your computer&#39;s screen, and follow-along using supplied lesson files and curriculum sent
              to you before the course.</p>
          </div>
          <div class="col-lg-6">
            <h4><a href="/classes/locations/agi-training-orlando.html">Orlando</a></h4>
            <p>1800 Pembrook Drive<br/>Orlando, FL 32820<br/>(800) 851-9237<br/>Live, private training for groups and
              individuals with an instructor in the same classroom with you.</p><h4><a
                href="/classes/locations/agi-training-chicago.html">Chicago</a></h4><p>22 W. Washington Street, Suite
              1500</p>
            <p>Chicago, IL 60602<br/>(800) 851-9237 <br/>Live, private training for groups and individuals with
              an instructor in the same classroom with you.</p><h4><a
                href="/classes/locations/agi-training-london.html">London</a></h4>
            <p>Corinthian House<br/>279 Tottenham Court Road<br/>London, W1T 7RJ<br/>Live, private training for groups
              and individuals in London, UK with an instructor in the same classroom with you.</p><h4><a
                href="/custom-training">On-Site Training</a></h4>
            <p>Private and custom training is available on-site, at your location. Call the American Graphics
              Institute training center nearest you, or our headquarters at (781) 376-6044 to discuss hosting a
              class at your location.</p>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-6">
            <h4><a href="/classes/contact.html">Contact Us</a></h4>
            <p>Toll Free: (800) 851-9237</p>
            <p><a href="/classes/contact.html">Online contact form</a></p>
            <p>American Graphics Institute<br/>150 Presidential Way<br/>Woburn, MA, 01801</p>
          </div>
          <div class="col-lg-6">
            <h4>On-site Training at Your Location</h4>
            <p>American Graphics Institute offers private and customized training at your location. Over the past
              20 years our instructors have traveld to clients across the U.S., Canada, Europe, Asia,
              Australia, and Africa. We customize our on-site training to your needs, adapting the content to your
              goals and objectives. Call to speak to a training representative to discuss on-site training options:
              (781) 376-6044 or (800) 851-9237.</p>
          </div>
        </div>
      </div>


    </div>
  </div>

</div>
