<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php

// Display certificate link if user has profile2 first and last names populated
// or else display a link for the user to update their profile.

$account_uid = $row->commerce_order_uid;

$profile2 = profile2_load_by_user($account_uid);

$has_profile = FALSE;

if (isset($profile2) && isset($profile2['main']->field_first_name['und'][0]['safe_value']) && isset($profile2['main']->field_last_name['und'][0]['safe_value'])) {
  $has_profile = TRUE;
}

if ($has_profile) {
  print $output;
}
else {
  print '<a href="/profile-main/' . $account_uid . '/edit">Update your profile to view certificate</a>';
}
