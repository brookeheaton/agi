<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>

<?php

$path_alias = drupal_get_path_alias('node/' . $row->nid);

// Check user has the required role to view the epub or lesson files.
$authorized_to_view = FALSE;

if (key_exists(6, $user->roles)) {
  $authorized_to_view = TRUE;
}
elseif (key_exists(11, $user->roles)) {
  $authorized_to_view = TRUE;
}
elseif (key_exists(3, $user->roles)) {
  $authorized_to_view = TRUE;
}
elseif (key_exists(4, $user->roles)) {
  $authorized_to_view = TRUE;
}

if ($authorized_to_view) {
  if (isset($row->field_field_path_to_epub[0]['rendered']['#markup'])) {
    $epub_link = '/books/reader/' . $row->field_field_path_to_epub[0]['rendered']['#markup'];
  }

  if (isset($row->field_field_dcb_all_lesson_files[0]['rendered']['#markup'])) {
    $lesson_files_link = $row->field_field_dcb_all_lesson_files[0]['rendered']['#markup'];
  }
  elseif (isset($row->field_field_all_lesson_files_external_[0]['raw']['url'])) {
    $lesson_files_link = $row->field_field_all_lesson_files_external_[0]['raw']['url'];
  }
}

?>

<div class="row mb-2">

  <div class="col-lg-3">
    <?php print render($row->field_field_dc_book_image[0]['rendered']); ?>
  </div>

  <div class="col-lg-9 last">
    <h2><a href="/<?php print $path_alias; ?>"><?php print $row->node_title; ?></a></h2>
    <p><?php print $row->field_body[0]['rendered']['#markup']; ?></p>
    <div class="d-lg-flex align-items-lg-center">
      <?php
        print (isset($epub_link) ? '<a href="' . $epub_link . '" class="btn btn-primary mb-3 mr-3 d-block">Read book</a> ' : '');
        print (isset($lesson_files_link) ? '<a href="' . $lesson_files_link . '" class="btn btn-primary mb-3 mr-3 d-block">Download lesson files</a> ' : '');
      ?>
      <a href="/<?php print $path_alias; ?>" class="btn btn-primary d-block mb-3">View book details</a>
    </div>

  </div>

</div>
