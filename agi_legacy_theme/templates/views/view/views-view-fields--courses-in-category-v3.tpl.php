<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>

<?php
$row_title = $row->node_field_data_field_courses_in_category_title;
?>

<div class="cf">
  <fieldset class="collapsible collapsed">
    <legend class="container-row cf" style="width: 100%;">
      <div class="col9"><?php (isset($fields['title'])) ? print ($fields['title']->content) : print ''; ?></div>
      <div class="col3 align-center"><span class="fieldset-legend"><img src="/sites/all/themes/agi/assets/images/trans_bg_10.png" style="height: 40px; width: 150px; margin: 0 auto;" alt="expand icon"></span></div>
    </legend>

    <div class="fieldset-wrapper m-t-1">

      <?php foreach ($fields as $id => $field): ?>
        <?php if ($field->handler->field_alias != 'node_field_data_field_courses_in_category_title'): ?>
          <?php if (!empty($field->separator)): ?>
            <?php print $field->separator; ?>
          <?php endif; ?>

          <?php print $field->wrapper_prefix; ?>
            <?php print $field->label_html; ?>
            <?php print $field->content; ?>
          <?php print $field->wrapper_suffix; ?>
        <?php endif; ?>

      <?php endforeach; ?>

    </div>

  </fieldset>
</div>