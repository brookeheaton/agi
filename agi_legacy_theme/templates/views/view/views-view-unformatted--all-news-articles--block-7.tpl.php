<?php
$groups = array_chunk($rows, 3);

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($groups as $group): ?>
  <div class="row">
  <?php foreach ($group as $id => $row): ?>
    <div class="col-md-4">
      <div<?php if ($classes_array[$id]): ?> class="<?php print $classes_array[$id]; ?>"<?php endif; ?>>
        <?php print $row; ?>
      </div>
    </div>
  <?php endforeach; ?>
  </div>
<?php endforeach; ?>
