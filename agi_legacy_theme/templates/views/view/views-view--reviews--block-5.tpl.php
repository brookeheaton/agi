<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */

$cat_loc_node = NULL;
$array = $view->result;
$num_of_reviews = count($array);
$total_num_of_category_reviews = $view->args['total_num_of_reviews'];
?>
<?php if (!empty($array) || $total_num_of_category_reviews > 0) : ?>
<div>

  <div class="<?php print $classes; ?>">
    <?php print render($title_prefix); ?>
    <?php if ($title): ?>
      <?php print $title; ?>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($header): ?>
      <div class="view-header">
        <?php print $header; ?>
      </div>
    <?php endif; ?>

    <?php if ($exposed): ?>
      <div class="view-filters">
        <?php print $exposed; ?>
      </div>
    <?php endif; ?>

    <?php if ($attachment_before): ?>
      <div class="attachment attachment-before">
        <?php print $attachment_before; ?>
      </div>
    <?php endif; ?>

      <?php
      // Render rich snippets.
      if (isset($view->args[0])) {
          $cat_loc_node = node_load($view->args[0]);
          $alt_text = '';
          $city = 'online';
          if (!empty($cat_loc_node->field_location_for_cat[LANGUAGE_NONE][0]['entity']->field_city_for_reference_pages)) {
            $city = $cat_loc_node->field_location_for_cat[LANGUAGE_NONE][0]['entity']->field_city_for_reference_pages[LANGUAGE_NONE][0]['safe_value'];
          }
          elseif ($cat_loc_node->field_location_for_cat[LANGUAGE_NONE][0]['entity']->nid == 670) {
            $city = 'private';
          }
          if ($cat_loc_node->type == 'seconday_location_category_page') {
            $city = $cat_loc_node->field_secondary_location[LANGUAGE_NONE][0]['entity']->field_address_city[LANGUAGE_NONE][0]['safe_value'];
            $cat_node = node_load($cat_loc_node->field_secondary_location[LANGUAGE_NONE][0]['entity']->field_nearby_primary_location[LANGUAGE_NONE][0]['target_id']);
            $short_cat_name = $cat_loc_node->field_cat_for_location['und'][0]['entity']->field_short_category_name[LANGUAGE_NONE][0]['safe_value'];
            $alt_text = $short_cat_name . ' courses ' . (strtolower($city) != "online" ? "in" : "") . ' ' . $city . ' review rating';
          }
          else {
            $cat_node = node_load($cat_loc_node->field_cat_for_location['und'][0]['target_id']);
            $short_cat_name = $cat_loc_node->field_cat_for_location['und'][0]['entity']->field_short_category_name['und'][0]['safe_value'];
            $alt_text = $short_cat_name . ' courses ' . (strtolower($city) != "online" ? "in" : "") . ' ' . $city . ' review rating';
          }

          $new_array = array();
          $count = 0;
          $average = 0;
          if ($num_of_reviews > 0) {
            for ($i = 0; $i < $num_of_reviews; $i++) {
              $new_array[] = (int)$array[$i]->_field_data['nid']['entity']->field_review_rating['und']['0']['value'];
            }
            $count = count($new_array);
            $average = $count ? array_sum($new_array) / count($new_array) : 0;
            $average = round($average, 0, PHP_ROUND_HALF_UP);
          }
          elseif (isset($view->args['total_num_of_reviews']) && $view->args['total_num_of_reviews'] > 0 && isset($view->args['total_reviews_average'])) {
            $count = $view->args['total_num_of_reviews'];
            $average = $view->args['total_reviews_average'];
          }

          $a = 0;
          print '<div class="stars">';
          while ($a < $average) {
              print '
              <svg class="feather-icon">
                <use xlink:href="#training-reviews">
              </svg>';
              $a++;
          }
          print '</div>';

          // Dont render rich snippet data on our testing pages.
          if ($cat_loc_node->field_cat_for_location[LANGUAGE_NONE][0]['target_id'] == '38') {
              print '<div>
               <div><span>' . $short_cat_name . ' classes </span>rating:</div>
               <div>
                 <span>' . $average . '</span> stars from
                 <span>' . ($total_num_of_category_reviews ? $total_num_of_category_reviews : $num_of_reviews) . '</span> reviewers
               </div>
             </div>';
          }
          else {
            $image = !empty($cat_loc_node->field_cat_for_location[LANGUAGE_NONE][0]['entity']->field_subject_thumbnail_image) ? $cat_loc_node->field_cat_for_location[LANGUAGE_NONE][0]['entity']->field_subject_thumbnail_image[LANGUAGE_NONE][0]['uri'] : NULL;
            if ($image) {
              $image = file_create_url($image);
            }
            else {
              $image = 'https://www.agitraining.com/sites/all/themes/agi/assets/images/Adobe-Training-AGI.png';
            }

            $product_description = $short_cat_name . ' classes and ' . $short_cat_name . ' training in ' . $city;
            if (strtolower($city) == 'private') {
              $product_description = 'Private ' . $short_cat_name . ' classes and ' . $short_cat_name . ' training';
            }
            elseif (strtolower($city) == 'online') {
              $product_description = $short_cat_name . ' classes and ' . $short_cat_name . ' training online';
            }

            print '<div itemscope itemtype="http://schema.org/Product">

             <meta itemprop="description" content="' . $product_description . ' from American Graphics Institute."/>
             <meta itemprop="brand" content="' . $cat_loc_node->field_cat_for_location[LANGUAGE_NONE][0]['entity']->title . '">
             <meta itemprop="mpn" content="' . $cat_loc_node->field_cat_for_location[LANGUAGE_NONE][0]['entity']->nid . '">
             <meta itemprop="sku" content="' . $short_cat_name . ' classes ' . $city . '"/>
             <meta itemprop="productID" content="' . $short_cat_name . ' classes ' . $city . '"/>
             <meta itemprop="image" content="' . $image . '"/>
             <span itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
              <meta itemprop="offerCount" content="10">
              <meta itemprop="lowPrice" content="450">
              <meta itemprop="highPrice" content="995">
              <meta itemprop="priceCurrency" content="USD">
             </span>
             <div><span itemprop="name">' . $short_cat_name . ' classes </span>rating:</div>
             <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                <meta itemprop="ratingValue" content="' . $average . '" />
                <meta itemprop="reviewCount" content="' . $total_num_of_category_reviews . '" />
                <meta itemprop="worstRating" content="1"/>
                <meta itemprop="bestRating" content="5"/>
               <span>' . $average . '</span> stars from
               <span>' . $total_num_of_category_reviews . '</span> reviewers
             </div>';
            // Missing div is below, we want the Product schema to wrap it.
          }
      }
      ?>

    <?php if ($rows): ?>
      <div class="view-content" style="margin: 1rem 0;">
        <?php print $rows; ?>
      </div>
    <?php elseif (isset($view->args['reviews'])) : ?>
      <div class="category-reviews my-3" itemprop="review" itemscope itemtype="http://schema.org/Review">
        <?php foreach ($view->args['reviews'] as $review) : ?>
          <div class="category-review-item">
            <?php print $review; ?>
          </div>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>

    <?php if ($footer): ?>
      <div class="view-footer">
        <?php print $footer; ?>
      </div>
    <?php elseif (isset($view->args['link_to_reviews'])) : ?>
        <?php print $view->args['link_to_reviews']; ?>
    <?php endif; ?>

    <?php if (!empty($cat_loc_node) && !empty($cat_loc_node->field_location_for_cat) && $cat_loc_node->field_location_for_cat[LANGUAGE_NONE][0]['target_id'] == "24") : ?>
      <p class="google-reviews">
        <a href="https://www.google.com/search?q=american+graphics+institute+nyc#lrd=0x89c258fe532e13b3:0x8ad09e9907ab113a,1" target="_blank">
          <span>Google Reviews:</span>
          <span class="stars">
            <svg class="feather-icon">
              <use xlink:href="#training-reviews">
            </svg>
            <svg class="feather-icon">
              <use xlink:href="#training-reviews">
            </svg>
            <svg class="feather-icon">
              <use xlink:href="#training-reviews">
            </svg>
            <svg class="feather-icon">
              <use xlink:href="#training-reviews">
            </svg>
            <svg class="feather-icon">
              <use xlink:href="#training-reviews">
            </svg>
          </span>
          <span class="score">4.8</span>
        </a>
      </p>
    <?php endif; ?>


    <?php /** Schema wrapper Product ending div */ ?>
    <?php if ($cat_loc_node->field_cat_for_location[LANGUAGE_NONE][0]['target_id'] != '38') : ?>
      </div>
    <?php endif; ?>

</div>
</div>
<?php endif; ?>
