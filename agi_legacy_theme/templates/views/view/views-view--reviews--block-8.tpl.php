<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */


$cat_loc_node = node_load($view->args[0]);
$alt_text = '';
$city = 'online';
if (!empty($cat_loc_node->field_location_for_cat[LANGUAGE_NONE][0]['entity']->field_city_for_reference_pages)) {
  $city = $cat_loc_node->field_location_for_cat[LANGUAGE_NONE][0]['entity']->field_city_for_reference_pages[LANGUAGE_NONE][0]['safe_value'];
}
elseif ($cat_loc_node->field_location_for_cat[LANGUAGE_NONE][0]['entity']->nid == 670) {
  $city = 'private';
}
if ($cat_loc_node->type == 'seconday_location_category_page') {
  $city = $cat_loc_node->field_secondary_location[LANGUAGE_NONE][0]['entity']->field_address_city[LANGUAGE_NONE][0]['safe_value'];
  $cat_node = node_load($cat_loc_node->field_secondary_location[LANGUAGE_NONE][0]['entity']->field_nearby_primary_location[LANGUAGE_NONE][0]['target_id']);
  $short_cat_name = $cat_loc_node->field_cat_for_location['und'][0]['entity']->field_short_category_name[LANGUAGE_NONE][0]['safe_value'];
  $alt_text = $short_cat_name . 'courses in ' . $city . ' review rating';
}
else {
  $cat_node = node_load($cat_loc_node->field_cat_for_location['und'][0]['target_id']);
  $short_cat_name = $cat_loc_node->field_cat_for_location['und'][0]['entity']->field_short_category_name['und'][0]['safe_value'];
  $alt_text = $short_cat_name . ' courses in ' . $city . ' review rating';
}

$array = $view->result;
$num_of_reviews = count($array);
$total_num_of_category_reviews = $view->args['total_num_of_reviews'];

$new_array = array();
for ($i = 0; $i < $num_of_reviews; $i++) {
  $new_array[] = (int) $array[$i]->_field_data['nid']['entity']->field_review_rating['und']['0']['value'];
}

$count = count($new_array);
$average = $count ? array_sum($new_array) / count($new_array) : 0;
$average = round($average, 0, PHP_ROUND_HALF_UP);

$image = !empty($cat_loc_node->field_cat_for_location[LANGUAGE_NONE][0]['entity']->field_subject_thumbnail_image) ? $cat_loc_node->field_cat_for_location[LANGUAGE_NONE][0]['entity']->field_subject_thumbnail_image[LANGUAGE_NONE][0]['uri'] : NULL;
if ($image) {
  $image = file_create_url($image);
}
else {
  $image = 'https://www.agitraining.com/sites/all/themes/agi/assets/images/Adobe-Training-AGI.png';
}

$product_description = $short_cat_name . ' classes and ' . $short_cat_name . ' training in ' . $city;
if ($city == 'private') {
  $product_description = 'Private ' . $short_cat_name . ' classes and ' . $short_cat_name . ' training';
}
elseif ($city == 'online') {
  $product_description = $short_cat_name . ' classes and ' . $short_cat_name . ' training online';
}

?>
<div class="<?php print $classes; ?>">
  <div itemscope itemtype="http://schema.org/Product">

    <meta itemprop="description" content="<?php print $product_description; ?> from American Graphics Institute."/>
    <meta itemprop="brand" content="<?php print $cat_loc_node->field_cat_for_location[LANGUAGE_NONE][0]['entity']->title; ?>">
    <meta itemprop="mpn" content="<?php print $cat_loc_node->field_cat_for_location[LANGUAGE_NONE][0]['entity']->nid; ?>">
    <meta itemprop="sku" content="<?php print $short_cat_name; ?> classes <?php print $city; ?>"/>
    <meta itemprop="productID" content="<?php print $short_cat_name; ?> classes <?php print $city; ?>"/>
    <meta itemprop="image" content="<?php print $image; ?>"/>
    <span itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
      <meta itemprop="offerCount" content="10">
      <meta itemprop="lowPrice" content="450">
      <meta itemprop="highPrice" content="995">
      <meta itemprop="priceCurrency" content="USD">
    </span>
    <meta itemprop="name" content="<?php print $short_cat_name; ?> classes">
    <span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
      <meta itemprop="ratingValue" content="<?php print $average; ?>" />
      <meta itemprop="reviewCount" content="<?php print $total_num_of_category_reviews; ?>" />
      <meta itemprop="worstRating" content="1"/>
      <meta itemprop="bestRating" content="5"/>
    </span>

    <p><strong>Average <?php print $average; ?> out of 5 from all reviewers!</strong></p>

    <?php if ($exposed): ?>
      <div class="view-filters">
        <?php print $exposed; ?>
      </div>
    <?php endif; ?>

    <?php if ($attachment_before): ?>
      <div class="attachment attachment-before">
        <?php print $attachment_before; ?>
      </div>
    <?php endif; ?>
    <?php if ($rows): ?>
      <div class="view-content">
        <?php print $rows; ?>
      </div>
    <?php elseif ($empty): ?>
      <div class="view-empty">
        <?php print $empty; ?>
      </div>
    <?php endif; ?>

  </div>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div><?php /* class view */ ?>
