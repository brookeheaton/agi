<?php

/**
 * @file
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $caption: The caption for this table. May be empty.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */

// Schema.org data for the outter tr tag.
$schema = ' itemscope itemtype = "http://schema.org/EducationEvent"';
// Dont Render rich snippet data on our JSON-LD Test pages.
$node = node_load($view->args[0]);
if (!empty($node->field_cat_for_location) &&  $node->field_cat_for_location[LANGUAGE_NONE][0]['target_id'] == 38) {
  $cat = node_load($node->field_cat_for_location[LANGUAGE_NONE][0]['target_id']);
  $schema = '';
}
else {
  // Another check for to ensure the result product entity isnt set to illustrator.
  $first_item = reset($view->result);
  if ((isset($first_item->node_field_data_field_cat_for_location_nid) && $first_item->node_field_data_field_cat_for_location_nid == 38) ||
    (isset($first_item->field_courses_in_category_node_nid) && $first_item->field_courses_in_category_node_nid == 38) ||
    (isset($first_item->field_field_event_name) && $first_item->field_field_event_name[0]['raw']['entity']->field_primary_category[LANGUAGE_NONE][0]['target_id'] == 38) ||
    (arg(0) == 'node' && arg(1) == '38')) {

    $cat = node_load($node->field_cat_for_location[LANGUAGE_NONE][0]['target_id']);
    $schema = '';
  }
}
?>
<div class="table-responsive">
<table <?php if ($classes) { print 'class="table '. $classes . '" '; } ?><?php print $attributes; ?>>
   <?php if (!empty($title) || !empty($caption)) : ?>
     <caption><?php print $caption . $title; ?></caption>
  <?php endif; ?>
  <?php if (!empty($header)) : ?>
    <thead>
      <tr>
        <?php foreach ($header as $field => $label): ?>
          <th <?php if ($header_classes[$field]) { print 'class="'. $header_classes[$field] . '" '; } ?>>
            <?php print $label; ?>
          </th>
        <?php endforeach; ?>
      </tr>
    </thead>
  <?php endif; ?>
  <tbody>
    <?php foreach ($rows as $row_count => $row): ?>
      <tr <?php if ($row_classes[$row_count]) { print 'class="' . implode(' ', $row_classes[$row_count]) .'"';  } ?><?php print $schema; ?>>
        <?php foreach ($row as $field => $content): ?>
          <td <?php if ($field_classes[$field][$row_count]) { print 'class="'. $field_classes[$field][$row_count] . '" '; } ?><?php print drupal_attributes($field_attributes[$field][$row_count]); ?>>
            <?php print $content; ?>
          </td>
        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
</div>
