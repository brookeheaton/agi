<?php
/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div>

    <div class="<?php print $classes; ?>">
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <?php print $title; ?>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php if ($header): ?>
          <div class="view-header">
            <?php print $header; ?>
          </div>
      <?php endif; ?>

      <?php if ($exposed): ?>
          <div class="view-filters">
            <?php print $exposed; ?>
          </div>
      <?php endif; ?>

      <?php if ($attachment_before): ?>
          <div class="attachment attachment-before">
            <?php print $attachment_before; ?>
          </div>
      <?php endif; ?>

      <?php
      $node = node_load($view->args[0]);
      $cat_node = NULL;
      if ($node->type == 'course' && !empty($node->field_primary_category)) {
        $cat_node = node_load($node->field_primary_category[LANGUAGE_NONE][0]['target_id']);
      }
      // Loop through the rows and get the total number of reviews as well as the
      // average rating
      // @TODO wrap results in rich snippet data
      $array = $view->result;
      $num_of_reviews = count($array);
      $new_array = [];
      for ($i = 0; $i < $num_of_reviews; $i++) {
        $new_array[] = (int) $array[$i]->_field_data['nid']['entity']->field_review_rating['und']['0']['value'];
      }
      $count = count($new_array);
      if ($count > 0) {
        $average = round($view->course_average, 0, PHP_ROUND_HALF_UP);
      }
      else {
        $average = 0;
      }
      $a = 0;
      print '<div class="review-stars">';
      while ($a < $average) {
        print '
              <svg class="feather-icon">
                <use xlink:href="#training-reviews">
              </svg>';
        $a++;
      }
      print '<span>Average <span itemprop="average">5</span> out of <span itemprop="best">5</span> from all reviewers!</span></div>';

      $image = !empty($cat_node->field_subject_thumbnail_image) ? $cat_node->field_subject_thumbnail_image[LANGUAGE_NONE][0]['uri'] : NULL;
      if ($image) {
        $image = file_create_url($image);
      }
      else {
        $image = 'https://www.agitraining.com/sites/all/themes/agi/assets/images/Adobe-Training-AGI.png';
      }

      $product_description = $node->title;

      print '
      <div itemscope itemtype="http://schema.org/Product">    
         <meta itemprop="description" content="' . $product_description . ' from American Graphics Institute."/>
         <meta itemprop="brand" content="' . $cat_node->title . '">
         <meta itemprop="mpn" content="' . $cat_node->nid . '">
         <meta itemprop="sku" content="' . $node->title . '"/>
         <meta itemprop="productID" content="' . $node->title . '"/>
         <meta itemprop="image" content="' . $image . '"/>
         <span itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
          <meta itemprop="offerCount" content="10">
          <meta itemprop="lowPrice" content="450">
          <meta itemprop="highPrice" content="995">
          <meta itemprop="priceCurrency" content="USD">
         </span>
         <meta itemprop="name" content="' . $node->title . ' ">
         <span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
            <meta itemprop="ratingValue" content="' . $average . '" />
            <meta itemprop="reviewCount" content="' . $num_of_reviews . '" />
            <meta itemprop="worstRating" content="1"/>
            <meta itemprop="bestRating" content="5"/>
         </span>';
        // Missing div is below, we want the Product schema to wrap it.
        ?>

        <?php if ($rows): ?>
          <div class="view-content" style="margin: 1rem 0;">
            <?php print $rows; ?>
          </div>
        <?php elseif ($empty): ?>
          <div class="view-empty">
            <?php print $empty; ?>
          </div>
        <?php endif; ?>

      </div>

      <?php if ($pager): ?>
        <?php print $pager; ?>
      <?php endif; ?>

      <?php if ($attachment_after): ?>
          <div class="attachment attachment-after">
            <?php print $attachment_after; ?>
          </div>
      <?php endif; ?>

      <?php if ($more): ?>
        <?php print $more; ?>
      <?php endif; ?>

      <?php if ($footer): ?>
          <div class="view-footer">
            <?php print $footer; ?>
          </div>
      <?php endif; ?>

      <?php if ($feed_icon): ?>
          <div class="feed-icon">
            <?php print $feed_icon; ?>
          </div>
      <?php endif; ?>

    </div><?php /* class view */ ?>
    <p style="display:none;">A total of <?php print $count; ?> user reviews</p>
</div>
