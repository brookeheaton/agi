<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>

<div>
<?php
$node = menu_get_object();
if ($node && $node->nid) {
  print '<p itemprop="itemreviewed">' . $node->title . '</p>';
}
?>

<?php
// Loop through the rows and get the total number of reviews as well as the
// average rating
// @TODO wrap results in rich snippet data
$array = $view->result;
$num_of_reviews = count($array);

$new_array = array();

for($i=0; $i<$num_of_reviews; $i++) {
  $new_array[] = (int)$array[$i]->_field_data['nid']['entity']->field_review_rating['und']['0']['value'];
}

$count = count($new_array);
$average = array_sum($new_array) / count($new_array);
$average = round($average, 0, PHP_ROUND_HALF_UP);
?>

<div class="<?php print $classes; ?>">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>

  <?php
  while ($a < $average) {
    print '<img src="/sites/all/themes/agi/assets/images/rating-star.png" />';
    $a++;
  }
  print '<p class="large-text" itemprop="rating" itemscope itemtype="http://data-vocabulary.org/Rating"><span itemprop="average"> ' . $average . '</span> out of <span itemprop="best">5</span> stars from ' . $count .' reviewers</p>';
  ?>

  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if ($rows): ?>
    <div class="view-content">
      <?php print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div><?php /* class view */ ?>
<p>A total of <?php print $count; ?> user reviews</p>
</div>
