<?php

/**
 * @file
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $caption: The caption for this table. May be empty.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
?>
<?php
$first_result_key = key($result);
$course_nid = $result[$first_result_key]->node_field_data_field_courses_in_category_nid;
$course_node = node_load($course_nid);
$course_description = $course_node->field_course_abstract['und'][0]['safe_value'];
$course_pdf = file_create_url($course_node->field_course_pdf['und'][0]['uri']);

$cat_nid = $view->args[0];
$cat_node = node_load($cat_nid);
// Change $cat_nid value if display is block_2
if ($view->current_display == 'block_2') {
  $cat_nid = $course_node->field_primary_category['und'][0]['target_id'];
}

$cat_path_alias = drupal_get_path_alias('node/' . $cat_nid);

// Alt text.
$review_icon_alt_text = 'Rating Star';
$class_details_icon_alt_text = 'Details Icon';
$course_outline_icon_alt_text = 'PDF Icon';
$date_icon_alt_text = 'Date Icon';

$short_cat_name = $cat_node->field_cat_for_location[LANGUAGE_NONE][0]['entity']->field_short_category_name[LANGUAGE_NONE][0]['safe_value'];
$short_course_name = $course_node->field_short_course_name[LANGUAGE_NONE][0]['safe_value'];
if (!empty($cat_node->field_location_for_cat)) {
    if (!empty($cat_node->field_location_for_cat[LANGUAGE_NONE][0]['entity']->field_city_for_reference_pages)) {
        $city_name = $cat_node->field_location_for_cat[LANGUAGE_NONE][0]['entity']->field_city_for_reference_pages[LANGUAGE_NONE][0]['safe_value'];
        $review_icon_alt_text = $short_cat_name . ' courses in ' . $city_name . ' review rating';
        $class_details_icon_alt_text = $short_course_name . ' in ' . $city_name . ' class details';
        $course_outline_icon_alt_text = $short_course_name . ' in ' . $city_name . ' course outline';
        $date_icon_alt_text = $short_cat_name . ' class ' . $city_name . ' - dates';
    }
    else {
      $review_icon_alt_text = $short_cat_name . ' courses online review rating';
      $class_details_icon_alt_text = $short_course_name . ' online class details';
      $course_outline_icon_alt_text = $short_course_name . ' online course outline';
      $date_icon_alt_text = $short_cat_name . ' class online - dates';
    }
}
?>
<div class="alt_bg">
  <fieldset>
    <legend class="p-a-1 container-row cf" style="width: 100%;">
      <div class="col9"><h3><?php print $title; ?></h3></div>
    </legend>


    <div class="fieldset-wrapper m-t-1">
      <p class="p-l-1 p-r-1"><?php print $course_description; ?></p>

      <div class="links container-row align-sm-center cf p-l-1 p-r-1">
        <div class="col3 p-b-1">
          <div class="review-stars">
            <?php for ($i=0;$i<5;$i++) : ?>
              <svg class="feather-icon">
                <use xlink:href="#training-reviews">
              </svg>
            <?php endfor; ?>
            <a href="/reviews/<?php print $cat_nid . '/' . $cat_path_alias; ?>">See Reviews</a>
          </div>
        </div>

        <div class="col3 p-b-1">
          <svg class="feather-icon">
            <use xlink:href="#class-details"></use>
          </svg>
          <a href="/<?php print drupal_get_path_alias('node/' . $course_nid); ?>">View Class Details</a>
        </div>

        <div class="col3 p-b-1">
          <svg class="feather-icon">
            <use xlink:href="#training-outline"></use>
          </svg>
          <a href="<?php print $course_pdf; ?>" target="_blank">Download Course Outline</a>
        </div>

        <div class="col3 p-b-1">
          <svg class="feather-icon">
            <use xlink:href="#training-dates"></use>
          </svg>
          <a href="#see-course-dates">See class dates</a>
        </div>

      </div>
    </div>
  </fieldset>
</div>
