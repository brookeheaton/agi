<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */


$subject_node = node_load($view->args[0]);
$short_cat_name = $subject_node->field_short_category_name[LANGUAGE_NONE][0]['value'];
$array = $view->result;
$num_of_reviews = count($array);
$total_num_of_category_reviews = isset($view->args['total_num_of_reviews'])? $view->args['total_num_of_reviews'] : count($array);

$new_array = array();
for ($i = 0; $i < $num_of_reviews; $i++) {
  $new_array[] = (int) $array[$i]->_field_data['nid']['entity']->field_review_rating['und']['0']['value'];
}

$count = count($new_array);
$average = $count ? array_sum($new_array) / count($new_array) : 0;
$average = round($average, 0, PHP_ROUND_HALF_UP);

$image = !empty($subject_node->field_subject_thumbnail_image) ? $subject_node->field_subject_thumbnail_image[LANGUAGE_NONE][0]['uri'] : NULL;
if ($image) {
  $image = file_create_url($image);
}
?>
<div class="<?php print $classes; ?>">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <div itemscope itemtype="http://schema.org/Product">

    <meta itemprop="description" content="UX Classes and UX Training from American Graphics Institute."/>
    <meta itemprop="brand" content="<?php print $subject_node->title;?>">
    <meta itemprop="mpn" content="<?php print $subject_node->nid;?>">
    <meta itemprop="sku" content="<?php print $short_cat_name;?> classes"/>
    <meta itemprop="productID" content="<?php print $short_cat_name;?> classes"/>
    <meta itemprop="image" content="<?php print $image;?>"/>
    <span itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
              <meta itemprop="offerCount" content="10">
              <meta itemprop="lowPrice" content="450">
              <meta itemprop="highPrice" content="995">
              <meta itemprop="priceCurrency" content="USD">
             </span>
    <div><span itemprop="name"><?php print $short_cat_name;?> classes </span>rating:</div>
    <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
      <meta itemprop="ratingValue" content="<?php print $average;?>" />
      <meta itemprop="reviewCount" content="<?php print $total_num_of_category_reviews;?>" />
      <meta itemprop="worstRating" content="1"/>
      <meta itemprop="bestRating" content="5"/>
      <span><?php print $average;?></span> stars from
      <span><?php print $total_num_of_category_reviews;?></span> reviewers
    </div>

    <?php if ($exposed): ?>
      <div class="view-filters">
        <?php print $exposed; ?>
      </div>
    <?php endif; ?>

    <?php if ($attachment_before): ?>
      <div class="attachment attachment-before">
        <?php print $attachment_before; ?>
      </div>
    <?php endif; ?>
    <?php if ($rows): ?>
      <div class="view-content">
        <?php print $rows; ?>
      </div>
    <?php elseif ($empty): ?>
      <div class="view-empty">
        <?php print $empty; ?>
      </div>
    <?php endif; ?>

  </div>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div><?php /* class view */ ?>
