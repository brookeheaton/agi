<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>

    <div class="stars larger">
      <?php $i = 0; ?>
      <?php while ($i < $view->result[$id]->field_field_review_rating[0]['raw']['value']) : ?>
        <svg class="feather-icon">
          <use xlink:href="#training-reviews">
        </svg>
        <?php $i++; ?>
      <?php endwhile; ?>
    </div>

    <?php $author = !empty($view->result[$id]->field_field_reviewer_name) && !empty(trim($view->result[$id]->field_field_reviewer_name[0]['rendered']['#markup'])) ? $view->result[$id]->field_field_reviewer_name[0]['rendered']['#markup'] : 'N/A'; ?>
    <div itemprop="review" itemscope itemtype="http://schema.org/Review">
      <span>"<span itemprop="description"><?php print $view->result[$id]->field_body[0]['rendered']['#markup']; ?></span>" -<span itemprop="author"> <?php print $author; ?></span></span>
      <meta itemprop="datePublished" content="<?php print date('Y-m-d', $view->result[$id]->node_created); ?>">
      <span itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
          <meta itemprop="worstRating" content = "1">
          <meta itemprop="ratingValue" content="<?php print $view->result[$id]->field_field_review_rating[0]['raw']['value']; ?>">
          <meta itemprop="bestRating" content="5">
        </span>
    </div>

  </div>
<?php endforeach; ?>
