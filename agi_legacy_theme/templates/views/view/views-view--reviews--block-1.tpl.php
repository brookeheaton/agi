<?php
/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
// Render rich snippets.
if (isset($view->argument['nid']->argument)) {
  $cat_node = node_load($view->argument['nid']->argument);
  if ($cat_node->type == 'category_location_page') {
    $cat_node = $cat_node->field_cat_for_location[LANGUAGE_NONE][0]['entity'];
  }
  $short_cat_name = $cat_node->field_short_category_name['und'][0]['safe_value'];
  $array = $view->result;
  $num_of_reviews = count($array);
  $total_num_of_category_reviews = $view->args['total_num_of_reviews'];

  if ($num_of_reviews == 0) {
    // Dont print anything if its zero.
    return;
  }

  $new_array = array();
  for ($i = 0; $i < $num_of_reviews; $i++) {
    $new_array[] = (int)$array[$i]->_field_data['nid']['entity']->field_review_rating['und']['0']['value'];
  }
  $count = count($new_array);
  $average = array_sum($new_array) / count($new_array);
  $average = round($average, 0, PHP_ROUND_HALF_UP);
  $a = 0;

  $image = !empty($cat_node->field_subject_thumbnail_image) ? $cat_node->field_subject_thumbnail_image[LANGUAGE_NONE][0]['uri'] : NULL;
  if ($image) {
    $image = file_create_url($image);
  } else {
    $image = 'https://www.agitraining.com/sites/all/themes/agi/assets/images/Adobe-Training-AGI.png';
  }
}
else {
  return;
}
?>
<div style="margin-top: 40px;">

  <div class="<?php print $classes; ?>">
    <?php print render($title_prefix); ?>
    <?php if ($title): ?>
      <?php print $title; ?>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($header): ?>
      <div class="view-header">
        <?php print $header; ?>
      </div>
    <?php endif; ?>

    <?php if ($exposed): ?>
      <div class="view-filters">
        <?php print $exposed; ?>
      </div>
    <?php endif; ?>

    <?php if ($attachment_before): ?>
      <div class="attachment attachment-before">
        <?php print $attachment_before; ?>
      </div>
    <?php endif; ?>

    <?php
      print '<div class="review-stars">';
      while ($a < $average) {
        print '
              <svg class="feather-icon">
                <use xlink:href="#training-reviews">
              </svg>';
        $a++;
      }
      print '</div>';

      // Dont render rich snippet data on our testing pages.
      if (arg(0) == 'node' && arg(1) == '38') {
        print '<div>
           <div><span>' . $short_cat_name . ' classes </span>rating:</div>
           <div>
             <span>' . $average . '</span> stars from
             <span>' . $total_num_of_category_reviews . '</span> reviewers
           </div>
         </div>';
      }
      else {
        $product_desc = $short_cat_name . ' classes and ' . $short_cat_name;
        print '<div itemscope itemtype="http://schema.org/Product">
             <meta itemprop="description" content="' . $product_desc . ' training from American Graphics Institute."/>
             <meta itemprop="brand" content="' . $cat_node->title . '">
             <meta itemprop="mpn" content="' . $cat_node->nid . '">
             <meta itemprop="sku" content="' . $cat_node->title . '"/>
             <meta itemprop="productID" content="' . $cat_node->title . '"/>
             <meta itemprop="image" content="' . $image . '"/>
            <span itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
              <meta itemprop="offerCount" content="19">
              <meta itemprop="lowPrice" content="450">
              <meta itemprop="highPrice" content="995">
              <meta itemprop="priceCurrency" content="USD">
            </span>
           <div>
            <span itemprop="name">' . $short_cat_name . ' classes </span>rating:
           </div>
           <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
              <meta itemprop="ratingValue" content="' . $average . '" />
              <meta itemprop="reviewCount" content="' . $total_num_of_category_reviews . '" />
              <meta itemprop="worstRating" content="1"/>
              <meta itemprop="bestRating" content="5"/>
             <span>' . $average . '</span> stars from
             <span>' . $total_num_of_category_reviews . '</span> reviewers
           </div>';
           // Missing div is below, we want the Product schema to wrap it.
      }
    ?>

    <?php if ($rows): ?>
      <div class="view-content" style="margin: 1rem 0;">
        <?php print $rows; ?>
        <?php /** Schema wrapper Product ending div */ ?>
        <?php if (arg(0) == 'node' && arg(1) != '38') : ?>
          </div>
        <?php endif; ?>
      </div>
    <?php elseif ($empty): ?>
      <div class="view-empty">
        <?php print $empty; ?>
      </div>
    <?php endif; ?>

    <?php if ($pager): ?>
      <?php print $pager; ?>
    <?php endif; ?>

    <?php if ($attachment_after): ?>
      <div class="attachment attachment-after">
        <?php print $attachment_after; ?>
      </div>
    <?php endif; ?>

    <?php if ($more): ?>
      <?php print $more; ?>
    <?php endif; ?>

    <?php if ($footer): ?>
      <div class="view-footer">
        <?php print $footer; ?>
      </div>
    <?php endif; ?>

    <?php if ($feed_icon): ?>
      <div class="feed-icon">
        <?php print $feed_icon; ?>
      </div>
    <?php endif; ?>

  </div><?php /* class view */ ?>
  <p style="display:none;">A total of <?php print $count; ?> user reviews</p>
</div>
