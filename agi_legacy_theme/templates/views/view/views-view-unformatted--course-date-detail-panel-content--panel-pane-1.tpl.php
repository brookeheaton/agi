<?php

/**
 * @file
 * Custom template for main content pane of course-date nodes.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <div itemscope itemtype="http://schema.org/EducationEvent">
      <?php print $row; ?>
    </div>
  </div>
<?php endforeach; ?>
