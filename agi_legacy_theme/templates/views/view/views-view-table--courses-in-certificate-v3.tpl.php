<?php

/**
 * @file
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $caption: The caption for this table. May be empty.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
?>
<?php
$cat_nid = $view->args[0];
$cat_path_alias = drupal_get_path_alias('node/' . $cat_nid);

$first_result_key = key($result);
$course_nid = $result[$first_result_key]->node_field_data_field_cp_courses_nid;
$course_node = node_load($course_nid);
$course_description = $course_node->field_course_abstract['und'][0]['safe_value'];
$course_pdf = file_create_url($course_node->field_course_pdf['und'][0]['uri']);

$related_category = $cat_nid;
if ($view->result) {
  // Get the related category from the first result row.
  if (!empty($view->result[0]) && !empty($view->result[0]->_field_data['nid']['entity']->field_cp_related_categories)) {
     $related_category = $view->result[0]->_field_data['nid']['entity']->field_cp_related_categories[LANGUAGE_NONE][0]['target_id'];
  }
}
?>
<div class="alt_bg">
  <fieldset class="collapsible collapsed" itemscope itemtype="http://schema.org/EducationEvent">
    <legend class="p-a-1 container-row cf" style="width: 100%;">
      <div class="col9"><h3><?php print $title; ?></h3></div>
      <div class="col3 align-right"><span class="fieldset-legend"><img src="/sites/all/themes/agi/assets/images/trans_bg_100.png" style="height: 40px; width: 150px; margin: 0 auto;" alt="expand icon"></span></div>
    </legend>


    <div class="fieldset-wrapper m-t-1">
      <p class="p-l-1 p-r-1"><?php print $course_description; ?></p>

      <div class="container-row align-center m-b-1 cf">
        <div class="col4"><a href="/reviews/<?php print $related_category . '/' . $cat_path_alias; ?>">
          <img src="/sites/all/themes/agi/assets/images/star-blue.png" alt="Rating Star">
          <img src="/sites/all/themes/agi/assets/images/star-blue.png" alt="Rating Star">
          <img src="/sites/all/themes/agi/assets/images/star-blue.png" alt="Rating Star">
          <img src="/sites/all/themes/agi/assets/images/star-blue.png" alt="Rating Star">
          <img src="/sites/all/themes/agi/assets/images/star-blue.png" alt="Rating Star">
          See Reviews</a>
        </div>
        <div class="col4"><a href="/<?php print drupal_get_path_alias('node/' . $course_nid); ?>"><img src="/sites/all/themes/agi/assets/images/paper-icon.png" alt="Details Icon"> View Class Details</a></div>
        <div class="col4"><a href="<?php print $course_pdf; ?>" target="_blank"><img src="/sites/all/themes/agi/assets/images/pdf-blue-icon.png" alt="PDF Icon"> Download Course Outline</a></div>
      </div>

      <p class="h4 blue-light-bg p-a-1 no-mar-bot"><span itemprop="name"><?php print strip_tags($title); ?></span> Course Dates</p>
      <table <?php if ($classes) { print 'class="'. $classes . '" '; } ?><?php print $attributes; ?> style="width: 100%;">
      <table <?php if ($classes) { print 'class="'. $classes . '" '; } ?><?php print $attributes; ?> style="width: 100%;">
        <?php if (!empty($header)) : ?>
          <thead>
            <tr>
              <?php foreach ($header as $field => $label): ?>
                <th <?php if ($header_classes[$field]) { print 'class="'. $header_classes[$field] . '" '; } ?>>
                  <?php print $label; ?>
                </th>
              <?php endforeach; ?>
            </tr>
          </thead>
        <?php endif; ?>
        <tbody>
          <?php foreach ($rows as $row_count => $row): ?>
            <tr <?php if ($row_classes[$row_count]) { print 'class="' . implode(' ', $row_classes[$row_count]) .'"';  } ?>>
              <?php foreach ($row as $field => $content): ?>

                <td <?php if ($field_classes[$field][$row_count]) { print 'class="'. $field_classes[$field][$row_count] . '" '; } ?><?php print drupal_attributes($field_attributes[$field][$row_count]); ?>>
                  <?php print $content; ?>

                  <?php if ($field == 'field_event_start_date') : ?>
                    <div class="dom-only">
                      <img itemprop="image" src="https://www.agitraining.com/sites/all/themes/agi/assets/images/Adobe-Training-AGI.png">
                      <p abbrev="city"><?php print $row['field_city_for_reference_pages']; ?></p>
                    </div>
                  <?php endif; ?>

                  <?php if ($field == 'field_city_for_reference_pages') : ?>
                    <?php $location = !empty($result[$row_count]->_field_data['node_field_data_field_event_location_nid']) ? $result[$row_count]->_field_data['node_field_data_field_event_location_nid']['entity'] : NULL; ?>
                    <?php if ($location) : ?>
                    <div style="display:none;">
                      <span itemprop="location" itemscope itemtype="http://schema.org/Place">
                        <span itemprop="name">American Graphics Institute</span>
                        <span itemprop="address" itemscope  itemtype="http://schema.org/PostalAddress">
                          <span itemprop="addressLocality"><?php print $row['field_city_for_reference_pages']; ?></span>
                          <?php if (!empty($location->field_location_city)) : ?>
                            <span itemprop="addressRegion"><?php print $location->field_location_city[LANGUAGE_NONE][0]['value']; ?></span>
                          <?php endif; ?>
                        </span>
                      </span>
                    </div>
                    <?php endif; ?>
                  <?php endif; ?>
                </td>



              <?php endforeach; ?>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>


  </fieldset>
</div>
