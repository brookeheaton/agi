<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
$city = $row->field_field_address_city[0]['rendered']['#markup'];
$state = $row->field_field_address_state[0]['rendered']['#markup'];
?>
<?php
// @TODO: JY 11/2016 Remember to remove this asap after migration to JSON-LD is complete.
// Dont print rich snippets if illustrator related pages.
if ($row->node_field_data_field_cat_for_location_nid == 38) : ?>
  <div>
    <div><strong>AGI Training <?php print render($row->field_field_address_city[0]['rendered']); ?></strong></div>
    <div><p><span><?php print render($row->field_field_address_street[0]['rendered']); ?></span><br><span><?php print render($row->field_field_address_city[0]['rendered']); ?></span>, <span><?php print render($row->field_field_state_abbreviated_[0]['rendered']); ?></span><br><span><?php print render($row->field_field_address_zip[0]['rendered']); ?></span><br><span>USA</span><br><span>(800) 851-9237</span></p></div>
    <p>Offering <?php print render($row->field_field_short_category_name[0]['rendered']); ?> classes and <?php print render($row->field_field_short_category_name[0]['rendered']); ?> training in <?php print render($row->field_field_address_city[0]['rendered']); ?>, <?php print render($row->field_field_state_abbreviated_[0]['rendered']); ?>.</p>
  </div>
  <p><a href="<?php print url(current_path(), ['absolute' => TRUE]); ?>">See all Training Classes available in <?php print $city; ?>, <?php print $state; ?></a>.</p>
<?php else : ?>
  <div itemscope="" itemtype="http://schema.org/EducationalOrganization">
    <div itemprop="name"><strong>AGI Training <?php print render($row->field_field_address_city[0]['rendered']); ?></strong></div>
    <div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
      <p>
        <span itemprop="streetAddress"><?php print render($row->field_field_address_street[0]['rendered']); ?></span><br>
        <span itemprop="addressLocality"><?php print render($row->field_field_address_city[0]['rendered']); ?></span>, <span itemprop="addressRegion"><?php print render($row->field_field_state_abbreviated_[0]['rendered']); ?></span><br>
        <span itemprop="postalCode"><?php print render($row->field_field_address_zip[0]['rendered']); ?></span><br>
        <span itemprop="addressCountry">USA</span><br>
        <?php if (!empty($row->field_field_location_tel)) : ?>
          <span itemprop="telephone"><?php print render($row->field_field_location_tel[0]['rendered']); ?></span>
        <?php else: ?>
          <span itemprop="telephone">(800) 851-9237</span>
        <?php endif; ?>
      </p>
    </div>
    <p itemprop="description">Offering <?php print render($row->field_field_short_category_name[0]['rendered']); ?> classes and <?php print render($row->field_field_short_category_name[0]['rendered']); ?> training in <?php print render($row->field_field_address_city[0]['rendered']); ?>, <?php print render($row->field_field_state_abbreviated_[0]['rendered']); ?>.</p>
  </div>
  <p><a href="<?php print url('node/' . $row->node_field_data_field_secondary_location_nid); ?>">See all Training Classes available in <?php print $city; ?>, <?php print $state; ?></a>.</p>
<?php endif; ?>
