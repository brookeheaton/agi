<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
$category = isset($row->field_field_review_category) ? $row->field_field_review_category[0]['raw']['entity'] : NULL;
$categoryView = views_get_view("reviews");
$categoryView->set_display("ctools_context_1");
$categoryView->set_arguments([0 => $category->nid]);
$categoryView->execute();
$scores = [];
$totalScoreValue = 0;
$categoryAverageScore = 5;
foreach ($categoryView->result as $item) {
  if (!empty($item->field_field_review_rating)) {
    $scores[] = $item->field_field_review_rating[0]['raw']['value'];
    $totalScoreValue = $totalScoreValue + $item->field_field_review_rating[0]['raw']['value'];
  }
}

$categoryTotalReviews = count($scores);
$categoryAverageScore = round($totalScoreValue / $categoryTotalReviews, 0, PHP_ROUND_HALF_UP);
$categoryShortName = !empty($category->field_short_category_name) ? $category->field_short_category_name[LANGUAGE_NONE][0]['value'] : str_replace("classes", "", strtolower($category->title));
?>
<div itemprop="itemReviewed" itemscope itemtype="https://schema.org/Product">
  <meta itemprop="name" content="<?php print $category->title; ?>">
  <meta itemprop="description" content="AGI offers regularly scheduled <?php print $category->title; ?> along with private and customized <?php print $categoryShortName; ?> training programs">
  <meta itemprop="url" content="<?php print url(drupal_get_path_alias("node/" . $category->nid), ['absolute' => 'html']); ?>">
  <meta itemprop="brand" itemtype="http://schema.org/Brand" content="American Graphics Institute">
  <meta itemprop="mpn" content="<?php print $category->nid; ?>">
  <meta itemprop="sku" content="<?php print $category->title; ?>">
  <meta itemprop="image" content="https://www.agitraining.com/sites/all/themes/agi/assets/images/Adobe-Training-AGI.pnge">
  <span itemprop="offers" itemscope="" itemtype="http://schema.org/AggregateOffer">
          <meta itemprop="offerCount" content="10">
          <meta itemprop="lowPrice" content="450">
          <meta itemprop="highPrice" content="995">
          <meta itemprop="priceCurrency" content="USD">
      </span>
  <div itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
    <meta itemprop="worstRating" content="1">
    <meta itemprop="bestRating" content="5">
    <meta itemprop="ratingValue" content="<?php print $categoryAverageScore; ?>">
    <meta itemprop="reviewCount" content="<?php print $categoryTotalReviews; ?>">
  </div>
  <div itemprop="review" itemscope itemtype="http://schema.org/Review">
    <div itemprop="reviewRating" itemscope itemtype="https://schema.org/Rating">
      <meta itemprop="ratingValue" content="5">
    </div>
    <?php foreach ($fields as $id => $field): ?>

      <?php if (!empty($field->separator)): ?>
        <?php print $field->separator; ?>
      <?php endif; ?>

      <?php print $field->wrapper_prefix; ?>
      <?php print $field->label_html; ?>

      <?php print $field->content; ?>

      <?php print $field->wrapper_suffix; ?>

    <?php endforeach; ?>
  </div>
</div>

