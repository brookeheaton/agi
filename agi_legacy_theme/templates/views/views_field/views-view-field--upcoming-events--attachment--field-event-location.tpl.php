<?php $node = menu_get_object(); $location = $node->field_location_city[LANGUAGE_NONE][0]['value']; ?>
<span><?php print $location; ?><br>(online)</span>
<div style="display:none;">
  <span itemprop="location" itemscope itemtype="http://schema.org/Place">
    <span itemprop="name">American Graphics Institute</span>
    <span itemprop="address" itemscope  itemtype="http://schema.org/PostalAddress">
      <span itemprop="addressLocality"><?php print $location; ?></span>
    </span>
  </span>
</div>
