<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
$location_nid = $row->field_field_event_location[0]["raw"]["target_id"];
$node_from_menu = menu_get_object();
$rich_snippets = TRUE;
?>
<?php
if (!empty($node_from_menu) && is_object($node_from_menu) && $node_from_menu->type == 'category_location_page' && $node_from_menu->field_cat_for_location[LANGUAGE_NONE][0]['target_id'] != 38) {
  $rich_snippets = TRUE;
}
elseif (
  // @TODO: JY 11/2016 Remember to remove this asap after migration to JSON-LD is complete.
//  ($view->name == 'secondary_location_upcoming_events' && $row->node_field_data_field_cat_for_location_nid == 38) ||
  (arg(0) == 'node' && arg(1) == '38') ||
  ($view->name == 'upcoming_events' && isset($row->node_field_data_field_primary_category_nid) && $row->node_field_data_field_primary_category_nid == 38 && $view->current_display != "block_7") ||
  ($view->name == 'upcoming_events' && isset($row->field_field_event_name) && $row->field_field_event_name[0]['raw']['entity']->field_primary_category[LANGUAGE_NONE][0]['target_id'] == 38 && $view->current_display != "block_7" && $row->node_field_data_field_primary_category_nid != "9720") ||
  ($view->name == 'pri' && isset($row->field_field_event_name) && $row->field_field_event_name[0]['raw']['entity']->field_primary_category[LANGUAGE_NONE][0]['target_id'] == 38 && $location_nid != "1702" && !in_array($view->args[0], ILLUSTRATOR_ALLOW_RICH_SNIPPETS))
) {
  $rich_snippets = FALSE;
}

$event_frequency = !empty($row->field_field_event_frequency) ? $row->field_field_event_frequency[0]['raw']['taxonomy_term']->name : 'weekday';
$time = "10:00 am to 5:00 pm";
if (strpos(strtolower($event_frequency), "nights") !== FALSE) {
  $time = "6:00 pm to 9:00 pm";
}
?>

<?php
if (isset($row->_field_data['product_id']['entity']->field_half_day_course[LANGUAGE_NONE][0]['value']) && $row->_field_data['product_id']['entity']->field_half_day_course[LANGUAGE_NONE][0]['value']) {
  $time = "12:30 pm - 5:00 pm";
}
// Set a boolean value based upon if it is a single or mult-day event.
if ($row->field_field_event_start_date[0]['raw']['value'] == $row->field_field_event_start_date[0]['raw']['value2']) {
  $single_day = TRUE;
}
elseif ($row->field_field_event_start_date[0]['raw']['value2'] == NULL) {
  $single_day = TRUE;
}
else {
  $single_day = FALSE;
}
?>

<?php if (!$rich_snippets) : ?>
  <?php print str_replace("itemprop=\"startDate\"", "", $output); ?>
  <?php print '<br /><span class="text-muted text-sm">' . $time . '</span>'; ?>
<?php else : ?>

<?php
if ($single_day) {
  print $output . '<br><span class="text-muted text-sm">' . $time . '</span>';
}
else {
  // if a multi-day event we need to split up the rich snippet values.
  $output = explode(' to ', $output);

  if ($output[1]) {
    print $output[0] . '</span> to<br><span itemprop="endDate">' . $output[1];
  }
  else {
    print $output[0];
  }
  print '<br>' . '<span class="text-muted text-sm">' . $time . '</span>';
}
?>

<?php
// Location Info
$city = NULL;
$state = NULL;
if (isset($row->_field_data['node_field_data_field_secondary_location_nid'])) {
  $secondary_location = $row->_field_data['node_field_data_field_secondary_location_nid']['entity'];
  if (!empty($secondary_location->field_address_city)) {
    $city = $secondary_location->field_address_city[LANGUAGE_NONE][0]['value'];
  }
  if (!empty($secondary_location->field_state_abbreviated_)) {
    $state = $secondary_location->field_state_abbreviated_[LANGUAGE_NONE][0]['value'];
  }
}
?>
<div style="display: none;">
  <?php
  $class_name = '';
  if (isset($row->field_field_event_name[0]['raw']['entity']->title)) {
    $class_name = $row->field_field_event_name[0]['raw']['entity']->title;
  }
  elseif (isset($row->node_field_data_field_event_name_title)) {
    $class_name = $row->node_field_data_field_event_name_title;
  }
  elseif (isset($row->node_field_data_field_cp_courses_title)) {
    $class_name = $row->node_field_data_field_cp_courses_title;
  }
  $location = $city . ' (online)';
  if (isset($row->field_field_event_location[0]['raw']['entity']->field_city_for_reference_pages[LANGUAGE_NONE][0]['safe_value'])) {
    $location = $row->field_field_event_location[0]['raw']['entity']->field_city_for_reference_pages['und'][0]['safe_value'];
  }
  elseif (isset($row->node_field_data_field_event_location_title)) {
    $location = $row->node_field_data_field_event_location_title;
  }
  elseif (isset($row->field_field_city_for_reference_pages) && !empty($row->field_field_city_for_reference_pages)) {
    $location = $row->field_field_city_for_reference_pages[0]['raw']['value'];
  }

  $start_date = substr($row->field_field_event_start_date[0]['raw']['value'], 0, strrpos($row->field_field_event_start_date[0]['raw']['value'], ' '));
  $end_date = substr($row->field_field_event_start_date[0]['raw']['value'], 0, strrpos($row->field_field_event_start_date[0]['raw']['value'], ' '));
  if (isset($row->field_field_event_start_date[0]['raw']['value2'])) {
    $end_date = substr($row->field_field_event_start_date[0]['raw']['value2'], 0, strrpos($row->field_field_event_start_date[0]['raw']['value'], ' '));
  }
  /**
  * @todo: do away with hard-coded image defintion & properly associate image with location node
  */
  switch ($location) {
    case 'Boston':
      $image_file = 'training-classes-boston-american-graphics-institute.jpg';
      break;
    case 'NYC':
      $image_file = 'agi-adobe-training-nyc.jpg';
      break;
    case 'Philadelphia':
      $image_file = 'agi-adobe-training-philadelphia.jpg';
      break;
    default:
      $image_file = 'training-class-online-agi.jpg';
      break;
  }
  ?>
<?php
if ($single_day) {
  echo '<meta itemprop="endDate" content="' . $end_date . '" />';
}
?>
<meta itemprop="description" content="<?php print $class_name . ', ' . $location . ' on ' . $start_date; ?>" />
<span itemscope itemprop="performer" itemtype="http://schema.org/Organization">
  <span itemprop="name">American Graphics Institute</span>
</span>
<span itemscope itemprop="organizer" itemtype="http://schema.org/Organization">
  <span itemprop="name">American Graphics Institute</span>
  <span itemprop="url">https://www.agitraining.com</span>
</span>
<meta itemprop="image" content="https://www.agitraining.com/sites/all/themes/agi/assets/images/<?php echo $image_file; ?>" />
<meta itemprop="eventStatus" content="EventScheduled" />
<?php if (strpos(strtolower($city), "online") !== FALSE) : ?>
  <meta itemprop="eventAttendanceMode" content="OnlineEventAttendanceMode">
<?php else : ?>
  <meta itemprop="eventAttendanceMode" content="OfflineEventAttendanceMode">
<?php endif; ?>
</div>
<?php endif; ?>
