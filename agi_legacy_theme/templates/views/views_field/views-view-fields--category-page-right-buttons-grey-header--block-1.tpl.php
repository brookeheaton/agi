<?php
$is_online = FALSE;
// Remove map links on Online classes.
if ($view->name == 'category_page_right_buttons_grey_header' && $view->current_display == 'block_1') {
  if (!empty($view->result)) {
    // Check first row for the location for this category.
    if ($view->result[0]->node_field_data_field_location_for_cat_nid == '1702') {
      $is_online = TRUE;
    }
  }
}
?>


<?php foreach ($fields as $id => $field): ?>

  <?php
  if ($is_online && $id == "nothing_2") {
    continue;
  }
  ?>

  <?php if (!empty($field->separator)): ?>
    <?php print $field->separator; ?>
  <?php endif; ?>

  <?php print $field->wrapper_prefix; ?>
  <?php print $field->label_html; ?>
  <?php print $field->content; ?>
  <?php print $field->wrapper_suffix; ?>
<?php endforeach; ?>
