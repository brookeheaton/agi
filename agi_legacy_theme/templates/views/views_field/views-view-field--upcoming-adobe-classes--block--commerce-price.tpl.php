<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>

<?php
// Strip the dollar sign from the price for rich snippet usage.
$price_decimal_no_usd = str_replace('$','',$field->original_value);

// Get the path for rich snippet.
$detail_page_path = drupal_get_path_alias('node/' . $row->field_cd_event_commerce_product_nid);

print '<span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer"><a href="/' . $detail_page_path . '" itemprop="url"></a>
<link itemprop="availability" href="http://schema.org/InStock" />';
if (isset($row->_field_data['product_id']['entity']->created)) {
  $validFrom = date('Y-m-d H:i:s', $row->_field_data['product_id']['entity']->created);
  print '<span itemprop="validFrom" style="display: none">' . $validFrom . '</span>';
}
print '<span itemprop="priceCurrency" content="USD">$</span><span itemprop="price" content="' . $price_decimal_no_usd . '">' . $price_decimal_no_usd . '</span></span>';
?>
