<?php
$event_frequency = "Weekdays";
if (!empty($row->field_field_event_frequency)) {
  $event_frequency = $row->field_field_event_frequency[0]['rendered']['#markup'];
}
$additional_info = "";
if (!empty($row->field_field_additional_information)) {
  $additional_info = $row->field_field_additional_information[0]['rendered']['#markup'];
}

if (
  $row->field_field_event_name[0]['raw']['entity']->field_primary_category[LANGUAGE_NONE][0]['target_id'] == 38 &&
  $view->current_display != "block_7" &&
  // Graphic design course needs Illustrator to have inline snippets because there is a mix of non-illustrator classes.
  $row->node_field_data_field_primary_category_nid != "9720"
) : ?>
  <?php print $output; ?>
  <?php print '<div class="frequency text-sm text-muted mt-1">' . $event_frequency . '</div>'; ?>
  <?php print '<div class="additional-info text-sm text-muted mt-1">' . $additional_info . '</div>'; ?>

<?php else : ?>
<?php
$output = str_replace('<a ', '<a itemprop="url" ', $output);
?>

<?php
// TODO: Set values for variables.
$class_name = $row->field_field_course_name[0]['raw']['safe_value'];
$city = t('Online');
if (isset($row->field_field_event_location[0]['raw']['entity']->field_city_for_reference_pages['und'][0]['safe_value'])) {
  $city = $row->field_field_event_location[0]['raw']['entity']->field_city_for_reference_pages['und'][0]['safe_value'];
}
$description = $class_name . ' in ' . $city;

$course_nid = $row->field_field_event_name[0]['raw']['target_id'];
$url = drupal_get_path_alias('node/' . $course_nid);
?>

<div>
  <div itemprop="name"><?php print $output; ?></div>
  <div class="frequency text-sm text-muted mt-1"><?php print $event_frequency; ?></div>
  <div class="additional-information text-sm text-muted mt-1"><?php print $additional_info; ?></div>
  <div class="dom-only">
    <img itemprop="image" src="https://www.agitraining.com/sites/all/themes/agi/assets/images/Adobe-Training-AGI.png" />
    <p abbrev="city"><?php print $city; ?></p>

    <meta itemprop="eventStatus" content="EventScheduled" />
    <?php if (strpos(strtolower($city), "online") !== FALSE) : ?>
     <meta itemprop="eventAttendanceMode" content="OnlineEventAttendanceMode">
    <?php else : ?>
      <meta itemprop="eventAttendanceMode" content="OfflineEventAttendanceMode">
    <?php endif; ?>

  </div>
</div>
<?php endif; ?>
