<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */

$city = t('Online');
if (isset($row->field_field_event_location[0]['raw']['entity']->field_city_for_reference_pages['und'][0]['safe_value'])) {
  $city = $row->field_field_event_location[0]['raw']['entity']->field_city_for_reference_pages['und'][0]['safe_value'];
}
$event_frequency = "Weekdays";
if (!empty($row->field_field_event_frequency)) {
  $event_frequency = $row->field_field_event_frequency[0]['rendered']['#markup'];
}
$additional_info = "";
if (!empty($row->field_field_additional_information)) {
  $additional_info = $row->field_field_additional_information[0]['rendered']['#markup'];
}
?>
<?php if ($view->name == 'pri' && $row->field_courses_in_category_node_nid == 38) : ?>
  <div><?php print $output; ?></div>
  <div class="frequency text-sm text-muted mt-1"><?php print $event_frequency; ?></div>
  <div class="additional-info text-sm text-muted mt-1"><?php print $additional_info; ?></div>
<?php else : ?>
<div itemprop="name"><?php print $output; ?></div>
  <div class="frequency text-sm text-muted mt-1"><?php print $event_frequency; ?></div>
  <div class="additional-info text-sm text-muted mt-1"><?php print $additional_info; ?></div>
  <meta itemprop="eventStatus" content="EventScheduled" />
  <?php if (strpos(strtolower($city), "online") !== FALSE) : ?>
    <meta itemprop="eventAttendanceMode" content="OnlineEventAttendanceMode">
  <?php else : ?>
    <meta itemprop="eventAttendanceMode" content="OfflineEventAttendanceMode">
  <?php endif; ?>
<?php endif; ?>
