<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen ify
 * the view is modified.
 */
?>
<?php

print $output;
// ** MOVED TO agi_misc.module **
//$address_check_array = $row->field_field_event_location[0]['raw']['entity']->field_location_address;
//
//$primary_category_nid = $row->field_field_event_name[0]['raw']['entity']->field_primary_category['und'][0]['target_id'];
//$location_nid = $row->field_field_event_location[0]['raw']['target_id'];
//
//// Get the category location page.
//$cat_loc_result = db_query('
//          SELECT s.entity_id
//          FROM field_data_field_cat_for_location s
//          LEFT JOIN field_data_field_location_for_cat l ON s.entity_id=l.entity_id
//          WHERE l.field_location_for_cat_target_id = :location
//          AND s.field_cat_for_location_target_id = :category', array(':location' => $location_nid, ':category' => $primary_category_nid)
//);
//
//$cat_loc_nid = $cat_loc_result->fetchField();
//
//$cat_loc_alias = url('node/' . $cat_loc_nid);
//
//$current_path = current_path();
//
//if (!empty($address_check_array)) {
//  // Define variables.
//   $title = $row->field_field_event_location[0]['raw']['entity']->field_location_name['und'][0]['safe_value'];
//  $address = $row->field_field_event_location[0]['raw']['entity']->field_location_address['und'][0]['safe_value'];
//  $city = $row->field_field_event_location[0]['raw']['entity']->field_location_city['und'][0]['safe_value'];
//  $state = $row->field_field_event_location[0]['raw']['entity']->field_location_state['und'][0]['safe_value'];
//  $zip = $row->field_field_event_location[0]['raw']['entity']->field_location_zip['und'][0]['safe_value'];
//  $city_url = drupal_get_path_alias('node/' . $row->field_field_event_location[0]['raw']['entity']->nid);
//
//  if ($city == 'Woburn') {
//    $big_city = 'Boston';
//  }
//  elseif ($city == 'Conshohocken') {
//    $big_city = 'Philadelphia';
//  }
//  elseif ($city == 'New York') {
//    $big_city = 'New York';
//  }
//
//  if ('node/' . $cat_loc_nid != $current_path) {
//    $location_string = '<a href="' . $cat_loc_alias . '">' . $big_city . '</a>';
//  }
//  else {
//    $location_string = $big_city;
//  }
//
//
//  // Create output.
//  $output = '<span itemprop="location" itemscope itemtype="http://schema.org/Place">';
//  $output .= '<span class="font-xs">' . $location_string . '</span><br/>';
//  $output .= '<span itemprop="name" class="font-xxs mar-top-10">American Graphics Institute</span><br/>';
//  $output .= '<span itemprop="alternateName" style="display:none">' . $title . '</span>';
//  $output .= '<div class="font-xxs" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">';
//  $output .= '<span itemprop="streetAddress">' . $address . ' </span><br/>';
//  $output .= '<span itemprop="addressLocality">' . $city . ', </span>';
//  $output .= '<span itemprop="addressRegion">' . $state . ' </span><span itemprop="postalCode">' . $zip . ' </span>';
//  $output .= '</div></span>';
//
//  print $output;
//  }
//
//  else {
//
//    if ('node/' . $cat_loc_nid != $current_path) {
//      $location_string = '<a href="' . $cat_loc_alias . '">Online</a>';
//    }
//    else {
//      $location_string = 'Online';
//    }
//
//    // Define variables.
//    $title = $row->field_field_event_location[0]['raw']['entity']->field_location_name['und'][0]['safe_value'];
//
//    // Create output.
//    $output = '<span itemprop="location" itemscope itemtype="http://schema.org/Place">';
//    $output .= '<span itemprop="name">' . $location_string . '</span>';
//    $output .= '<div class="font-xxs mar-top-10">Live instructor-led class <span itemprop="address">online</span></div></span>';
//
//    print $output;
//  }

?>
