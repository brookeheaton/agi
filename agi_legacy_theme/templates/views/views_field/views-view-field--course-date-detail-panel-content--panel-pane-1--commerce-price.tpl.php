<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>

<?php
// Strip the dollar sign from the price for rich snippet usage.
$price_decimal_no_usd = str_replace('$','',$field->original_value);
$price_for_rich_snippet = str_replace(',','',$price_decimal_no_usd);
$created_date = date('Y-m-d H:i:s', $row->_field_data['node_field_data_field_event_name_nid']['entity']->created);
print '<span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer"><span itemprop="validFrom" style="display: none">' . $created_date . '</span><link itemprop="availability" href="http://schema.org/InStock" />
<span itemprop="priceCurrency" content="USD">$</span><span itemprop="price" content="' . $price_for_rich_snippet . '">' . $price_decimal_no_usd . '</span>';

// TODO: Get path alias of the node this is being displayed upon.
$path_alias = drupal_get_path_alias();
print '<span style="display:none"><a href="https://www.agitraining.com/' . $path_alias .'" itemprop="url">Registration Page</a></span></span></span>';
?>
