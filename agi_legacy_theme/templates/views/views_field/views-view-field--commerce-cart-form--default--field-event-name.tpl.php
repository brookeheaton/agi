<?php
$event_frequency = "Weekdays";
if (!empty($row->field_field_event_frequency)) {
  $event_frequency = $row->field_field_event_frequency[0]['rendered']['#markup'];
}
$additional_info = "";
if (!empty($row->field_field_additional_information)) {
  $additional_info = $row->field_field_additional_information[0]['rendered']['#markup'];
}
?>
<?php print $output; ?>
<?php print '<div class="frequency text-sm text-muted mt-1">' . $event_frequency . '</div>'; ?>
<?php print '<div class="additional-info text-sm text-muted mt-1">' . $additional_info . '</div>'; ?>
