<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * Override the default rendering of the location field to also display rich
 * snippet data, which is hidden from the user.
 */
?>

  <?php

  print $output;

  $location = $row->node_field_data_field_event_location_title;

  switch ($location) {
    case 'AGI Training Boston':
      $rich_snippet = '<div itemprop="location" itemscope itemtype="http://schema.org/Place" style="display: none;">
        <span itemprop="name">American Graphics Institute</span>
        <span itemprop="alternateName">AGI Training Boston</span>
        <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
          <span itemprop="streetAddress">120 Presidential Way </span><br/><span itemprop="addressLocality">Woburn </span>
          <span itemprop="addressRegion">MA </span>
          <span itemprop="postalCode">01801 </span>
        </div>
      </div>';
      break;

    case 'AGI Training New York City':
      $rich_snippet = '<div itemprop="location" itemscope itemtype="http://schema.org/Place" style="display: none;">
        <span itemprop="name">American Graphics Institute</span>
        <span itemprop="alternateName">AGI Training New York</span>
        <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
          <span itemprop="streetAddress">21 W 46th St. </span><br/><span itemprop="addressLocality">New York </span>
          <span itemprop="addressRegion">NY </span>
          <span itemprop="postalCode">10036 </span>
        </div>
      </div>';
      break;

    case 'AGI Training Philadelphia':
      $rich_snippet = '<div itemprop="location" itemscope itemtype="http://schema.org/Place" style="display: none;">
        <span itemprop="name">American Graphics Institute</span>
        <span itemprop="alternateName">AGI Training Philadelphia</span>
        <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
          <span itemprop="streetAddress">101 West Elm Street </span><br/><span itemprop="addressLocality">Conshohocken </span>
          <span itemprop="addressRegion">PA </span>
          <span itemprop="postalCode">19428 </span>
        </div>
      </div>';
      break;

    case 'Online Training':
      $rich_snippet = '<div itemprop="location" itemscope itemtype="http://schema.org/Place" style="display: none;">
        <span itemprop="name">American Graphics Institute</span>
        <span itemprop="alternateName">AGI Training Online</span>
          <span itemprop="address">Online</span>
      </div>';
      break;

    default :
      $rich_snippet = '<div itemprop="location" itemscope itemtype="http://schema.org/Place" style="display: none;">
        <span itemprop="name">American Graphics Institute</span>
      </div>';
      break;
  }

  print $rich_snippet;
  ?>




