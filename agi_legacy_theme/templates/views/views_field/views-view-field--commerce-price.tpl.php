<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>

<?php
$node_from_menu = menu_get_object();
$rich_snippets = TRUE;
// Strip the dollar sign from the price for rich snippet usage.
$price_decimal_no_usd = str_replace('$','',$field->original_value);
$price_for_rich_snippet = str_replace(',','',$price_decimal_no_usd);

// Get the path for rich snippet.
$detail_page_path = '';
if (isset($row->field_cd_event_commerce_product_nid)) {
  $detail_page_path = drupal_get_path_alias('node/' . $row->field_cd_event_commerce_product_nid);
}
$location_nid = $row->field_field_event_location[0]["raw"]["target_id"];

$currentNode = menu_get_object();
$currentNodeCategory = NULL;
if (!empty($currentNode) && isset($currentNode->nid) && $currentNode->type == "category_location_page") {
  $currentNodeCategory = $currentNode->field_cat_for_location[LANGUAGE_NONE][0]['target_id'];
}
?>
<?php
if (!empty($node_from_menu) && is_object($node_from_menu) && $node_from_menu->type == 'category_location_page' && $node_from_menu->field_cat_for_location[LANGUAGE_NONE][0]['target_id'] != 38) {
  $rich_snippets = TRUE;
}
elseif (
  // @TODO: JY 11/2016 Remember to remove this asap after migration to JSON-LD is complete.
//  ($view->name == 'secondary_location_upcoming_events' && $row->node_field_data_field_cat_for_location_nid == 38) ||
  (arg(0) == 'node' && arg(1) == '38') ||
  ($view->name == 'upcoming_events' && isset($row->node_field_data_field_primary_category_nid) && $row->node_field_data_field_primary_category_nid == 38 && $view->current_display != "block_7") ||
  ($view->name == 'upcoming_events' && isset($row->field_field_event_name) && $row->field_field_event_name[0]['raw']['entity']->field_primary_category[LANGUAGE_NONE][0]['target_id'] == 38 && $view->current_display != "block_7" && $row->node_field_data_field_primary_category_nid != "9720") ||
  ($view->name == 'pri' && isset($row->field_field_event_name) && ($row->field_field_event_name[0]['raw']['entity']->field_primary_category[LANGUAGE_NONE][0]['target_id'] == 38) && $location_nid != "1702" && !in_array($view->args[0], ILLUSTRATOR_ALLOW_RICH_SNIPPETS))
) {
  $rich_snippets = FALSE;
}
?>

<?php if (!$rich_snippets) : ?>
  <span><a href="/<?php print $detail_page_path; ?>"></a>
  <span>$</span><span><?php print $price_decimal_no_usd; ?></span></span>
<?php else : ?>
  <span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer"><a href="/<?php print $detail_page_path; ?>" itemprop="url"></a>
  <span itemprop="validFrom" style="display: none"><?php print date('c', $row->_field_data['field_event_name_node_product_id']['entity']->created); ?></span>
  <span itemprop="priceCurrency" content="USD">$</span><link itemprop="availability" href="http://schema.org/InStock" /><span itemprop="price" content="<?php print $price_for_rich_snippet; ?>"><?php print $price_decimal_no_usd; ?></span></span>
<?php endif; ?>
