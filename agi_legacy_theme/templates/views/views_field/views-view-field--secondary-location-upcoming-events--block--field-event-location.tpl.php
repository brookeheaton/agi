<?php
$city = t('Online');
if (isset($row->field_field_event_location[0]['raw']['entity']->field_city_for_reference_pages['und'][0]['safe_value'])) {
  $city = $row->field_field_event_location[0]['raw']['entity']->field_city_for_reference_pages['und'][0]['safe_value'];
}

// We are including nearby primary locations as of June 4th.
// Check the location in this row to understand if it is a primary or secondary location.
$locationType = "secondary";
if ($row->field_field_event_location[0]['raw']['entity']->nid != "1702") {
  $locationType = "primary";
}

$locationTitle = $city;
if ($locationType == "secondary") {
  $city = $row->field_field_address_city[0]['rendered']['#markup'] . '<br>(online)';
}
else {
  // Add a link the city.
  // Find category location page.
  $query = db_select('node', 'n')
    ->fields('n', ['nid'])
    ->condition('n.type', 'category_location_page')
    ->range(0,1);
  $query->join('field_data_field_location_for_cat', 'loc', 'loc.entity_id = n.nid');
  $query->join('field_data_field_cat_for_location', 'cat', 'cat.entity_id = n.nid');
  $query->condition('loc.field_location_for_cat_target_id', $row->field_field_event_location[0]['raw']['target_id']);
  $query->condition('cat.field_cat_for_location_target_id', $row->node_field_data_field_cat_for_location_nid);
  $results = $query->execute()->fetchAll();
  if ($results) {
    $category_location_page = drupal_get_path_alias('node/' . $results[0]->nid, ['absolute' => TRUE]);
    $city = l($city, $category_location_page);
  }
}
?>

<span><?php print $city; ?></span>
<div style="display:none;">
  <span itemprop="location" itemscope itemtype="http://schema.org/Place">
    <span itemprop="name">American Graphics Institute</span>
    <span itemprop="address" itemscope  itemtype="http://schema.org/PostalAddress">
      <span itemprop="addressLocality"><?php print $row->field_field_address_city[0]['rendered']['#markup']; ?></span>
      <?php if (isset($row->_field_data['node_field_data_field_secondary_location_nid'])) : ?>
        <span itemprop="addressRegion"><?php print $row->_field_data['node_field_data_field_secondary_location_nid']['entity']->field_state_abbreviated_[LANGUAGE_NONE][0]['value']; ?></span>
      <?php endif; ?>
    </span>
  </span>

  <meta itemprop="eventStatus" content="EventScheduled" />
  <?php if (strpos(strtolower($city), "online") !== FALSE) : ?>
    <meta itemprop="eventAttendanceMode" content="OnlineEventAttendanceMode">
  <?php else : ?>
    <meta itemprop="eventAttendanceMode" content="OfflineEventAttendanceMode">
  <?php endif; ?>
</div>
