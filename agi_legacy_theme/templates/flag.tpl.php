<?php

/**
 * @file
 * Default theme implementation to display a flag link, and a message after the action
 * is carried out.
 *
 * Available variables:
 *
 * - $flag: The flag object itself. You will only need to use it when the
 *   following variables don't suffice.
 * - $flag_name_css: The flag name, with all "_" replaced with "-". For use in 'class'
 *   attributes.
 * - $flag_classes: A space-separated list of CSS classes that should be applied to the link.
 *
 * - $action: The action the link is about to carry out, either "flag" or "unflag".
 * - $status: The status of the item; either "flagged" or "unflagged".
 *
 * - $link_href: The URL for the flag link.
 * - $link_text: The text to show for the link.
 * - $link_title: The title attribute for the link.
 *
 * - $message_text: The long message to show after a flag action has been carried out.
 * - $message_classes: A space-separated list of CSS classes that should be applied to
 *   the message.
 * - $after_flagging: This template is called for the link both before and after being
 *   flagged. If displaying to the user immediately after flagging, this value
 *   will be boolean TRUE. This is usually used in conjunction with immedate
 *   JavaScript-based toggling of flags.
 * - $needs_wrapping_element: Determines whether the flag displays a wrapping
 *   HTML DIV element.
 *
 * Template suggestions available, listed from the most specific template to
 * the least. Drupal will use the most specific template it finds:
 * - flag--name.tpl.php
 * - flag--link-type.tpl.php
 *
 * NOTE: This template spaces out the <span> tags for clarity only. When doing some
 * advanced theming you may have to remove all the whitespace.
 */
?>
<?php if ($needs_wrapping_element): ?>
<div class="flag-outer flag-outer-<?php print $flag_name_css; ?>">
  <?php endif; ?>
  <span class="<?php print $flag_wrapper_classes; ?>">
  <?php if ($link_href): ?>
    <a href="<?php print $link_href; ?>" title="<?php print $link_title; ?>" class="<?php print $flag_classes ?>"
       rel="nofollow"><?php print $link_text; ?></a><span class="flag-throbber">&nbsp;</span>
  <?php else: ?>
    <span class="<?php print $flag_classes ?>"><?php print $link_text; ?></span>
  <?php endif; ?>
    <?php if ($after_flagging): ?>
      <span class="<?php print $message_classes; ?>">
      <?php print $message_text; ?>
    </span>
    <?php endif; ?>
</span>
  <?php if ($needs_wrapping_element): ?>
</div>
<?php endif; ?>


<div class="mb-5"><h3>The online Creative Cloud Certification Exams validate your knowledge of Adobe&reg; Creative Cloud&reg;
    applications.</h3>
  <div class="row justify-content-center align-items-center">
    <div class="col4 col-md-4 text-center">
      <div><a href="/adobe/indesign/classes/indesign-certification-test-online"><img
            alt="InDesign Skills Assessment for Creative Cloud"
            src="/sites/default/files/indesign-skills-assessment.gif" style="width: 48px; height: 59px;"/></a></div>
      <h4 class="mt-3"><a href="/adobe/indesign/classes/indesign-certification-test-online">InDesign Certification Test
          Online</a></h4></div>
    <div class="col4 col-md-4 text-center">
      <div><a href="/adobe/photoshop/classes/photoshop-certification-test-online"><img
            alt="Photoshop Skills Assessment for Creative Cloud"
            src="/sites/default/files/photoshop-skills-assessment.gif" style="width: 76px; height: 59px;"/></a></div>
      <h4 class="mt-3"><a href="/adobe/photoshop/classes/photoshop-certification-test-online">Photoshop Certification
          Test Online</a></h4></div>
    <div class="col4 col-md-4 text-center">
      <div><a href="/adobe/illustrator/classes/illustrator-certification-test-online"><img
            alt="Illustrator Skills Assessment for Creative Cloud"
            src="/sites/default/files/illustrator-skills-assessment.gif" style="width: 103px; height: 62px;"/></a></div>
      <h4 class="mt-3"><a href="/adobe/illustrator/classes/illustrator-certification-test-online">Illustrator
          Certification Test Online</a></h4></div>
  </div>
</div>
<div style="margin-top: 25px;"><p>Creative Cloud Certification Tests from American Graphics Institute validates your
    skills and understanding of Adobe Creative Cloud tools. These certification exams provide an independent validation
    created by the expert instructors of American Graphics Institute who have more than a decade of experience with
    these applications. The online Creative Cloud Certification tests are developed by the authors of more than 20
    Creative Cloud books, including <em>Creative Cloud for Dummies</em> and <em>Creative Cloud Digital Classrom.</em>&nbsp;Use
    these Creative Cloud certifications to prove to employers or clients that you have the skills needed to effectively
    use the Creative Cloud applications.</p>
  <h3>Preparing for the online Creative Cloud Certification Test</h3>
  <p>There are several ways in which you can prepare for the Creative Cloud Certification Test. You can attend the&nbsp;<a
      href="/adobe/creative-cloud-training">Creative Cloud training classes</a>&nbsp;to&nbsp;learn the skills necessary
    for using these popular applications. These are available online or in-person. You can study on your own or use any
    the Creative Cloud books. You can take a practice Creative Cloud Certification test &nbsp;using the free <a
      href="/adobe/creative-cloud-training/creative-cloud-skill-assessment">Creative Cloud skills assessment</a> which
    is also available online.</p>
  <p>If your school, business, or organization is interested in having five or more people take the Creative Cloud
    Certification test, you can <a href="/contact">contact American Graphics Institute</a>&nbsp;for information about
    our group certification program.</p>
  <h2>Independent Creative Cloud Certification</h2>
  <p>The Creative Cloud Certification tests offered by American Graphics Institute are an independent validation of
    skills.&nbsp;Adobe and Creative Cloud are either registered trademarks or trademarks of Adobe Systems Incorporated
    in the United States and/or other countries.</p></div><p>&nbsp;</p>
