<?php
/**
 * @file
 * Themes the question report
 *
 */
/*
 * Naming Conventions for new templates:
 * Base template file name: quiz-report-form.tpl.php
 *   - Override a specific quiz's results page: quiz-report-form--quiz-[nid].tpl.php
 *     Where [nid] is the quiz nid
 *   - Override a specific quiz's specific result page: quiz-report-form--quiz-[nid]-result-[rid].tpl.php
 *     Where [nid] is the quiz nid and [rid] is the result id.
 *
 */

/**
 * Hide Form Elements that are being "pretty" rendered.
 * Do not remove this, or an ugly version of the question results
 * will display below your themed version.
 */
global $user;
if (isset($hide_form_element)) {
  foreach ($hide_form_element as $key) {
    hide($form[$key]);
  }
}
?>
<div class="agi-quiz-results <?php print $quiz_type; ?>">
  <div class="row">
    <div class="score col-xs-12">
      <?php print $score_summary; ?>
    </div>
  </div>

  <?php if (isset($certificate)) : ?>
    <div class="row">
      <div class="col-xs-12">
        <p class="view-certificate-instructions">Your certificate has been added to your member profile for you to view and share.</p>
        <?php print l('Return to site', '/view-course-history', array('attributes' => array('class' => array('btn', 'btn-success')))); ?>
        <?php print l('View Certificate', $certificate, array('attributes' => array('class' => array('btn', 'btn-success')))); ?>
      </div>
    </div>
  <?php else : ?>
    <div class="row">
      <div class="col-xs-12">
        <p class="failed-exam-instructions">You have not answered enough questions correctly for a passing score.</p>
        <?php print l('Return to site', '/view-course-history', array('attributes' => array('class' => array('btn')))); ?>
      </div>
    </div>
  <?php endif; ?>

  <?php if (isset($summary_pass)) : ?>
    <div class="row">
      <div class="col-xs-12">
        <div class="summary summary-pass"><?php print $summary_pass; ?></div>
      </div>
    </div>
  <?php elseif (isset($summary_default)) : ?>
    <div class="row">
      <div class="col-xs-12">
        <div class="summary summary-default"><?php print $summary_default; ?></div>
      </div>
    </div>
  <?php endif; ?>
</div>

