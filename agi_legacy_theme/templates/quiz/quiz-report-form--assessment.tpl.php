<?php
/**
 * @file
 * Themes the question report
 *
 */
/*
 * Naming Conventions for new templates:
 * Base template file name: quiz-report-form.tpl.php
 *   - Override a specific quiz's results page: quiz-report-form--quiz-[nid].tpl.php
 *     Where [nid] is the quiz nid
 *   - Override a specific quiz's specific result page: quiz-report-form--quiz-[nid]-result-[rid].tpl.php
 *     Where [nid] is the quiz nid and [rid] is the result id.
 *
 */

/**
 * Hide Form Elements that are being "pretty" rendered.
 * Do not remove this, or an ugly version of the question results
 * will display below your themed version.
 */
global $user;
if (isset($hide_form_element)) {
  foreach ($hide_form_element as $key) {
    hide($form[$key]);
  }
}
?>
<div class="agi-quiz-results container <?php print $quiz_type; ?>">
  <div class="row quiz-nav">
    <div class="col-xs-12">
      <h1><?php print $quiz_title; ?></h1>
    </div>
  </div>
  <div class="row">
    <div class="score col-xs-12 <?php print ($pass ? 'pass' : 'fail'); ?>">
      <p>You answered <?php print $total_correct; ?> out of <?php print $total_questions; ?> questions correctly</p>
    </div>
  </div>

  <?php if (!$pass) : ?>
    <div class="row incorrect-questions">
      <div class="col-xs-12">
        <p>Questions answered incorrectly:</p>
        <?php foreach($questions as $question) : ?>
          <?php if (!$question['is_correct']) : ?>
            <div class="question">
              <div class="question-text"><?php print render($question['question']['body'][0]['#markup']); ?></div>
              <div class="answer-feedback"><?php print render($question['answer_feedback']); ?></div>
            </div>
          <?php endif; ?>
        <?php endforeach; ?>
      </div>
    </div>
  <?php endif; ?>

  <?php if (isset($summary_pass)) : ?>
    <div class="row">
      <div class="col-xs-12">
        <div class="summary summary-pass"><?php print $summary_pass; ?></div>
      </div>
    </div>
  <?php elseif (isset($summary_default)) : ?>
    <div class="row">
      <div class="col-xs-12">
        <div class="summary summary-default"><?php print $summary_default; ?></div>
      </div>
    </div>
  <?php endif; ?>

  <div class="quiz-score-submit" style="display:none;">
    <?php print drupal_render_children($form); ?>
  </div>
</div>

