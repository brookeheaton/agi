<?php
/**
 * @file
 * Template file for the Quiz Answering form.
 */
hide($form['navigation']);
?>
<div class="agi-answering-form row <?php print $quiz_type; ?>">
  <div class="row quiz-nav">
    <div class="col-xs-12 col-sm-3">
      <?php print $quiz_progress; ?>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12">
      <div class="question">
        <?php print $question_text; ?>
        <?php print drupal_render_children($form); ?>
        <div class="row">
          <div class="col-xs-12 col-sm-3 col-sm-push-9">
            <div class="actions">
              <div class="form-group has-feedback submit">
                <?php print render($form['navigation']['submit']); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>