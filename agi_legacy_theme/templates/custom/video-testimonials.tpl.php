<?php
/**
 * @file
 * Template file for the Video Testimonials sidebar content.
 */
?>
<p class="large-text"><strong>Students talk about their<br>AGI Certificate program</strong></p>
<div class="mb-3"><a class="colorbox-inline" href="/?width=640&amp;height=360&amp;inline=true#certificate-program-testimonial-alyssa"><img alt="Certificate Program Testimonial - Alyssa" src="/sites/all/themes/agi/assets/images/certificate-program-testimonial-alyssa-play.jpg" style="max-width:100%"/></a></div>
<div class="mb-3"><a class="colorbox-inline" href="/?width=640&amp;height=360&amp;inline=true#certificate-program-testimonial-sean"><img alt="Certificate Program Testimonial - Sean" src="/sites/all/themes/agi/assets/images/certificate-program-testimonial-sean-play.jpg" style="max-width:100%"/></a></div>
<div class="mb-3"><a class="colorbox-inline" href="/?width=640&amp;height=360&amp;inline=true#certificate-program-testimonial-mike"><img alt="Certificate Program Testimonial - Mike" src="/sites/all/themes/agi/assets/images/certificate-program-testimonial-mike-play.jpg" style="max-width:100%"/></a></div>
<div style="display:none;"><video controls="" height="360" id="certificate-program-testimonial-alyssa" poster="/sites/all/themes/agi/assets/images/certificate-program-testimonial-alyssa.png" preload="none" width="100%"><source src="https://d2cqpmwr4b912e.cloudfront.net/certificate-program-testimonial-alyssa.mp4" type="video/mp4"></video></div>
<div style="display:none;"><video controls="" height="360" id="certificate-program-testimonial-sean" poster="/sites/all/themes/agi/assets/images/certificate-program-testimonial-sean.png" preload="none" width="100%"><source src="https://d2cqpmwr4b912e.cloudfront.net/certificate-program-testimonial-sean.mp4" type="video/mp4"></video></div>
<div style="display:none;"><video controls="" height="360" id="certificate-program-testimonial-mike" poster="/sites/all/themes/agi/assets/images/certificate-program-testimonial-mike.png" preload="none" width="100%"><source src="https://d2cqpmwr4b912e.cloudfront.net/certificate-program-testimonial-mike.mp4" type="video/mp4"></video></div>
