<?php
/**
 * NYC
 */
?>
<div class="row">
  <?php if (!empty($location_images) && !empty($location_images[0])) : ?>
  <div class="col-md-5">
    <?php print drupal_render($location_images[0]); ?>
  </div>
  <?php endif; ?>
  <div class="col-md-7">
    <h6 class="h2"><?php print $heading_one; ?></h6>
    <?php print $body_one; ?>
  </div>
</div>

<div class="row">
  <?php if (!empty($location_images) && !empty($location_images[1])) : ?>
  <div class="col-md-5">
    <?php print drupal_render($location_images[1]); ?>
  </div>
  <?php endif; ?>
  <div class="col-md-7">
    <h6 class="h2"><?php print $heading_two; ?></h6>
    <?php print $body_two; ?>
  </div>
</div>


