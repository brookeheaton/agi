<p>Use of this site is governed by our&nbsp;<a href="terms">Terms and Conditions</a>&nbsp;and our&nbsp;<a href="privacy">Privacy Policy</a>.</p>
<div>
  <div itemscope="" itemtype="http://schema.org/School">
    <span itemprop="name">American Graphics Institute</span>
    <span itemprop="alternateName" style="display:none">AGI Training</span>
    <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
      <span itemprop="streetAddress">150 Presidential Way</span>
      <span itemprop="addressLocality">Woburn</span>,
      <span itemprop="addressRegion">MA</span>
      <span itemprop="postalCode">01801</span>,
      <span itemprop="addressCountry">USA</span>. Telephone:
      <span itemprop="telephone">(781) 376-6044</span>
    </span>
    <br /><br />
    <h5>Reviews of our classes</h5>
    <span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
      <span class="stars">
      <?php
      $a = 0;
      while ($a < $average) {
        print '
            <svg class="feather-icon">
              <use xlink:href="#training-reviews">
            </svg>';
        $a++;
      }
      ?>
      </span>
      <span itemprop="ratingValue"><?php print $average; ?></span> - based on
      <span itemprop="reviewCount"><?php print $reviews_count; ?></span> reviews.
    </span>
  </div>
</div>
