<?php if (!$is_front) : ?>
  <div id="breadcrumbs">
    <?php if (!empty($breadcrumb_items)) : ?>
      <div itemscope itemtype="http://schema.org/BreadcrumbList">
        <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
          <span>
            <a href="/" itemprop="item">
              <span itemprop="name">
                <?php print t('Home'); ?>
              </span>
            </a>
            <meta itemprop="position" content="0"/>
          </span> &rsaquo;
        </span>
        <?php foreach ($breadcrumb_items as $index => $item) : ?>
          <?php if (!empty($item['url'])) : ?>
            <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
              <span>
                <a itemprop="item" href="<?php print $item['url']; ?>">
                  <span itemprop="name">
                    <?php print $item['title']; ?>
                  </span>
                </a>
                <meta itemprop="position" content="<?php print $index + 1; ?>"/>
              </span> &rsaquo;
            </span>

          <?php else : ?>
            <span>
               <span class="name">
                 <?php print $item['title']; ?>
               </span>
             </span>
          <?php endif; ?>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>
  </div>
<?php endif; ?>
