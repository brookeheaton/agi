<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col p-0">
        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>

    <?php if ($tabs): ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>

    <div id="mainContent" class="row">
      <div class="col">

        <div class="row">
          <div class="col-lg-8 offset-lg-4">
            <h1 class="mb-3"><?php print $title; ?></h1>
          </div>
        </div>
        <?php print render($page['content']); ?>
      </div>

      <div id="sidebar-left">
        <div class="request-info mb-5">
          <div class="card">
            <?php if (!empty($request_info_image)) : ?>
              <?php print $request_info_image; ?>
            <?php endif; ?>
            <div class="card-body">
              <?php print $request_info; ?>
            </div>
          </div>
        </div>

        <div class="need-help mb-5">
          <h5>Need help choosing the right <?php print $node->title; ?>? Call us:</h5>
          <p>Boston: 781-376-6044<br>
            Philadelphia: 610-228-0951<br>
            Toll Free: 800-851-9237</p>
        </div>

        <div class="category-description mb-5">
          <div class="my-3 text-center"><?php print $category_logo; ?></div>
          <p>Hands-on <?php print $abbrev_cat_name; ?> classes and <?php print $abbrev_cat_name; ?> training using Mac or Windows computers with a live instructor in the same classroom.</p>
        </div>
        <div class="reviews mb-5">
          <?php print $reviews; ?>
        </div>

        <div class="all-classes-include mb-5">
          <h2 style="font-size: 24px;">All classes include</h2>
          <p style="font-size:16px">&nbsp;<img alt="free retakes of courses" src="/sites/default/files/free-retakes-1.svg" style="width: 24px; height: 24px;" /> Free retakes</p>
          <p style="font-size:16px">&nbsp;<img alt="Curriculum provided" src="/sites/default/files/curriculum-provided-1.svg" style="width: 24px; height: 24px;" /> Detailed curriculum</p>
          <p style="font-size:16px"><img alt="Live instructor" src="/sites/default/files/live-instructor.svg" style="width: 24px; height: 24px;" /> Live instructor</p>
          <p style="font-size:16px">&nbsp;<img alt="flexible rescheduling" src="/sites/default/files/rescheduling-1.svg" style="width: 24px; height: 24px;" /> Flexible rescheduling</p>
        </div>

        <?php if (!empty($node->field_black_header_subtext)) : ?>
          <div class="header-subtext">
            <?php print $node->field_black_header_subtext[LANGUAGE_NONE][0]['value']; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>

    <div class="row mt-5">
      <div class="col-md-12">

        <?php if (isset($related_certification_training)) : ?>
        <div class="related-cert-training">
          <?php print $related_certification_training; ?>
        </div>
        <?php endif; ?>

        <div class="external-links mb-3">
          <?php print $external_links; ?>
        </div>

        <?php
        // FAQs
        if (isset($faqs)) : ?>
          <div class="faq">
            <h2 dir="ltr"> <?php print $abbrev_cat_name; ?> Classes FAQ<a id="indesign-classes-faq" name="indesign-classes-faq"></a></h2>
            <p dir="ltr">Because there are many options for learning <?php print $abbrev_cat_name; ?> you may have questions.We have assembled a list of <?php print $abbrev_cat_name; ?> training FAQ here along with answers:</p>
            <?php print $faqs; ?>
          </div>
        <?php endif; ?>

        <?php
        // Adobe Cert Training Category Page.
        if (isset($node->field_adobe_cert_cat_page[LANGUAGE_NONE][0]['value']) && $node->field_adobe_cert_cat_page[LANGUAGE_NONE][0]['value'] == 1) :
        ?>
        <div class="adobe-cert-assoc">
          <?php
          $field = field_view_field('node', $node, 'field_adobe_certified_associate', ['label' => 'hidden']);
          print render($field);
          ?>
        </div>
        <?php endif; ?>

        <?php if (isset($related_news_to_a_category)) : ?>
          <div class="related-news mb-3">
            <h5>Recent <?php print $short_cat_name; ?> Training News</h5>
            <?php print $related_news_to_a_category; ?>
          </div>
        <?php endif; ?>

        <div class="rel-author-info">
          <p>AGI&#39;s team of experienced instructors are led by <a href="http://www.jennifersmith.com/" rel="author">Jennifer Smith</a> the design and usability expert and best-selling author of more than 20 books including&nbsp;Creative Cloud for Dummies,&nbsp;Creative Suite for Dummies, and&nbsp;Photoshop Digital Classroom. Classes are led by experienced professionals who have extensive professional and training experience, and also work in our consulting practice areas and as practicing professionals. For Federal agencies, American Graphics Institute classes are offered under GSA contract 47QTCA19D003Y.</p>
        </div>
      </div>
    </div>

    <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div> <!-- End of wrapper -->
