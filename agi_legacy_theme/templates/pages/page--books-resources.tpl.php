<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col p-0">

        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>
    <?php print render($page['head']); ?>
    <?php if ($tabs): ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>

    <div id="mainContent" class="row">
      <div class="col">

        <div class="section section-one">
          <div class="row flex-row-reverse flex-md-row">

            <div class="col-md-4">
              <h3>Request Information</h3>
              <?php if (isset($request_info)) : ?>
                <div class="request-info mb-5">
                  <div class="card">
                    <div class="card-body">
                      <?php print $request_info; ?>
                    </div>
                  </div>
                </div>
              <?php endif; ?>
            </div>

            <div class="col-md-8 pl-md-5">
              <div class="view-box">
                <div class="row">
                  <div class="col-md-12">

                    <?php
                      $node_path = explode('/', drupal_get_normal_path(current_path()));
                      $page_id = $node_path[4];
                    ?>
                    <?php
                     print views_embed_view('dcb_resource_pages','panel_pane_1', $page_id);
                    ?>
                  </div>
                  <div class="col-md-12 mt-3">
                    <?php
                     print views_embed_view('dcb_resource_pages','panel_pane_5', $page_id);
                    ?>
                  </div>
                </div>
              </div>
              <div class="links-box mt-3">
                <p class="pane-title larger-text">
                  All Lesson Files
                </p>
                <?php
                 print views_embed_view('dcb_resource_pages','panel_pane_3', $page_id);
                ?>
                <p class="pane-title larger-text mt-3">
                  Individual Lesson Files
                </p>
                <?php
                 print views_embed_view('dcb_resource_pages','panel_pane_2', $page_id);
                ?>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>

    <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div> <!-- End of wrapper -->
