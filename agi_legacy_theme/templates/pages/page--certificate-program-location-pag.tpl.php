<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col p-0">
        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>

    <?php if ($tabs): ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>

    <div id="mainContent" class="row">
      <div class="col">

        <div class="row">
          <div class="col-lg-8 offset-lg-4">
            <h1 class="mb-3"><?php print $title; ?></h1>
          </div>
        </div>

        <?php print render($page['content']); ?>
        <?php if (!empty($node->field_cp_loc_iocation)) : ?>
          <div class="location">
            <?php print render(node_view($node->field_cp_loc_iocation[LANGUAGE_NONE][0]['entity'])); ?>
          </div>
        <?php endif; ?>
      </div>

      <div id="sidebar-left">
        <div class="request-info mb-3">
          <div class="card">
            <?php if (!empty($request_info_image)) : ?>
              <?php print $request_info_image; ?>
            <?php endif; ?>
            <div class="card-body">
              <?php print $request_info; ?>
            </div>
          </div>
        </div>

        <?php if (!empty($node->field_cp_loc_program)) : ?>
          <?php if ($node->field_cp_loc_iocation[LANGUAGE_NONE][0]['target_id'] == '24') : ?>
            <div class="mb-3">
              <p>Need information about the <?php print $node->field_cp_loc_program[LANGUAGE_NONE][0]['entity']->title; ?>? Call us at 212-922-1206 or 800-851-9237.</p>
            </div>
          <?php elseif ($node->field_cp_loc_iocation[LANGUAGE_NONE][0]['target_id'] == '26') : ?>
            <div class="mb-3">
              <p>Need information about the <?php print $node->field_cp_loc_program[LANGUAGE_NONE][0]['entity']->title; ?>? Call us at 610-228-0951 or 800-851-9237.</p>
            </div>
          <?php else : ?>
            <div class="mb-3">
              <p>Need information about the <?php print $node->field_cp_loc_program[LANGUAGE_NONE][0]['entity']->title; ?>? Call us at 781-376-6044 or 800-851-9237.</p>
            </div>
          <?php endif; ?>
        <?php endif; ?>
        <div class="mb-3">
          <a href="/sites/default/files/American-Graphics-Institute-AGI-Certificate-Programs.pdf">Download the Student Information Catalog</a> for Certificate Programs at AGI.
        </div>

        <?php if (isset($random_quote)) : ?>
        <div class="testimonials">
          <?php print $random_quote; ?>
        </div>
        <?php endif; ?>

        <div class="video-testimonials">
          <p class="large-text">Students talk about their AGI Certificate program</p>
          <div class="mb-3"><a class="colorbox-inline"
                               href="/?width=640&amp;height=360&amp;inline=true#certificate-program-testimonial-alyssa"><img
                alt="Certificate Program Testimonial - Alyssa"
                src="/sites/all/themes/agi/assets/images/certificate-program-testimonial-alyssa-play.jpg"
                style="max-width:100%"/></a></div>
          <div class="mb-3"><a class="colorbox-inline"
                               href="/?width=640&amp;height=360&amp;inline=true#certificate-program-testimonial-sean"><img
                alt="Certificate Program Testimonial - Sean"
                src="/sites/all/themes/agi/assets/images/certificate-program-testimonial-sean-play.jpg"
                style="max-width:100%"/></a></div>
          <div class="mb-3"><a class="colorbox-inline"
                               href="/?width=640&amp;height=360&amp;inline=true#certificate-program-testimonial-mike"><img
                alt="Certificate Program Testimonial - Mike"
                src="/sites/all/themes/agi/assets/images/certificate-program-testimonial-mike-play.jpg"
                style="max-width:100%"/></a></div>
          <div style="display:none;">
            <video controls="" height="360" id="certificate-program-testimonial-alyssa"
                   poster="/sites/all/themes/agi/assets/images/certificate-program-testimonial-alyssa.png"
                   preload="none" width="100%">
              <source src="https://d2cqpmwr4b912e.cloudfront.net/certificate-program-testimonial-alyssa.mp4"
                      type="video/mp4"/>
            </video>
          </div>
          <div style="display:none;">
            <video controls="" height="360" id="certificate-program-testimonial-sean"
                   poster="/sites/all/themes/agi/assets/images/certificate-program-testimonial-sean.png" preload="none"
                   width="100%">
              <source src="https://d2cqpmwr4b912e.cloudfront.net/certificate-program-testimonial-sean.mp4"
                      type="video/mp4"/>
            </video>
          </div>
          <div style="display:none;">
            <video controls="" height="360" id="certificate-program-testimonial-mike"
                   poster="/sites/all/themes/agi/assets/images/certificate-program-testimonial-mike.png" preload="none"
                   width="100%">
              <source src="https://d2cqpmwr4b912e.cloudfront.net/certificate-program-testimonial-mike.mp4"
                      type="video/mp4"/>
            </video>
          </div>
        </div>
        <div class="bbb-logo">
          <?php print $bbb_logo; ?>
        </div>
      </div>
    </div>

    <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div> <!-- End of wrapper -->
