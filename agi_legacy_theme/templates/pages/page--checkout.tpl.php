<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col p-0">
        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>

    <?php if ($tabs): ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>

    <div id="mainContent" class="row flex-lg-row-reverse">
      <div class="col-lg-9 min-height">
        <?php print render($page['content']); ?>
      </div>

      <div class="col-lg-3" id="sidebar-left">
        <!-- Begin DigiCert site seal HTML and JavaScript -->
        <div data-language="en" id="DigiCertClickID_snUyqBxZ">
          Registration is Secure
        </div>
        <h2 class="m-0"><img alt="" id="DigiCertClickID_snUyqBxZ" src="/sites/all/themes/agi/assets/images/secure-registration.png" style="width: 149px; height: 71px;" /></h2>
        <h2 class="m-0"><img alt="" src="/sites/default/files/default_images/bbb-acred.png" style="width: 149px; height: 71px;" /></h2>
        <h2 style="font-size:24px; margin-top:16px">Have questions<br />or need help?</h2>
        <p style="font-size:16px">Click the chat option in the corner<br>
          or call 781-376-6044<br>
          or 800-851-9237 to speak<br>
          with a training consultant.</p>

        <div class="all-classes-include mt-5">
          <h2 style="font-size: 24px;">All classes include</h2>
          <p style="font-size:16px">&nbsp;<img alt="free retakes of courses" src="/sites/default/files/free-retakes-1.svg" style="width: 24px; height: 24px;" /> Free retakes</p>
          <p style="font-size:16px">&nbsp;<img alt="Curriculum provided" src="/sites/default/files/curriculum-provided-1.svg" style="width: 24px; height: 24px;" /> Detailed curriculum</p>
          <p style="font-size:16px"><img alt="Live instructor" src="/sites/default/files/live-instructor.svg" style="width: 24px; height: 24px;" /> Live instructor</p>
          <p style="font-size:16px">&nbsp;<img alt="flexible rescheduling" src="/sites/default/files/rescheduling-1.svg" style="width: 24px; height: 24px;" /> Flexible rescheduling</p>
        </div>
      </div>
    </div>

    <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div> <!-- End of wrapper -->
