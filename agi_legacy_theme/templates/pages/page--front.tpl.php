<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col p-0">
        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>

    <div class="hero">
      <div class="hero-image-bg">

      </div>
      <div class="hero-content">
        <h1 class="text-white" style="text-transform: capitalize;">Adobe classes, Photoshop classes, UX classes and InDesign courses.</h1>
      </div>
    </div>

    <div class="section section-one">
      <div class="row flex-row-reverse">
        <div class="col-md-8 pl-md-5">
          <div class="intro mb-3">
            <h3>American Graphics Institute offers <a href="/adobe">Adobe classes</a> including <a href="/adobe/photoshop/classes">Photoshop classes</a>, <a href="/adobe/indesign/classes">InDesign courses</a>, <a href="/adobe/illustrator/classes">Illustrator classes</a>, <a href="/adobe/after-effects/classes">After Effects classes</a>, and <a href="/adobe/premiere-pro/classes">Premiere Pro classes</a>. Web design courses include <a href="/ux/classes">UX Classes</a>, <a href="/html/classes">HTML</a> and <a href="/html-email/classes">HTML Email</a> courses, and <a href="/wordpress/classes">WordPress classes</a>. Live <a href="/adobe/creative-cloud-training">Creative Cloud training classes</a> online and in Boston or Philadelphia.</h3>
          </div>
          <div class="features mb-3">
            <ul class="list-agi">
              <li>Introductory to advanced Adobe classes and web design courses</li>
              <li>In-person classes with an instructor in the same classroom</li>
              <li>Online classes with a live instructor</li>
              <li>Private training and custom Adobe classes available world-wide</li>
              <li>Adobe classes, Apple training, and courses for Google Analytics and Web Design</li>
              <li>Classes led by authors of more than 50 Adobe training and web design books</li>
              <li>Offering Adobe classes in Boston, and Philadelphia</li>
            </ul>
          </div>
          <div class="class-finder">
            <div class="bd-callout bd-callout-secondary">
              <h5>Find a training class</h5>
              <p>Choose from these popular courses.</p>
              <div class="row">
                <div class="col-lg-6">
                  <?php print $class_finder; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <h3>20 years & 50,000 clients</h3>
          <p>For more than 20 years American Graphics Institute has provided training programs to 50,000 clients
            including 90 of the Fortune 100 companies. We've been hired by Adobe, Apple, Google, and Microsoft to
            deliver UX training, Photoshop Classes, InDesign Courses and more to their employees.</p>
          <?php if (isset($request_info)) : ?>
            <div class="request-info">
              <div class="card">
                <div class="card-body">
                  <?php print $request_info; ?>
                </div>
              </div>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>

    <div class="section section-two">
      <div class="row bg-blue-300">
        <div class="col-lg-4">
          <div class="py-3 px-3 px-xl-5">
            <h3 class="text-base"><a href="/ux/classes">UX Classes</a></h3>
            <div class="text-sm">
              <p>Our UX courses and UX training provide skills for designing apps and websites:</p>
              <ul class="list-unstyled">
                <li><a href="/<?php print drupal_get_path_alias('node/196'); ?>">UX Classes in Boston</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/198'); ?>">UX Courses in Philadelphia</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/1704'); ?>">UX Classes Online</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/9826'); ?>">UX Certificate | UX Consulting</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-4 bg-white">
          <div class="py-3 px-3 px-xl-5">
            <h3 class="text-base"><a href="/adobe/photoshop/classes">Photoshop Training Courses</a></h3>
            <div class="text-sm">
              <p>Our Adobe Photoshop training classes help you master the art of digital imaging and retouching:</p>
              <ul class="list-unstyled">
                <li><a href="/<?php print drupal_get_path_alias('node/174'); ?>">Boston Photoshop Training Courses</a>
                </li>
                <li><a href="/<?php print drupal_get_path_alias('node/181'); ?>">Philadelphia Photoshop Courses</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/1744'); ?>">Online Photoshop Classes</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/671'); ?>">Photoshop Certification Classes</a></p>
                </li>
              </ul>
            </div>

          </div>
        </div>
        <div class="col-lg-4">
          <div class="py-3 px-3 px-xl-5">
            <h3 class="text-base"><a href="/adobe/indesign/classes">InDesign Training Classes</a></h3>
            <div class="text-sm">
              <p>Adobe InDesign courses at AGI are led by the authors of best-selling InDesign books:</p>
              <ul class="list-unstyled">
                <li><a href="/<?php print drupal_get_path_alias('node/213'); ?>">Boston InDesign Classes</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/215'); ?>">Philadelphia InDesign Courses</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/1731'); ?>">Online InDesign Classes</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>


      <div class="row bg-blue-300">
        <div class="col-lg-4 bg-white">
          <div class="py-3 px-3 px-xl-5">
            <h3 class="text-base"><a href="/adobe/illustrator/classes">Illustrator Training Classes</a></h3>
            <div class="text-sm">
              <p>Our Adobe Illustrator classes and Illustrator training provide digital illustration skills:</p>
              <ul class="list-unstyled">
                <li><a href="/<?php print drupal_get_path_alias('node/210'); ?>">Boston Illustrator Classes</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/212'); ?>">Philadelphia Illustrator Courses</a>
                </li>
                <li><a href="/<?php print drupal_get_path_alias('node/1734'); ?>">Illustrator Training Online</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="py-3 px-3 px-xl-5">
            <h3 class="text-base"><a href="/adobe/after-effects/classes">After Effects Training Courses</a></h3>
            <div class="text-sm">
              <p>Adobe After Effects classes at AGI are led by the authors of several After Effects books:</p>
              <ul class="list-unstyled">
                <li><a href="/<?php print drupal_get_path_alias('node/225'); ?>">Boston After Effects Classes</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/227'); ?>">Philadelphia After Effects Courses</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/1712'); ?>">Online After Effects Courses</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-4 bg-white">
          <div class="py-3 px-3 px-xl-5">
            <h3 class="text-base"><a href="/adobe/premiere-pro/classes">Premiere Pro Training Courses</a></h3>
            <div class="text-sm">
              <p>These Adobe Premiere Pro training classes teach this popular digital video editing app:</p>
              <ul class="list-unstyled">
                <li><a href="/<?php print drupal_get_path_alias('node/228'); ?>">Boston Premiere Pro Classes</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/230'); ?>">Philadelphia Premiere Pro Classes</a>
                </li>
                <li><a href="/<?php print drupal_get_path_alias('node/1725'); ?>">Online Premiere Pro Classes</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>


      <div class="row bg-blue-300">
        <div class="col-lg-4">
          <div class="py-3 px-3 px-xl-5">
            <h3 class="text-base"><a href="/html/classes">HTML Training Courses</a></h3>
            <div class="text-sm">
              <p>These CSS and HTML classes teach skills for designing and developing online content:</p>
              <ul class="list-unstyled">
                <li><a href="/<?php print drupal_get_path_alias('node/190'); ?>">Boston HTML Courses</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/192'); ?>">Philadelphia HTML Courses</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/1736'); ?>">Online HTML Courses</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/15676'); ?>">CSS Training Courses</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/143'); ?>">HTML5 Training Courses</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/74'); ?>">HTML Email Classes</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-4 bg-white">
          <div class="py-3 px-3 px-xl-5">
            <h3 class="text-base"><a href="/adobe">Adobe Classes</a></h3>
            <div class="text-sm">
              <p>Adobe classes and Adobe Training in Boston, Philadelphia, online and on-site:</p>
              <ul class="list-unstyled">
                <li><a href="/<?php print drupal_get_path_alias('node/6'); ?>">Adobe Classes and training</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/6'); ?>">Online Adobe classes</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/25936'); ?>">Online Creative Cloud classes</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/1558'); ?>">Captivate Training Courses</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/58'); ?>">Connect Training Classes</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/63'); ?>">Adobe Acrobat Training Courses</a></li>
                <li><a href="/<?php print drupal_get_path_alias('node/66'); ?>">LiveCycle Designer Training Classes</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="section section-three">
      <div class="row">
        <div class="col-md-12 related-news">
          <?php print $recent_news; ?>
        </div>
      </div>
    </div>

    <div class="section section-four">
      <div class="row">
        <div class="col-md-12">
          <p><strong>Creative Cloud Classes and Adobe Training</strong><br/>Regularly scheduled Adobe <a
              href="/adobe/creative-cloud-classes-boston">Creative Cloud courses in Boston</a>, as well as <a
              href="/adobe/creative-cloud-classes-philadelphia">Creative Cloud training in Philadelphia</a> and <a
              href="/adobe/creative-cloud-training/creative-cloud-classes-online">Creative Cloud training online</a>.
            AGI offers Adobe training courses for all Creative Cloud tools, including Photoshop classes and InDesign
            courses as well as <a href="/adobe/adobe-certification-training">Adobe Certification training</a>&nbsp;and
            <a href="/adobe/xd/classes">Adobe XD training</a>. American Graphics Institute also offers <a
              href="/adobe/creative-cloud-training/creative-cloud-certification-test-online">online Creative Cloud
              certification exams</a> including an <a
              href="/adobe/photoshop/classes/photoshop-certification-test-online">online Photoshop certification
              test</a>, a <a href="/adobe/indesign/classes/indesign-certification-test-online">InDesign certification
              exam online</a>, and a <a href="/adobe/illustrator/classes/illustrator-certification-test-online">Illustrator
              Certification test online</a>. Our <a
              href="/adobe/creative-cloud-training/creative-cloud-skill-assessment">Creative Cloud skills assessment</a>
            can help you identify the best classes for you, or contact us to speak with a training consultant.</p>
          <p><strong>UX Training and Web Design Courses</strong><br/>American Graphics Institute provides UX workshops,&nbsp;<a
              href="/ux/classes/ux-certificate/information-architecture-training-class">Information Architecture (IA)
              training</a>, as well <a href="/ux/classes/sketch-training">Sketch training</a>. Other courses include <a
              href="/web-design-classes">web design classes</a> and also <a href="/web-development/classes">web
              development courses</a>, which range from HTML training, CSS workshops, and <a
              href="/drupal/classes/boston">Drupal classes in Boston</a>. For understanding web analytics, our <a
              href="/google/analytics/training/boston-google-analytics-training-classes">Google Analytics training
              classes in Boston</a>&nbsp;are offered monthly as are the <a href="/google/analytics/training/philadelphia">Google Analytics
              courses in Philadelphia</a>.&nbsp; Regularly scheduled <a href="/wordpress/classes">WordPress training
              classes</a>, including <a href="/wordpress/classes/boston">WordPress courses in Boston</a>are available once per month. Our web design
            focused&nbsp;<a href="/adobe/dreamweaver/classes/boston">Dreamweaver training courses</a>&nbsp;are also held
            monthly, as are <a href="/web-accessibility-training-classes">Web Accessibility training</a> workshops and
            <a href="/email-marketing-classes">email marketing courses</a></p>
          <p><strong>Powerpoint Training and Keynote Classes</strong><br/>These include <a
              href="/apple/keynote/classes">Apple Keynote training classes</a>, for designing presentations using
            Keynote, which are available at your location, or at our classrooms including <a href="/apple/keynote/classes/boston">Keynote
              Courses in Boston</a>. Or enroll in the <a href="/powerpoint-design-classes">PowerPoint design courses</a>
            and learn to create great looking presentations.</p>
          <p><strong>Video Editing Training</strong><br/>If you want to <a href="/video-editing/training">learn video
              editing</a> tools, regularly scheduled <a href="/apple/final-cut/classes">Final Cut Pro training
              classes</a> are available including <a href="/apple/final-cut/classes/boston">Boston Final Cut Pro
              Courses</a>&nbsp;and&nbsp;<a
              href="/apple/final-cut/classes/philadelphia">Philadelphia Final Cut Pro classes</a>. Other video editing
            classes include Premiere Pro courses and After Effects classes available in Boston, Philadelphia, and
            as live online classes.</p>
          <p><strong>E-learning Training</strong><br/>AGI also provides <a href="/elearning-classes">e-learning
              courses</a>, including <a href="/adobe/captivate/classes">Captivate training classes</a> and regularly
            scheduled <a href="/camtasia">Camtasia training</a>. If you are looking for a specific course you
            can&nbsp;<a href="/classes/training.html">see all training classes</a>&nbsp;for a list of all courses
            available at AGI.</p>
          <p><strong>3D Modeling</strong><br/>Learn 3D design with <a href="/sketchup-training-classes">Sketchup
              classes</a>, and <a href="/3ds-max/training">3DS training</a> as well as After Effects courses.</p>
          <p><strong>Learn from the Experts</strong><br/>We&#39;ve been hired by&nbsp;<a href="/about/clients">thousands
              of clients</a>&nbsp;including Adobe, Apple, Google, and Microsoft.&nbsp;You&#39;ll find reviews for all
            our classes listed on each individual course page, you can see <a href="/reviews">reviews from all AGI
              Training classes</a>, or contact us for additional client references from the thousands of participants of
            our training courses who have volunteered to serve as a reference. AGI offers many regularly scheduled <a
              href="/graphic-design-classes">graphic design classes</a>&nbsp;including <a
              href="/graphic-design-classes/boston-graphic-design-classes">graphic design courses in Boston</a>, and
            continues to offer <a href="/quarkxpress/training">QuarkXPress training classes</a>.</p>
          <p>Beyond our classrooms in <a href="/locations/boston">Boston</a> and <a href="/locations/philadelphia">Philadelphia</a>, we also offer <a
              href="/adobe/adobe/classes/all-locations">Adobe training courses in cities across the U.S.</a> and on-site
            at client locations globally.</p>
        </div>
      </div>
    </div>

    <div class="section section-five">
      <div class="row">
        <div class="col-md-12">
          <?php print $footer_copy_and_reviews; ?>
        </div>
      </div>
    </div>

    <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div>

