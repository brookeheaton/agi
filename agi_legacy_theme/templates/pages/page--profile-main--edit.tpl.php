<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col-sm-12">
        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>

    <div class="row justify-content-center">
      <div class="col-lg-10">

        <?php print theme('agi_user_profile_header', ['account' => $user]); ?>

        <div class="tabs mb-5">
          <h2 class="element-invisible">Secondary tabs</h2>
          <ul class="nav nav-pills">
            <li class="nav-item">
              <a class="nav-link active" href="<?php print '/profile/' . $user->uid; ?>" class="active">Membership</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/profile/<?php print $user->uid; ?>/upcoming">Upcoming</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/profile/<?php print $user->uid; ?>/completed">Completed</a>
            </li>
          </ul>
        </div>

        <div id="mainContent" class="row">
          <div class="col">
            <?php print render($page['content']); ?>
          </div>
        </div>

        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>
      </div>
    </div>


    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div> <!-- End of wrapper -->

