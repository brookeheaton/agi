<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col p-0">
        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>

    <?php if ($tabs): ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>

    <div id="mainContent" class="row">
      <div class="col">

        <div class="row">
          <div class="col-lg-8 offset-lg-4">
            <h1 class="mb-3"><?php print $title; ?></h1>
          </div>
        </div>

        <?php print render($page['content']); ?>
      </div>

      <div id="sidebar-left">
        <div class="request-info mb-3">
          <div class="card">
            <?php if (!empty($request_info_image)) : ?>
              <?php print $request_info_image; ?>
            <?php endif; ?>
            <div class="card-body">
              <?php print $request_info; ?>
            </div>
          </div>
        </div>
        <div class="reviews mb-5">
          <?php print $reviews; ?>
        </div>

        <div class="all-classes-include mb-5">
          <h2 style="font-size: 24px;">All classes include</h2>
          <p style="font-size:16px">&nbsp;<img alt="free retakes of courses" src="/sites/default/files/free-retakes-1.svg" style="width: 24px; height: 24px;" /> Free retakes</p>
          <p style="font-size:16px">&nbsp;<img alt="Curriculum provided" src="/sites/default/files/curriculum-provided-1.svg" style="width: 24px; height: 24px;" /> Detailed curriculum</p>
          <p style="font-size:16px"><img alt="Live instructor" src="/sites/default/files/live-instructor.svg" style="width: 24px; height: 24px;" /> Live instructor</p>
          <p style="font-size:16px">&nbsp;<img alt="flexible rescheduling" src="/sites/default/files/rescheduling-1.svg" style="width: 24px; height: 24px;" /> Flexible rescheduling</p>
        </div>

        <div class="bbb-logo">
          <?php print $bbb_logo; ?>
        </div>
      </div>
    </div>

    <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div> <!-- End of wrapper -->
