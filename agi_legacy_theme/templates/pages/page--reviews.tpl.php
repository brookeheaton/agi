<?php
$node = NULL;
$category_short_name = "";
if (arg(1)) {
  $node = node_load(arg(1));
  $category_short_name = $node->field_short_category_name[LANGUAGE_NONE][0]['value'];
}
$view_id = arg(1) == NULL ? "panel_pane_2" : "ctools_context_1";
$views = views_embed_view("reviews", $view_id, [
  0 => $node->nid
])
?>
<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col p-0">
        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>

    <?php if ($tabs): ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>

    <div id="mainContent" class="row">
      <div class="col">
        <div class="row">
          <div class="col-lg-8 offset-lg-4 min-height">
            <?php if (arg(1) !== NULL) : ?>
            <p>All participants in our <?php print $node->title; ?> have an opportunity to provide feedback as a part of their course. Below you&rsquo;ll find reviews from clients specific to our&nbsp;<?php print $category_short_name; ?>. We collect more detailed evaluations covering the instructor, materials, and classroom environment which are also available for you to review before enrolling.</p><h2>References for&nbsp;<?php print $node->title; ?></h2><p>We have more than 1,000 clients willing to serve as references for our <?php print $node->title; ?>. If you want to talk with a previous training client who has taken <?php print $node->title; ?> at American Graphics Institute, contact us and we&rsquo;ll be happy to provide you with additional references.</p>
            <?php endif; ?>
            <?php print $views; ?>
          </div>
        </div>
      </div>
      <div id="sidebar-left">

        <div class="request-info mb-3">
          <div class="card">
            <?php if (!empty($request_info_image)) : ?>
              <?php print $request_info_image; ?>
            <?php endif; ?>
            <div class="card-body">
              <?php print $request_info; ?>
            </div>
          </div>
        </div>
        <div class="my-3">
          <p class="larger-text">Questions about <?php print $node->title; ?>?</p><p>Call the American Graphics Institute location nearest you:</p><p>Boston: 781-376-6044</p><p>Philadelphia: 610-228-0951</p><p>Toll free: 800-851-9237</p>
        </div>
      </div>
    </div>

    <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div> <!-- End of wrapper -->
