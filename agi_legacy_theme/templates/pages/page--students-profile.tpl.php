<div id="wrapper">


  <?php include(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>

  <!-- End of header.php file -->


  <!-- Start of page specific content -->

  <div class="container mar-top-20 cf">
    <div>


      <?php print render($page['content']); ?>

    </div>


    <?php require_once (drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc');?>




  </div> <!-- End of wrapper -->

  <!-- Secondary nav pushed down in DOM -->
  <div id="secondaryNav" class="desktop-only">
    <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
  </div>

