<div id="wrapper">

  <?php require_once drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'; ?>

  <!-- End of header.php file -->
  <!-- Start of left-sidebar.php file -->

  <div id="leftSidebar">

    <?php print render($page['left_sidebar']); ?>


    <div class="clearfix">

    </div>
  </div>


  <!-- End of left-sidebar.php file -->


  <!-- Start of page specific content -->

  <div id="middle">
    <div id="mainContent">


      <?php print render($page['content']); ?>
    </div>
  </div>


  <!-- Start of right-sidebar.php file -->

  <div id="rightSidebar">

    <?php print render($page['right_sidebar']); ?>

  </div>

  <!-- End of right-sidebar.php file -->


  <?php require_once (drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc');?>

  <!-- Secondary nav pushed down in DOM -->
  <div id="secondaryNav" class="desktop-only">
    <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
  </div>


</div> <!-- End of wrapper -->


