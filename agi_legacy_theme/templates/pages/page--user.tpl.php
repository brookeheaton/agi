<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col p-0">
        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>

    <div id="mainContent" class="row justify-content-center">
      <?php if (arg(2) == "orders" || arg(2) == "imce") : ?>
        <div class="col-sm-12 mt-5">
          <?php print render($page['content']); ?>
        </div>
      <?php else : ?>
        <div class="col-lg-5 mt-5">
          <h4>Log-in to keep track of past and upcoming classes.</h4>
          <?php print render($page['content']); ?>
        </div>
      <?php endif; ?>


    </div>

    <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div> <!-- End of wrapper -->

