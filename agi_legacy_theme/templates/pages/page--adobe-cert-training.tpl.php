<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col p-0">
        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>

    <?php if ($tabs): ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>

    <div id="mainContent" class="row">
      <div class="col">

        <div class="row">
          <div class="col-lg-8 offset-lg-4">
            <h1 class="mb-3"><?php print $title; ?></h1>
          </div>
        </div>
        <?php print render($page['content']); ?>
      </div>

      <div id="sidebar-left">
        <div class="request-info mb-5">
          <div class="card">
            <?php if (!empty($request_info_image)) : ?>
              <?php print $request_info_image; ?>
            <?php endif; ?>
            <div class="card-body">
              <?php print $request_info; ?>
            </div>
          </div>
        </div>

        <div class="need-help mb-5">
          <h5>Need help choosing the right <?php print $node->title; ?>? Call us:</h5>
          <p>Boston: 781-376-6044<br>
            Philadelphia: 610-228-0951<br>
            Toll Free: 800-851-9237</p>
        </div>

        <div class="category-description mb-5">
          <div class="my-3 text-center"><?php print $category_logo; ?></div>
          <p>Hands-on <?php print $abbrev_cat_name; ?> classes and <?php print $abbrev_cat_name; ?> training using Mac or Windows computers with a live instructor in the same classroom.</p>
        </div>
        <div class="reviews">
          <?php print $reviews; ?>
        </div>

        <?php if (!empty($node->field_black_header_subtext)) : ?>
          <div class="header-subtext">
            <?php print $node->field_black_header_subtext[LANGUAGE_NONE][0]['value']; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>

    <div class="row mt-5">
      <div class="col-md-12">
        <h5 class="mb-3"><?php print $title; ?></h5>
        <div class="related-cert-training">
          <?php print $related_certification_training; ?>
        </div>
        <div class="external-links mb-5">
          <?php print $external_links; ?>
        </div>

        <div class="adobe-cert-assoc">
          <?php
          $field = field_view_field('node', $node, 'field_adobe_certified_associate', ['label' => 'hidden']);
          print render($field);
          ?>
        </div>

        <div class="related-news mb-3">
          <h5>Recent <?php print $short_cat_name; ?> Training News</h5>
          <?php print $related_news_to_a_category; ?>
        </div>

        <div class="rel-author-info">
          <p>AGI&#39;s team of experienced instructors are led by <a href="http://www.jennifersmith.com/" rel="author">Jennifer Smith</a> the design and usability expert and best-selling author of more than 20 books including&nbsp;Creative Cloud for Dummies,&nbsp;Creative Suite for Dummies, and&nbsp;Photoshop Digital Classroom. Classes are led by experienced professionals who have extensive professional and training experience, and also work in our consulting practice areas and as practicing professionals. For Federal agencies, American Graphics Institute classes are offered under GSA contract 47QTCA19D003Y.</p>
        </div>
      </div>
    </div>

    <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div> <!-- End of wrapper -->
