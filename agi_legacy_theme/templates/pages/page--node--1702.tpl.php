<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col p-0">
        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>

    <?php if ($tabs): ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>

    <div id="mainContent" class="row">
      <div class="col">

        <div class="row">
          <div class="col-lg-8 offset-lg-4">
            <h1 class="mb-3"><?php print $title; ?></h1>
          </div>
        </div>
        <?php print render($page['content']); ?>
      </div>

      <div id="sidebar-left">
        <div class="request-info mb-5">
          <div class="card">
            <?php if (!empty($request_info_image)) : ?>
              <?php print $request_info_image; ?>
            <?php endif; ?>
            <div class="card-body">
              <?php print $request_info; ?>
            </div>
          </div>
        </div>
        <div class="videos">
          <p><a class="colorbox-inline"
                href="/?width=640&amp;height=360&amp;inline=true#agi-training-online-courses"><img height="auto"
                                                                                                   src="/sites/all/themes/agi/assets/images/live-online-classes.png"
                                                                                                   width="100%"/></a>See
            how live online training works in this brief video.</p>
          <div style="display:none;">
            <video controls="" height="360" id="agi-training-online-courses"
                   poster="/sites/all/themes/agi/assets/images/agi-video-cover.png" preload="none" width="100%">
              <source src="http://d2cqpmwr4b912e.cloudfront.net/agi-training-online-courses.mp4"
                      type="video/mp4"></source>
            </video>
          </div>
        </div>

        <div class="links">
          <ul>
            <li><a href="#see-courses">Available classes</a></li>
            <li><a href="#how-it-works">How it works</a></li>
            <li><a href="#how-it-works">What you need</a></li>
            <li><a href="#how-it-works">Class times</a></li>
          </ul>
        </div>

        <div class="help">
          <p style="margin-top:40px;">Need help choosing the right course? Call us at<br/>781-376-6044 or 800-851-9237.
          </p>
        </div>

        <div class="bbb">
          <p><img alt="American Graphics Institute BBB Business Review"
                  src="/sites/all/themes/agi/assets/images/bbblogo.png" style="border: 0;"/></p>
        </div>

        <div class="clients">
          <p><strong>AGI Training Clients&nbsp;</strong></p>
          <p>Since 1995 we&rsquo;ve delivered training to more than 5,000 companies and 50,000 individuals including
            these firms:</p>
          Adobe Systems<br/>
          Apple<br/>
          Google<br/>
          Grey Worldwide<br/>
          Harvard Business Press<br/>
          McCann Erickson<br/>
          Microsoft<br/>
          MIT Press<br/>
          Rodale</p>
        </div>

        <div class="discounted">
          <p><strong>Discounted Training for Groups or Multiple Courses</strong></p>
          <p>If you or your organization is considering multiple classes, consider our training passes or training
            vouchers.</p>
        </div>
      </div>
    </div>

    <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div> <!-- End of wrapper -->

