<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col p-0">
        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>

    <?php if ($tabs): ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>

    <div id="mainContent" class="row">
      <div class="col">
        <div class="row">
          <div class="col-lg-8 offset-lg-4">
            <h1 class="mb-3"><?php print $title; ?></h1>
          </div>
        </div>

        <?php print render($page['content']); ?>
      </div>

      <div id="sidebar-left">
        <div class="request-info mb-5">
          <div class="card">
            <?php if (!empty($request_info_image)) : ?>
              <?php print $request_info_image; ?>
            <?php endif; ?>
            <div class="card-body">
              <?php print $request_info; ?>
            </div>
          </div>
        </div>

        <div class="links">
          <ul>
            <li><a href="/classes/locations.html">Locations</a></li>
            <li><a href="#class-times">Class times</a></li>
            <li><a href="#see-courses">Available classes</a></li>
            <li><a href="#class-times">Class Information</a></li>
          </ul>
        </div>

        <div class="help">
          <p style="margin-top:40px;">Need help choosing the right course? Call us at<br/>781-376-6044 or 800-851-9237.
          </p>
        </div>

        <div class="bbb">
          <p><img alt="American Graphics Institute BBB Business Review" src="/sites/all/themes/agi/assets/images/bbblogo.png" style="border: 0;"/></p>
        </div>
      </div>
    </div>

    <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div> <!-- End of wrapper -->

