<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col p-0">
        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>

    <?php if ($tabs): ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>

    <div id="mainContent" class="row flex-column-reverse flex-lg-row">
      <?php if (isset($request_info)) : ?>
        <div class="col-lg-4">
          <div class="request-info mb-5">
            <div class="card">
              <?php if (!empty($request_info_image)) : ?>
                <?php print $request_info_image; ?>
              <?php endif; ?>
              <div class="card-body">
                <?php print $request_info; ?>
              </div>
            </div>
          </div>
          <div class="bbb">
            <p><img alt="American Graphics Institute BBB Business Review"
                    src="/sites/all/themes/agi/assets/images/bbblogo.png" style="border: 0;"/></p>
          </div>
        </div>
      <?php endif; ?>

      <div class="col-lg-<?php print (isset($request_info) ? '8' : '12'); ?>">
        <?php if (!preg_match("/692/", $node->nid)) : ?>
          <h1 class="mb-3"><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($page['content']); ?>
      </div>
    </div>

    <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div> <!-- End of wrapper -->



