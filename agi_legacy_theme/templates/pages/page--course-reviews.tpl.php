<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col p-0">
        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>

    <?php print render($page['head']); ?>

    <?php if ($tabs): ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>


    <div id="mainContent" class="row">
      <div class="col">

        <div class="section section-one">
          <div class="row flex-column-reverse flex-md-row">

            <div class="col-md-4">
              <h3>Request Information</h3>
              <?php if (isset($request_info)) : ?>
                <div class="request-info mb-5">
                  <div class="card">
                    <div class="card-body">
                      <?php print $request_info; ?>
                    </div>
                  </div>
                </div>
              <?php endif; ?>

              <div class="description-box my-3">
                <h3>We never sell or share your information</h3>
                <p>Questions about After Effects Classes?</p>
                <p>Call the American Graphics Institute location nearest you:</p>
                <p>Boston: 781-376-6044</p>
                <p>Philadelphia: 610-228-0951</p>
                <p>Toll free: 800-851-9237</p>
              </div>

            </div>

            <div class="col-md-8 pl-md-5">

              <h1 class="mb-3"><?php print $title; ?></h1>

              <?php $new_title = explode( " ", $title );
              array_splice( $new_title, -1 );
              $new_title = implode( " ", $new_title );
              ?>

              <h2>Reviews of <?php print $new_title; ?></h2>
              <p>All participants in our <?php print $new_title; ?> have an opportunity to provide feedback as a part of their course. Below you’ll find reviews from clients specific to this <?php print $new_title; ?>. We collect more detailed evaluations covering the instructor, materials, and classroom environment which are also available for you to review before enrolling.</p>

              <h2>References for <?php print $new_title; ?></h2>
              <p>We have more than 1,000 clients willing to serve as references for our <?php print $new_title; ?>. If you want to talk with a previous training client who has taken <?php print $new_title; ?> at American Graphics Institute, contact us and we’ll be happy to provide you with additional references.</p>

              <div class="view-box">
                <?php
                $node_path = explode('/', drupal_get_normal_path(current_path()));
                $page_id = $node_path[1];
                print views_embed_view('reviews','ctools_context_2', $page_id);
                ?>
              </div>
            </div>

          </div>
        </div>
        <?php print render($page['content']); ?>
      </div>
    </div>

    <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div> <!-- End of wrapper -->
