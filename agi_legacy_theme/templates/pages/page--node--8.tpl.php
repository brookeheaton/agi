<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col p-0">
        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>

    <?php if ($tabs): ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>

    <div id="mainContent" class="row">
      <div class="col">

        <div class="row">
          <div class="col-lg-8 offset-lg-4">
            <h1 class="mb-3"><?php print $title; ?></h1>
          </div>
        </div>
        <?php print render($page['content']); ?>
      </div>

      <div id="sidebar-left">
        <div class="request-info mb-5">
          <div class="card">
            <?php if (!empty($request_info_image)) : ?>
              <?php print $request_info_image; ?>
            <?php endif; ?>
            <div class="card-body">
              <?php print $request_info; ?>
            </div>
          </div>
        </div>
        <div class="our-clients">
          <p><strong>Our Clients</strong></p>
          <p>For more than 15 years AGI has delivered training and consulting services to thousands of clients
            including:</p>
          <p>Adobe Systems<br/>
            Google<br/>
            Microsoft<br/>
            T.Rowe Price<br/>
            TBWA \ Worldwide<br/>
            Internal Revenue Service<br/>
            NY State Tax Authority<br/>
            National Archives<br/>
            Central Intelligence Agency<br/>
            U.S. Army Central Command<br/>
            Verizon<br/>
            Fortune Magazine<br/>
            New York Times<br/>
            HBO<br/>
            NBC<br/>
            Lockheed Martin<br/>
            Boeing<br/>
            Raytheon<br/>
            Lego<br/>
            HP / Compaq<br/>
            Gillette<br/>
            L.L. Bean<br/>
            New Balance<br/>
            Reebok<br/>
            Macy&rsquo;s<br/>
            Citizens Bank<br/>
            National Life<br/>
            Metropolitan Life<br/>
            State Farm<br/>
            Citigroup<br/>
            John Hancock<br/>
            Prudential<br/>
            Fidelity<br/>
            Johnson &amp; Johnson<br/>
            Pfizer<br/>
            Kaiser Permanente<br/>
            Glaxo Smith Kline<br/>
            Blue Cross / Blue Shield</p>
        </div>
      </div>
    </div>

    <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div> <!-- End of wrapper -->

