<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col p-0">
        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>

    <?php if ($tabs): ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>

    <div id="mainContent" class="row">
      <div class="col">

        <div class="row">
          <div class="col-lg-8 offset-lg-4">
            <h1 class="mb-3"><?php print $title; ?></h1>
          </div>
        </div>

        <?php print render($page['content']); ?>
      </div>

      <div id="sidebar-left">
        <div class="request-info mb-3">
          <div class="card">
            <?php if (!empty($request_info_image)) : ?>
              <?php print $request_info_image; ?>
            <?php endif; ?>
            <div class="card-body">
              <?php print $request_info; ?>
            </div>
          </div>
        </div>

        <div class="mb-3">
          <p>Need information about the <?php print $abbrev_cat_name; ?> Certificate? Call us at 781-376-6044 or 800-851-9237.</p>
        </div>
        <div class="mb-3">
          <a href="/sites/default/files/American-Graphics-Institute-AGI-Certificate-Programs.pdf">Download the Student Information Catalog</a> for Certificate Programs at AGI.
        </div>
        <div class="testimonials">
          <?php print $sidebar_video_testimonials; ?>
        </div>

        <div class="mb-3" itemscope="" itemtype="http://schema.org/School">
          <div itemprop="name">American Graphics Institute</div>
          <div itemprop="alternateName" style="display:none">AGI Training</div>
          <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
            <span itemprop="streetAddress">150 Presidential Way</span>
            <span itemprop="addressLocality">Woburn</span>, <span itemprop="addressRegion">MA</span><br>
            <span itemprop="postalCode">01801</span><br>
            <span itemprop="addressCountry">USA</span><br>
            <span itemprop="telephone">(781) 376-6044</span>
          </div>
        </div>


        <div class="bbb-logo">
          <?php print $bbb_logo; ?>
        </div>
      </div>
    </div>

    <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div> <!-- End of wrapper -->
