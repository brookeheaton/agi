<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col p-0">
        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>

    <?php if ($tabs): ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>

    <div id="mainContent" class="row flex-lg-row-reverse">


      <div class="col-lg-10">
        <h1 class="mb-3"><?php print $title; ?></h1>
        <?php print render($page['content']); ?>
      </div>

      <div class="col-lg-2">
        <?php print render($page['left_sidebar']); ?>
      </div>
    </div>

    <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div> <!-- End of wrapper -->

