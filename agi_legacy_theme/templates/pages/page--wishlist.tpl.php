<div id="wrapper">


  <?php include(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>

  <!-- End of header.php file -->


  <!-- Start of page specific content -->

  <div class="container mar-top-20 cf">
    <div>


      <?php if ($logged_in == FALSE) {
        print '<h1>Unauthorized to view this page.</h1>';
      }

      if ($logged_in):

        if (empty(profile2_load_by_user($user))) {
          print '<p><em><a href="/profile-main/' . $user->uid . '/edit">Please update your profile to include your name</a></em></p>';
        }
        else {

          $profile = profile2_load_by_user($user);

          $profile_first_name = $profile['main']->field_first_name['und'][0]['safe_value'];
          $profile_last_name = $profile['main']->field_last_name['und'][0]['safe_value'];
          $user_mail = user_is_logged_in() ? '<br><span class="user-mail">' . $user->mail . '</span>' : '';

          print '<h1>' . $profile_first_name . ' ' . $profile_last_name . $user_mail . '</h1>';
        }
        ?>

        <nav class="m-b-1">
          <a href="/profile-main" class="btn btn-secondary">Information</a>
          <a href="/view-course-history" class="btn btn-secondary">History</a>
          <a href="/view-online-registrations" class="btn btn-secondary">Upcoming</a>
          <a href="/wishlist" class="btn btn-primary">Wishlist</a>
          <a href="/profile-main/<?php print $user->uid; ?>/edit"
             class="btn btn-secondary">Profile</a>
          <a href="/user/<?php print $user->uid; ?>/edit"
             class="btn btn-secondary">Account</a>
          <a href="/user/logout" class="btn btn-secondary">Log out</a>
        </nav>
        <?php print render($page['content']); ?>

      <?php endif; ?>

    </div>


    <?php require_once (drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc');?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav" class="desktop-only">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>


  </div> <!-- End of wrapper -->



