<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col p-0">
        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>

    <?php if ($tabs): ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>

    <div id="mainContent" class="row">
      <div class="col">

        <div class="row">
          <div class="col-lg-8 offset-lg-4">
            <h1 class="mb-3"><?php print $title; ?></h1>
          </div>
        </div>
        
        <?php print render($page['content']); ?>
      </div>

      <div id="sidebar-left">
        <?php if (isset($variables['node']) && !empty($variables['node']->field_full_address)) : ?>
          <div class="address">
            <?php print render($variables['node']->field_full_address[LANGUAGE_NONE][0]['value']); ?>
          </div>
        <?php endif; ?>

        <?php if (isset($location_images)) : ?>
        <div class="images">
          <?php
          $image_chunk = array_chunk($location_images, 2);
          ?>
          <?php foreach ($image_chunk as $row) : ?>
            <div class="row">
              <?php foreach ($row as $image) : ?>
                <div class="col-md-6 p-1">
                  <?php print $image; ?>
                </div>
              <?php endforeach; ?>
            </div>
          <?php endforeach; ?>
        </div>
        <?php endif; ?>
      </div>
    </div>

    <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div> <!-- End of wrapper -->
