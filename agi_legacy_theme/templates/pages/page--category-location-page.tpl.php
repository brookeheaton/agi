<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col p-0">
        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>

    <?php if ($tabs): ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>

    <div id="mainContent" class="row">
      <div class="col">

        <?php print render($page['content']); ?>
      </div>
      <div id="sidebar-left">

        <?php if ($city_name == "Online" && isset($video) ) : ?>
          <div class="video">
            <?php print $video; ?>
          </div>
        <?php endif; ?>

        <div class="request-info mb-3">
          <div class="card">
            <?php if (!empty($request_info_image)) : ?>
              <?php print $request_info_image; ?>
            <?php endif; ?>
            <div class="card-body">
              <?php print $request_info; ?>
            </div>
          </div>
        </div>

        <div class="address">
          <?php print $address; ?>
        </div>
        <div class="reviews mb-5">
          <?php print $reviews; ?>
        </div>

        <div class="all-classes-include mb-5">
          <h2 style="font-size: 24px;">All classes include</h2>
          <p style="font-size:16px">&nbsp;<img alt="free retakes of courses" src="/sites/default/files/free-retakes-1.svg" style="width: 24px; height: 24px;" /> Free retakes</p>
          <p style="font-size:16px">&nbsp;<img alt="Curriculum provided" src="/sites/default/files/curriculum-provided-1.svg" style="width: 24px; height: 24px;" /> Detailed curriculum</p>
          <p style="font-size:16px"><img alt="Live instructor" src="/sites/default/files/live-instructor.svg" style="width: 24px; height: 24px;" /> Live instructor</p>
          <p style="font-size:16px">&nbsp;<img alt="flexible rescheduling" src="/sites/default/files/rescheduling-1.svg" style="width: 24px; height: 24px;" /> Flexible rescheduling</p>
        </div>

        <?php if (!empty($node->field_cat_for_location) && !empty($node->field_cat_for_location[LANGUAGE_NONE][0]['entity']->field_black_header_subtext)) : ?>
          <div class="book mt-3">
            <?php print $node->field_cat_for_location[LANGUAGE_NONE][0]['entity']->field_black_header_subtext['und'][0]['safe_value'];; ?>
          </div>
        <?php endif; ?>

        <?php if ($city_name != "Online" && isset($video)) : ?>
          <div class="video">
            <?php print $video; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>

    <div class="row mt-3">
      <div class="col-md-12">
        <?php if ($additional_information) : ?>
        <div class="additional-info mb-5">
          <?php if ($city_name == "Online") : ?>
            <h3 class="mb-4 h2"><?php print $abbrev_cat_name; ?> Classes Online</h3>
          <?php elseif ($city_name == "private") : ?>
            <h3 class="mb-4 h2">Private <?php print $abbrev_cat_name; ?> Classes</h3>
          <?php else : ?>
            <h3 class="mb-4 h2"><?php print $abbrev_cat_name; ?> Classes in <?php print $city_name; ?></h3>
          <?php endif; ?>
          <?php print $additional_information; ?>
        </div>
        <?php endif; ?>

        <?php if (!empty($node->field_course_location_info)) : ?>
          <div class="course-location-info">
            <?php
            $field = field_view_field('node', $node, 'field_course_location_info', ['label' => 'hidden']);
            print render($field);
            ?>
          </div>
        <?php endif; ?>


        <?php if ($related_news) : ?>
          <div class="related-news mb-3">
            <h5>Recent <?php print $abbrev_cat_name; ?> News</h5>
            <?php print $related_news; ?>
          </div>
        <?php endif; ?>

        <div class="rel-author-info">
          <p>AGI&#39;s team of experienced instructors are led by <a href="http://www.jennifersmith.com/" rel="author">Jennifer Smith</a> the design and usability expert and best-selling author of more than 20 books including&nbsp;Creative Cloud for Dummies,&nbsp;Creative Suite for Dummies, and&nbsp;Photoshop Digital Classroom. Classes are led by experienced professionals who have extensive professional and training experience, and also work in our consulting practice areas and as practicing professionals. For Federal agencies, American Graphics Institute classes are offered under GSA contract 47QTCA19D003Y.</p>
        </div>
      </div>
    </div>

    <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div> <!-- End of wrapper -->
