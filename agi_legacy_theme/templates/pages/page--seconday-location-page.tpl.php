<div id="wrapper">

  <div class="container" style="position:relative;">
    <div class="row">
      <div class="col p-0">
        <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/header.inc'); ?>
      </div>
    </div>

    <?php if ($tabs): ?>
      <div class="tabs">
        <?php print render($tabs); ?>
      </div>
    <?php endif; ?>

    <div id="mainContent" class="row">
      <div class="col">

        <div class="row">
          <div class="col-lg-8 offset-lg-4">
            <h1 class="mb-3"><?php print $title; ?></h1>
          </div>
        </div>
        <?php print render($page['content']); ?>
      </div>

      <div id="sidebar-left">
        <div class="address">
          <?php print $address; ?>
        </div>
        <div class="request-info mb-5">
          <div class="card">
            <?php if (!empty($request_info_image)) : ?>
              <?php print $request_info_image; ?>
            <?php endif; ?>
            <div class="card-body">
              <?php print $request_info; ?>
            </div>
          </div>
        </div>

        <div class="faculty">
          <p>The <?php print $variables['node']->title; ?>, <?php print $variables['node']->field_state[LANGUAGE_NONE][0]['value']; ?> training facility is a rental facility available for private classes and for groups. For regularly scheduled classes for individuals, view the class options listed to the left.</p>
        </div>
        <div class="questions">
          <h4 class="larger-text">Questions about training in <?php print $variables['node']->title; ?>?</h4>
          <p>Call 800-851-9237 or 781-376-6044 to speak with a training representative.</p>
        </div>
        <?php if ($sidebar_desc) : ?>
        <div class="description">
          <?php print $sidebar_desc; ?>
        </div>
        <?php endif; ?>

        <?php if (isset($location_images)) : ?>
        <div class="images">
          <?php
          $image_chunk = array_chunk($location_images, 2);
          ?>
          <?php foreach ($image_chunk as $row) : ?>
            <div class="row">
              <?php foreach ($row as $image) : ?>
                <div class="col-md-6 p-1">
                  <?php print $image; ?>
                </div>
              <?php endforeach; ?>
            </div>
          <?php endforeach; ?>
          <?php print render($location_images); ?>
        </div>
        <?php endif; ?>
      </div>
    </div>

    <?php require_once(drupal_get_path('theme', 'agi') . '/templates/layout/footer.inc'); ?>

    <!-- Secondary nav pushed down in DOM -->
    <div id="secondaryNav">
      <?php include(drupal_get_path('theme', 'agi') . '/templates/inc/secondary-nav.inc'); ?>
    </div>
  </div>
</div> <!-- End of wrapper -->
