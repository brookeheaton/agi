<?php
$footer_phone_icon_alt_text = isset($footer_phone_icon_alt_text) ? $footer_phone_icon_alt_text : 'Phone icon';
$footer_location_icon_alt_text = isset($footer_location_icon_alt_text) ? $footer_location_icon_alt_text : 'location icon';
$footer_contact_icon_alt_text = isset($footer_contact_icon_alt_text) ? $footer_contact_icon_alt_text : 'Contact us icon';
?>
<?php print render($page['footer']);?>
<!-- Start of footer -->
<div id="block-block-2" class="block block-block contextual-links-region">
  <div class="content">
    <div id="pageFooter">
      <div class="row">
        <div class="col-md-4">
          <div class="phone">
            <svg class="feather-icon">
              <use xlink:href="#phone">
            </svg>
            <span>781-376-6044</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="contact-us">
            <a href="/classes/contact.html">
              <svg class="feather-icon">
                <use xlink:href="#mail">
              </svg>
              <span>CONTACT US</span>
            </a>
          </div>
        </div>
        <div class="col-md-4">
          <div class="all-training-locations">
            <a href="/classes/locations.html">
              <svg class="feather-icon">
                <use xlink:href="#pin">
              </svg>
              <span>SEE ALL AGI TRAINING LOCATIONS</span>
            </a>
          </div>
        </div>
      </div>

    </div><!-- End of footer -->
  </div>
</div>
