<?php

/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 */
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>

<head profile="<?php print $grddl_profile; ?>">
  <?php print $head; ?>
  <link rel="preload" href="https://fonts.googleapis.com">
  <link rel="apple-touch-icon" href="/<?php print path_to_theme(); ?>/assets/images/apple-touch-icon.png">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>

  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TNV63K5');</script>
  <!-- End Google Tag Manager -->

</head>
<body class="<?php print $classes; ?> covid-19" <?php print $attributes;?>>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TNV63K5"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <?php require_once (drupal_get_path('theme', 'agi') . '/templates/inc/svg-icons.inc');?>
  <div id="alert-covid-19" class="covid-19 coronavirus">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <p style="padding:0;margin:0">Coronavirus (COVID-19) information: Live classes available in-person or online from our fully vaccinated instructors. <a href="/coronavirus-covid-19">Learn more about our reopening</a>.</p>
        </div>
      </div>
    </div>
    <div class="close-x"><span class="close--x">×</span></div>
  </div>

  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>

  <?php print $scripts; ?>

  <?php $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https:' : 'http:'; ?>

  <?php $router_item = menu_get_item(); ?>
  <?php
  // Add Olark script if NOT a DC Books detail page
  if ($router_item['path'] != 'books/%/%/errata/%' && $router_item['path'] != 'books/%/%/resources/%' && $router_item['path'] != 'books/%/%/registration/%') : ?>
    <?php
    // Also exclude from Tutorial nodes.
    if (($router_item['path'] == 'node/%' && $router_item['map'][1]->type != 'tutorials') || $router_item['path'] != 'node/%') : ?>
      <?php
      // Dont show on the imce
      if ($router_item['path'] != 'imce') : ?>
        <!-- begin olark code -->
        <script type="text/javascript" async> ;(function(o,l,a,r,k,y){if(o.olark)return; r="script";y=l.createElement(r);r=l.getElementsByTagName(r)[0]; y.async=1;y.src="//"+a;r.parentNode.insertBefore(y,r); y=o.olark=function(){k.s.push(arguments);k.t.push(+new Date)}; y.extend=function(i,j){y("extend",i,j)}; y.identify=function(i){y("identify",k.i=i)}; y.configure=function(i,j){y("configure",i,j);k.c[i]=j}; k=y._={s:[],t:[+new Date],c:{},l:a}; })(window,document,"static.olark.com/jsclient/loader.js");
          /* custom configuration goes here (www.olark.com/documentation) */
          olark.identify('7989-884-10-7263');</script>
        <!-- end olark code -->
      <?php endif; ?>
    <?php endif; ?>
  <?php endif; ?>
</body>
</html>
