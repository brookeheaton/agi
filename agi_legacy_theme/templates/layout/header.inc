<?php
// Schema.org data for Organization.
$org_schema = ' itemscope itemtype="http://schema.org/Organization"';
// Dont Render rich snippet data on our JSON-LD Test pages.
if (arg(0) == 'node' && arg(1) == '38') {
  $org_schema = '';
}
// Also dont use rich snippet data on course node pages that are illustrator classes.
if (isset($node) && $node->type == 'course' && $node->field_primary_category[LANGUAGE_NONE][0]['target_id'] == 38) {
  $org_schema = '';
}
// Also dont use rich snippet data on secondary location category node pages that are illustrator classes.
if (isset($node) && $node->type == 'seconday_location_category_page' && $node->field_cat_for_location[LANGUAGE_NONE][0]['target_id'] == 38) {
  $org_schema = '';
}
// Also dont use rich snippet data on primary location category node pages that are illustrator classes.
if (isset($node) && $node->type == 'category_location_page' && $node->field_cat_for_location[LANGUAGE_NONE][0]['target_id'] == 38) {
  $org_schema = '';
}

$logo_alt_text = isset($logo_alt_text) ? $logo_alt_text : 'Adobe training, UX Courses, and Photoshop Classes at AGI';
?>

<div id="pageHeader">
  <div id="branding" class="d-flex align-items-center"<?php print $org_schema; ?>>
    <a<?php print ($org_schema ? ' itemprop="url" href="https://www.agitraining.com/"' : ''); ?>
      title="Training and Classes at American Graphics Institute">
      <img<?php print ($org_schema ? ' itemprop="logo"' : ''); ?>
        src="/sites/all/themes/agi/assets/images/Adobe-Training-AGI.png"
        alt="<?php print $logo_alt_text; ?>"
        width="65" height="70"/>
    </a>
    <h5><a href="https://www.agitraining.com"><span<?php print ($org_schema ? ' itemprop="name"' : ''); ?>>American Graphics Institute</span></a></h5>
  </div>
  <nav id="mainNav" class="navbar navbar-dark navbar-expand-lg">
    <div class="actions d-flex align-items-center justify-content-between w-100 d-lg-none">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navigation" aria-controls="main-navigation" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon">Menu</span>
      </button>
      <div class="mobile-search-wrapper d-lg-none w-100" id="mobile-search-collapse-wrapper">
        <?php print render($page['search']); ?>
      </div>
    </div>
    <div class="collapse navbar-collapse" id="main-navigation">
      <?php if (isset($page['navigation'])) : ?>
        <?php print render($page['navigation']); ?>
      <?php endif; ?>
    </div>
  </nav>
  <?php print($messages); ?>
</div>

<div id="breadcrumbs-search" class="d-flex flex-column flex-md-row justify-content-between align-items-end">

    <?php include(drupal_get_path('theme', 'agi') . '/templates/custom/global-breadcrumbs.inc'); ?>

</div>


