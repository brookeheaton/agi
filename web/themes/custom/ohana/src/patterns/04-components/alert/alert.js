!(function (document) {
  'use strict';

  document.addEventListener('DOMContentLoaded', function () {
    var alert = document.querySelector('.alert');
    var button = document.querySelector('.alert__close');

    var getCookie = function getCookie(name) {
      var value = '; ' + document.cookie;
      var parts = value.split('; ' + name + '=');

      if (parts.length === 2) {
        return parts.pop().split(';').shift();
      }
    };

    var handleClick = function handleClick() {
      document.cookie = 'alert=true;';
      alert.classList.add('is-hidden');
    };

    if (button) {
      button.addEventListener('click', handleClick);
    }

    var cookieVal = getCookie('alert');

    if (cookieVal === 'true') {
      alert.classList.add('is-hidden');
    }
  });
})(document);
