!((document) => {
  'use strict';

  document.addEventListener('DOMContentLoaded', () => {
    const hamburger = document.querySelector('.hamburger');
    if (hamburger) {
      hamburger.addEventListener('click', () => {
        hamburger.getAttribute('aria-expanded') === 'false' ? hamburger.setAttribute('aria-expanded', 'true') : hamburger.setAttribute('aria-expanded', 'false');

        hamburger.classList.toggle('is-active');
        document.querySelector('.l-nav').classList.toggle('show');
      });
    }
  });
})(document);
