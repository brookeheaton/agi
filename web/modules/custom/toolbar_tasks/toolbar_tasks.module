<?php

/**
 *  @file
 * Contains toolbar_tasks.module.
 */

use Drupal\Core\Url;

/**
 * Implements hook_toolbar().
 */
function toolbar_tasks_toolbar() {

  // Do not show tabs when on the admin theme.
  $active_theme = \Drupal::theme()->getActiveTheme()->getName();
  $admin_theme = \Drupal::config('system.theme')->get('admin');
  if ($active_theme == $admin_theme) {
    return;
  }

  $manager = \Drupal::service('plugin.manager.menu.local_task');
  $tasks = $manager->getLocalTasks(\Drupal::routeMatch()->getRouteName());
  $subtree_tasks = [];
  foreach ($tasks['tabs'] as $key => $tab) {
    $subtree_tasks[$key] = [
      'title' => $tab['#link']['title'],
      'url' => $tab['#link']['url'],
      'weight' => $tab['#weight'],
    ];
  }

  $items['primary_tasks'] = [
    '#type' => 'toolbar_item',
    'tab' => [
      '#type' => 'link',
      '#title' => t('Page Actions'),
      '#url' => Url::fromRoute('<front>'),
      '#attributes' => [
        'title' => t('Home page'),
        'class' => ['toolbar-icon', 'toolbar-icon-edit'],
      ],
    ],
    'tray' => [
      '#heading' => t('Page actions'),
      'toolbar_actions' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['toolbar-menu-administration'],
        ],
        '#theme' => 'menu_local_tasks',
        '#primary' => $tasks['tabs'],
      ],
    ],
    '#cache' => [
      'contexts' => ['user.permissions', 'url.path'],
    ],
  ];

  return $items;
}

/**
 * Implements hook_page_attachments().
 */
function toolbar_tasks_page_attachments(array &$attachments) {
  $attachments['#attached']['library'][] = 'toolbar_tasks/toolbar';
}
